'use strict';

// local imports
const {
    CartSwagger,
} = require('.');

module.exports = {
    swagger: '2.0',
    info: {
        description: 'This is a node-auth server.',
        version: '1.0.0',
        title: 'Node Auth App',
        termsOfService: 'http://node-auth.com/terms/',
        contact: {
            email: 'support@node-auth.com',
        },
    },
    basePath: '/api',
    tags: [
        CartSwagger.tag,
    ],
    schemes: ['http', 'https'],
    securityDefinitions: {
        Bearer: {
            type: 'apiKey',
            name: 'Authorization',
            in: 'header',
            description: 'Authorization header using the Bearer scheme',
        },
    },
    definitions: {
        ...CartSwagger.definitions,
    },
    paths: {
        ...CartSwagger.paths,
    }
}