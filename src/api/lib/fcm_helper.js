const fetch = require('node-fetch');

exports.pushToFirebase = async (fetchBody, retryLimit) => {
    console.log('pushToFirebase call triggered ');

    const fetchOptions = {
        method: 'post',
        body: JSON.stringify(fetchBody),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'key=AAAA-tdBRfY:APA91bGt3dulilg-Fl9fncZLbDAmsl5jZDg4ggY89L7pYgHX1lgEj191sRl5-BHUoE4FaIdKUszbx82TqGTTWPknHBb69iecnVfmD9kmrSEn8Fp-ATxWXhQuPIhWlSBPSG05nvwrEvoO'
        },
    };

    let flag = retryLimit, flag2 = 0;
    do {
        console.log('do triggered ');
        try {
            console.log('try triggered ');
            let res = await fetch('https://fcm.googleapis.com/fcm/send', fetchOptions);
            let json = await res.json();
            console.log('data ', json);
            flag = retryLimit;
            flag2 = 1;
        } catch (err) {
            console.log('catch triggered ');
            console.log('err: ', err)
            ++flag;
            flag2 = 0;
        }
    } while (flag < retryLimit);

    return flag2;
}



exports.dpushToFirebase = async (fetchBody, retryLimit) => {
    console.log('d pushToFirebase call triggered ');
    const fetchOptions = {
        method: 'post',
        body: JSON.stringify(fetchBody),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'key=AAAAV3KnJmU:APA91bHP6ZSHGFI-Wb__zTgER9I3xhWTK4U2IUuoz3v_LydoYpexrRIP2kN2iY_20iXIKBRwaxJIfe9AsLkyTB-inOj7bZrmm9q5C66ygelHE1Z49QJz3aeDRKC6ODPf8vqlD0Sw6qjl'
        },
    };

    let flag = retryLimit, flag2 = 0;
    do {
        console.log('d do triggered ');
        try {
            console.log('d try triggered ');
            let res = await fetch('https://fcm.googleapis.com/fcm/send', fetchOptions);
            let json = await res.json();
            console.log('data ', json);
            flag = retryLimit;
            flag2 = 1;
        } catch (err) {
            console.log('d catch triggered ');
            console.log('d err: ', err)
            ++flag;
            flag2 = 0;
        }
    } while (flag < retryLimit);

    return flag2;
}


exports.lpushToFirebase = async (fetchBody, retryLimit) => {
    console.log('laundry man lpushToFirebase call triggered ');

    const fetchOptions = {
        method: 'post',
        body: JSON.stringify(fetchBody),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'key=AAAA42X9UJg:APA91bH4ohQFnYAdTvDlGQeLlDNBBrO6OvYvH41tL3fsEe9Pk666DVpAny0EYF0XH8BJGXXQDIHPHdEaIdEeNhdiZubDmaamLy4-lUD-8vID5kh73822LgsfVKWUh4F7Qd5FqQegAPV-'
        },
    };

    let flag = retryLimit, flag2 = 0;
    do {
        console.log('do triggered ');
        try {
            console.log('try triggered ');
            let res = await fetch('https://fcm.googleapis.com/fcm/send', fetchOptions);
            let json = await res.json();
            console.log('data ', json);
            flag = retryLimit;
            flag2 = 1;
        } catch (err) {
            console.log('catch triggered ');
            console.log('err: ', err)
            ++flag;
            flag2 = 0;
        }
    } while (flag < retryLimit);

    return flag2;
}
