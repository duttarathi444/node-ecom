'use strict';

// global imports
const express = require('express');
// local imports
const env = require('../../env');

const router = express.Router();

module.exports = router;

// home route
router.get('/', (req, res, next) => {
    res.send(`<div>
                <center>
                    <h1>@${env.app.name}</h1>
                    <h3>Swagger 2.0 Spec: <a href="/doc/swagger">/swagger.json</a></h3>
                    <h3>Swagger Explorer: <a href="/explorer/swagger">/swagger-explorer</a></h3>
                </center>
            </div>`
    );
});