'use strict';

// global imports
const express = require('express');
// local imports
const env = require('../../../env');
const {
    findCategory,
    findCategoryProducts,
    findAllProducts,
    findProduct,
    findProductsByCategory,
    findSliders
} = require('../../controllers').ProductController;

const { authenticate, authorize } = require('../../middlewares').AuthorizationMiddleware;
const { catchErrors } = require('../../middlewares').ErrorHandlerMiddleware;
const router = express.Router();
//const { v1 } = env.routes.user;

module.exports = router;

/**********************************************
 * ProductMaster Model APIs v1
 *
 *******************************/
router.get('/api/v1/product/findSliders', catchErrors(findSliders));
router.get('/api/v1/product/findCategory', catchErrors(findCategory));
router.get('/api/v1/product/findCategoryProducts/:id', catchErrors(findCategoryProducts));

router.get('/api/v1/product/findAllProducts', authenticate(), catchErrors(findAllProducts));
router.get('/api/v1/product/findProduct/:id', authorize(), catchErrors(findProduct));
router.get('/api/v1/product/findProductsByCategory/:id', authenticate(), catchErrors(findProductsByCategory));