'use strict';

// global imports
const express = require('express');
// local imports
const env = require('../../../env');
const { ordersDetailReport, misUserLogout, misUserLogin } = require('../../controllers').MisUserController;
const { authenticate, authorize, authorizeMisUser } = require('../../middlewares').AuthorizationMiddleware;
const { catchErrors } = require('../../middlewares').ErrorHandlerMiddleware;
const router = express.Router();
//const { v1 } = env.routes.user;

module.exports = router;


/*********************************
 * ProductMaster Model APIs v1
 *
 *******************************/

router.post('/api/v1/employee/misuser/login', catchErrors(misUserLogin));
router.post('/api/v1/employee/misuser/logout', authorizeMisUser(), catchErrors(misUserLogout));
router.post('/api/v1/employee/misuser/ordersDetailReport', catchErrors(ordersDetailReport));
