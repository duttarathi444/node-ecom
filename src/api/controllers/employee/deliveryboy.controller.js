// global import
const HttpStatus = require('http-status-codes');
const jwt = require('jsonwebtoken');
const moment = require('moment');
// global imports
const Sequelize = require("sequelize");
const op = Sequelize.Op;
// local imports
const { dpushToFirebase, lpushToFirebase, pushToFirebase } = require('../../lib/fcm_helper');
const { getModels, SequelizeOp } = require('../../models');
const env = require('../../../env');
const deliveryBoyDeliverCompleteStatusId = env.service_flow.deliveryboy_deliver_to_customer_status_id;// 1050
const deliveryBoyAcceptOrderFromLmanStatusId = env.service_flow.deliveryboy_accept_from_laundryman_status_id;// 1040
const utcOffsetMins = env.env.utcOffsetMins;
const jwtSecretKey = '123##$$)(***&';
/*
const env = require('../../../env');
const deliveryBoyDeliverCompleteStatusId = env.service_flow.deliveryboy_deliver_to_customer_status_id;// 1050
const deliveryBoyAcceptOrderFromLmanStatusId = env.service_flow.deliveryboy_accept_from_laundryman_status_id;// 1040
    order_creation_id: 1001,
    laundry_man_accept_status_id: 1020,
    laundry_man_complete_status_id: 1030,
    deliveryboy_accept_from_customer_status_id: 1010,
    deliveryboy_accept_from_laundryman_status_id: 1040,
    deliveryboy_deliver_to_customer_status_id: 1050

*/


const deliveryboy_accept_from_customer_status_id = env.service_flow.deliveryboy_accept_from_customer_status_id;
const deliveryboy_accept_from_laundryman_status_id = env.service_flow.deliveryboy_accept_from_laundryman_status_id;
const deliveryboy_deliver_to_customer_status_id = env.service_flow.deliveryboy_deliver_to_customer_status_id;
// Deliveryboy Login
exports.deliveryboyLogin = async (req, res) => {
    const { EmployeeLogin, db1Conn } = await getModels();
    console.log('\ndeliveryboy.controller.deliveryboyLogin  triggered -->');

    console.log(`req.body: `, req.body);
    // phoneno is same as otp here
    const { emp_login_id, emp_password, fcmToken } = req.body;

    console.log(`Delivery Boy Employee Login-Id:  `, emp_login_id);
    console.log(`Delivery Boy Employee Password:  `, emp_password);
    console.log(`Delivery Boy login fcmToken:  `, fcmToken);
    try {
        // Check employee given login_id and password match in employee_login table or not
        let employee = await EmployeeLogin.findOne({ where: { emp_login_id: emp_login_id, emp_password: emp_password, emp_role_id: 2 } });

        if (employee) {

            console.log(`\n\nEmployee details existing in emplolee_login table...`);

            // create an access token
            const access_token = jwt.sign({ id: employee.emp_id }, jwtSecretKey, {
                expiresIn: '365d', // expires in X secs
            });

            let lastAccessDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm A");
            await EmployeeLogin.update(
                { access_token: access_token, emp_last_login_date_time: lastAccessDate, fcmToken: fcmToken },
                { where: { emp_id: employee.emp_id } }
            )

            let employeeLoginDetails = await EmployeeLogin.findOne({ where: { emp_id: employee.emp_id } });
            res.json({ status: 1, message: "employee_login details with new access_token", data: employeeLoginDetails })


        }
        else {
            console.log('\n\n Employee given login_id and password does not match, Sending Error Response...');

            res.json({ status: 0, message: "login_id  and password not match. You are unauthenticated", data: {} });
        }

    } catch (error) {
        console.log(error);
        //res.status(404).send(error);
        // res.json({ status: 0, message: "auth error", data: JSON.stringify(error, 0, 2) });
        throw error
    }
}


// Delivery Boy  Logout 
exports.deliveryboyLogout = async (req, res) => {
    console.log('\ndeliveryboy.controller.logout triggered -->');
    const { EmployeeLogin, db1Conn } = await getModels();

    /*  {
        emp_id: req.emp_id
    }
    */
    try {
        let employee = await EmployeeLogin.findOne({ where: { emp_id: req.body.emp_id } });
        if (!employee) {
            const errorResponse = {
                status: 0,
                message: 'Invalid user',
                data: {}
            };
            return res.status(HttpStatus.BAD_REQUEST).send(errorResponse);
        }
        else {
            let deleteAccessToken = await EmployeeLogin.update(
                { access_token: null },
                { where: { emp_id: req.body.emp_id } }
            );

            if (deleteAccessToken) {
                res.json({ status: 1, message: "'User logout successful", data: {} });
            }
            else {
                res.json({ status: 0, message: "'User already logout ", data: {} });
            }
        }

    }
    catch (error) {
        console.log('\nUserController.logout error', error)
        throw error;

    }
}

// Code To Be Block
exports.newOrderListXYZ = async (req, res) => { }
//======================================
//Customer / Laundry Man  New Order List For Delivery Boy 
//=================================
exports.newOrderList = async (req, res) => {
    try {
        console.log('\ndeliveryboy.controller.newOrderList  triggered -->');
        const { PriceMaster, ProductMaster, OrderHeader, OrderDetails, DelboyOrderMapping, UserMaster, db1Conn } = await getModels();
        let newOrderList = await DelboyOrderMapping.findAll({
            where: { emp_id: req.body.emp_id, active_status: '1' }
        });  // retrive those records from delcoy_order_mapping table whose active_status=1 made by store_procedure

        let newOrderList2 = [];
        if (newOrderList) {
            await Promise.all(newOrderList.map(async (element) => {
                // filter logic to be added condition where payment_status = 'NA' or payment_status ='PAID'
                // WHERE  order_id: element.order_id AND (payment_status = 'NA' OR payment_status = 'PAID')
                let orderHeader = await OrderHeader.findOne({
                    where: {
                        order_id: element.order_id,
                        payment_status: {
                            [SequelizeOp.or]: ["NA", "PAID"]
                        },
                        order_flag: 1
                    }
                });
                if (orderHeader) {
                    console.log('\nDelivery Boy EmployeeId : ' + element.emp_id + 'mapping with  orderId :' + element.order_id);
                    // Select record  from order_header  based on  order_id
                    let orderHeader = await OrderHeader.findOne({ where: { order_id: element.order_id } });
                    console.log("\n\nORDER HEADER DETAILS======", JSON.stringify(orderHeader, 0, 2));

                    // Select customer_name from user_master based on user_id found in order_header
                    let userDetails = await UserMaster.findOne({ where: { user_id: orderHeader.user_id } });
                    console.log("\n\nUSER MASTER DETAILS======", JSON.stringify(userDetails, 0, 2));
                    let customer_name = "";
                    if (userDetails) {
                        if (userDetails.login_type === 'Otp') {
                            customer_name = userDetails.user_fname
                        }
                        else {
                            customer_name = userDetails.user_fname + " " + userDetails.user_lname;
                        }
                        element.setDataValue('status_id', orderHeader.status_id);
                        element.setDataValue('prority_id', element.id);
                        element.setDataValue('total_quantity', orderHeader.item_count);
                        element.setDataValue('total_price', orderHeader.total_price);
                        element.setDataValue('order_date', orderHeader.order_date);
                        element.setDataValue('customer_name', customer_name);
                        element.setDataValue('address', orderHeader.address);
                        element.setDataValue('payment_mode', orderHeader.payment_mode);
                        element.setDataValue('payment_status', orderHeader.payment_status);
                        let orderDetails = await OrderDetails.findAll({ where: { order_id: element.order_id } });
                        console.log("\n\nORDER DETAILS======", JSON.stringify(orderDetails, 0, 2));
                        await Promise.all(orderDetails.map(async (element_1) => {
                            // here select product_name from product_master based on prod_id
                            let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
                            console.log("\n\nPRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));
                            let price_master = await PriceMaster.findOne({ where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' } });
                            if (price_master && product_master) {
                                console.log("PRODUCT DETAILS======", JSON.stringify(price_master, 0, 2));
                                console.log('\nproduct ' + element_1.prod_id + ' price of ' + price_master.prod_price);
                                element_1.setDataValue('prod_name', product_master.prod_name);
                                element_1.setDataValue('prod_code', product_master.prod_code);
                                element_1.setDataValue('unit_price', price_master.prod_price);
                            }
                            else {
                                console.log('\nNo such product for ' + element_1.prod_id);
                                element_1.setDataValue('prod_name', "NA");
                                element_1.setDataValue('prod_code', "NA");
                                element_1.setDataValue('unit_price', 0);
                            }
                        }));
                        element.setDataValue('order_details', orderDetails);
                        newOrderList2.push(element);
                    } else {
                        console.log("\n\n No such User for OrderId: ", orderHeader.order_id);
                    }
                }
            }));
            res.json({ status: 1, message: "new customer order list", data: newOrderList2 });
        }
        else {
            // res.json({ status: 1, message: "cart details", data: "no cart" });
            res.json({ status: 0, message: "empty customer order list", data: {} });
        }
    }
    catch (error) {

        console.log(error);
        console.log("\n\ndeliveryboy.controller.newOrderList", JSON.stringify(error, 0, 2));
        //res.status(404).send(error);
        //res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        throw error
    }
}


//Accept Customer/ Laundry Man Order
exports.acceptOrder = async (req, res) => {
    const { Login, Employee, OrderNotifications, EmployeeLogin, LaundrymanOrderMapping, OrderHeader, OrderDetails, OrderHistory, DelboyOrderMapping, PriceMaster, LaundryServiceFlow, db1Conn, EmployeeRoleMaster, PartnerOrderTrack } = await getModels();
    console.log('\ndeliveryboy.controller.acceptCustomerOrder  triggered -->');
    console.log(`\n req.body: `, req.body);

    const { order_details, order_id, emp_id, user_id, delivery_type } = req.body;
    console.log("\n\nORDER ID======", order_id);
    console.log("\n\nEMPLOYEE ID======", emp_id);
    console.log("\n\nORDER DETAILS======", JSON.stringify(order_details, 0, 2));
    console.log("\n\nDELIVERY TYPE======", delivery_type);


    let gtotal_price = 0;
    let gtotal_quantity = 0;
    let status_id;
    const t = await db1Conn.transaction();

    try {
        if (delivery_type == 2) {
            status_id = deliveryboy_accept_from_laundryman_status_id;
        }
        else {
            status_id = deliveryboy_accept_from_customer_status_id;
        }
        // Here status_id comes from env file and module_id is  hard coded here, it will be dynamic in future
        let laundry_service_flow = await LaundryServiceFlow.findOne({ where: { status_id: status_id, module_id: 1 } }, { transaction: t });
        console.log("\n\n LAUNDRY SERVICE FLOW======", JSON.stringify(laundry_service_flow, 0, 2));
        // fetch role_id of role_name=Deliveryboy
        let emp_role_master = await EmployeeRoleMaster.findOne({ where: { emp_role_name: 'Deliveryboy' } }, { transaction: t });
        console.log("\n\n EMP ROLE MASTER======", JSON.stringify(emp_role_master, 0, 2));


        const deliveryboy_accept_corder_moment = moment().utc().utcOffset(utcOffsetMins);
        var deliveryboy_accept_corder_date_time = deliveryboy_accept_corder_moment.format("DD/MM/YYYY hh:mm A");
        let date_time_array = deliveryboy_accept_corder_date_time.split(" ");
        let deliveryboy_accept_corder_date = date_time_array[0];
        let deliveryboy_accept_corder_time = date_time_array[1] + " " + date_time_array[2];

        await Promise.all(order_details.map(async (element) => {
            console.log("\n\n ELEMENT OF ORDER Details======", JSON.stringify(element, 0, 2));
            // here select price from price_master based on prod_id
            let product = await PriceMaster.findOne({ where: { prod_id: element.prod_id, prod_status: 'CurPrice' } }, { transaction: t });
            console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
            console.log('\nproduct ' + element.prod_id + ' price of ' + product.prod_price);
            let quantity = parseInt(element.quantity);
            console.log('\nProduct quantity ' + quantity);
            let total_price = parseFloat(product.prod_price) * quantity;
            console.log('\nTotal Price ' + total_price);
            gtotal_price = gtotal_price + total_price;
            gtotal_quantity = gtotal_quantity + quantity;
            if (Number(total_price) === total_price && total_price % 1 === 0) {
                console.log('\nINTEGER ');
            }
            else {
                console.log('\nFLOAT');
            }

            if (delivery_type == 1) {
                const orderDetails = await OrderDetails.update(
                    { dboy_order_accept_quantity: quantity },
                    { where: { order_id: order_id, prod_id: element.prod_id } }, { transaction: t });
                console.log("\n\n ORDER DETAILS QUANTITY UPDATED ONLY ACCEPTING FROM CUSTOMER");
                console.log("\n\n ORDER DETAILS DELIVERY BOY ACCEPT ORDER QUANTITY UPDATED======", JSON.stringify(orderDetails, 0, 2));
            }
            //change 0n 11/5/2020 9.53pm as sukhranjan sir told me only DP accept quantity by recieve product from custome not from accepting from LP bcz of packing products
            // const orderDetails = await OrderDetails.update(
            //     { dboy_order_accept_quantity: quantity },
            //     { where: { order_id: order_id, prod_id: element.prod_id } }, { transaction: t });
            // console.log("\n\n ORDER DETAILS DELIVERY BOY ACCEPT ORDER QUANTITY UPDATED======", JSON.stringify(orderDetails, 0, 2));


            // element_1.setDataValue('prod_name', product_master.prod_name);
            // element_1.setDataValue('prod_code', product_master.prod_code);
            // element_1.setDataValue('per_unit_price', product.prod_price);
            // // element.setDataValue('total_price', total_price.toFixed(2));



        }));

        console.log('\nGRAND TOTAL PRICE ' + gtotal_price);
        console.log('\nGRAND TOTAL QUANTITY ' + gtotal_quantity);

        let gt_price = (Math.floor(gtotal_price * 100) / 100).toFixed(2);
        console.log('\nGRAND TOTAL PRICE gt_price' + gt_price);
        if (Number(gtotal_price) === gtotal_price && gtotal_price % 1 === 0) {
            console.log('\nGRAND TOTAL PRICE IS INTEGER ');
        }
        else {
            console.log('\nGRAND TOTAL PRICE IS FLOAT');
        }

        const orderHistory = await OrderHistory.create(
            {
                order_id: order_id, status_id: laundry_service_flow.status_id, status_date: deliveryboy_accept_corder_date, active_flag: laundry_service_flow.status_desc, user_id: user_id, emp_id: emp_id, role_id: emp_role_master.emp_role_id, status_time: deliveryboy_accept_corder_time, item_count: gtotal_quantity,
                total_price: gt_price
            },
            { transaction: t });
        console.log("\n\n ORDER HISTORY CREATED FOR DELIVERY BOY ======", JSON.stringify(orderHistory, 0, 2));
        let orderHeader;
        if (delivery_type == 1) {
            orderHeader = await OrderHeader.update(
                { status_id: laundry_service_flow.status_id, item_count: gtotal_quantity, total_price: gt_price },
                { where: { order_id: order_id, user_id: user_id } }, { transaction: t });
            console.log("\n\n ORDER HEADER UPDATED FOR DELIVERY BOY ACCEPT FROM CUSTOMER======", JSON.stringify(orderHeader, 0, 2));
        }
        else {
            orderHeader = await OrderHeader.update(
                { status_id: laundry_service_flow.status_id },
                { where: { order_id: order_id, user_id: user_id } }, { transaction: t });
            console.log("\n\n ORDER HEADER UPDATED FOR DELIVERY ACCEPT FROM LAUNDRY MAN======", JSON.stringify(orderHeader, 0, 2));
        }

        const delboyOrderMapping = await DelboyOrderMapping.update(
            { active_status: "0" },
            { where: { order_id: order_id, emp_id: emp_id } }, { transaction: t });
        console.log("\n\n Delivery BOy Order Mapping UPDATED======", JSON.stringify(delboyOrderMapping, 0, 2));
        if (delivery_type == 1) {
            const partnerOrderTrack = await PartnerOrderTrack.create(
                {
                    order_id: order_id, partner_id: emp_id, partner_type: 2, delivery_type: delivery_type, date: deliveryboy_accept_corder_date, time: deliveryboy_accept_corder_time
                },
                { transaction: t });
            console.log("\n\n PARTNER ORDER TRACK CREATED FOR ORDER RECEIVED FROM CUSTOMER======", JSON.stringify(partnerOrderTrack, 0, 2));


        }

        t.commit();
        if (delivery_type == 1) {
            try {
                let laundryman_order_mapping = await LaundrymanOrderMapping.findOne({ where: { order_id: order_id, active_status: 1 } });
                let employee_login = await EmployeeLogin.findOne({ where: { emp_id: laundryman_order_mapping.emp_id } });
                const msg_body = 'Order no ' + order_id + ' has been assigned to you. Delivery person on its way to hand over clothes ';
                const dorderNotifications = await OrderNotifications.create(
                    {
                        order_id: order_id,
                        status_id: 1010,
                        emp_id: laundryman_order_mapping.emp_id,
                        role_id: 1,
                        msg_title: 'Order has been assigened to laundry man',
                        msg_body: msg_body,
                        created_at: deliveryboy_accept_corder_date_time
                    });

                const fetchBody = {
                    notification: {
                        title: "iestre",
                        text: msg_body,
                        click_action: "com.maks.iestrelaundryman.NotificationActivity"
                    },
                    data: {
                        orderId: orderHeader.order_id
                    },
                    to: employee_login.fcmToken
                };

                let result = lpushToFirebase(fetchBody, 3);
                if (result) {
                    console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO LAUNDRY MAN");
                }

            }
            catch (error) {
                console.log("\n\n LAUNDRY MAN SEND NOTIFICATION ERROR ", error);
            }
        }
        else {

            try {
                let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: order_id, delivery_type: 2 } });
                let login = await Login.findOne({ where: { user_id: user_id } });
                if (dboy_order_mapping) {
                    let employee = await Employee.findOne({ where: { emp_id: emp_id } });
                    empname = employee.emp_fname + " " + employee.emp_lname;
                    emp_phoneno = employee.emp_contact_no;
                }
                const msg_body = 'Order no ' + order_id + ' has been collected by Delivery Person Mr. ' + empname +
                    '(Ph-' + emp_phoneno + '), will be delivered shortly';
                const dorderNotifications = await OrderNotifications.create(
                    {
                        order_id: order_id,
                        status_id: deliveryBoyAcceptOrderFromLmanStatusId,
                        user_id: user_id,
                        msg_title: 'Order has been collected by delivery person from laundry man',
                        msg_body: msg_body,
                        created_at: deliveryboy_accept_corder_date_time
                    });

                const fetchBody = {
                    notification: {
                        title: "iestre",
                        text: msg_body,
                        click_action: "com.purpuligo.laundry.NotificationActivity"
                    },
                    data: {
                        orderId: orderHeader.order_id
                    },
                    to: login.fcmToken
                };

                let result = pushToFirebase(fetchBody, 3);
                if (result) {
                    console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO CUSTOMER AFTER DELIVERI BOY RECEIVED FROM LMAN");
                }

            }
            catch (error) {
                console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO CUSTOMER AFTER DELIVERI BOY RECEIVED FROM LMAN ERROR ", error);
            }

        }
        res.json({ status: 1, message: "order accepted by delivery boy successfully", data: {} })


    }
    catch (error) {
        t.rollback()
        console.log("deliveryboy.controller.acceptCustomerOrder Error ===", error);
        // res.status(404).send(error);
        throw error;
    }
}

//======================================
//Delivery Boy Accepted Order List Of Customer
//=================================

exports.acceptedCustomerOrderList = async (req, res) => {
    try {
        console.log('\ndeliveryboy.controller.acceptedCustomerOrderList  triggered -->');
        const { OrderHeader, OrderDetails, OrderHistory, ProductMaster, UserMaster, LaundrymanOrderMapping, Employee, EmployeeWorkLocation, Address, LaundryServiceFlow, CartDetails, CartHeader, db1Conn } = await getModels();
        //let attributes = ['OrderHeader.order_id', 'OrderHeader.user_id', 'OrderHeader.order_date', 'OrderHeader.item_count_upd', 'OrderHeader.expected_delivery_time'];
        let attributes = ['order_id', 'trans_id', 'user_id', 'order_date', 'status_id', 'item_count', 'total_price', 'address', 'expected_delivery_time', 'actual_delivery_time'];
        let odr_ids = [];
        let orderHistories;
        let orderids = await OrderHistory.findAll({
            where: { emp_id: req.body.emp_id, status_id: 1020 }
        });

        console.log("\n\norderids======" + orderids);
        await Promise.all(orderids.map(async (element) => {
            console.log("\n\nORDER_ID======" + element.order_id);
            odr_ids.push(element.order_id);
        }));
        console.log("\n\nodr_ids======" + odr_ids);
        if (odr_ids.length > 0) {
            orderHistories = await OrderHistory.findAll({
                where: {
                    emp_id: req.body.emp_id, status_id: 1010,
                    [op.not]: [
                        { order_id: odr_ids },
                    ]

                }
            });
        }
        else {
            orderHistories = await OrderHistory.findAll({
                where: { emp_id: req.body.emp_id, status_id: 1010 }
            });
        }


        let orders = [];
        console.log("\n\nORDER HISTORIES======", JSON.stringify(orderHistories, 0, 2));
        if (orderHistories) {

            await Promise.all(orderHistories.map(async (element) => {
                //         console.log('\nUser--' + element.user_id + ' having  orderId ' + element.order_id);
                //         // here select all order deatils list  from order_details  based on user_id and order_id
                //         let orderDetails = await OrderDetails.findAll({ where: { user_id: element.user_id, order_id: element.order_id } });
                console.log("\n\nORDER_ID======" + element.order_id);

                const order = await OrderHeader.findOne({
                    where: { order_id: element.order_id }, attributes: attributes,
                    include: [
                        { model: OrderDetails, as: 'order_details' },
                        { model: OrderHistory, where: { status_id: 1010 }, as: 'order_history', attributes: ['status_date', 'status_time'] }
                    ]
                });
                orders.push(order);

            }));

            await Promise.all(orders.map(async (element) => {
                console.log("\n\n ORDER Details======", JSON.stringify(element.order_details, 0, 2));
                // here select customer_name from user_master based on user_id found in order_header
                let userDetails = await UserMaster.findOne({ where: { user_id: element.user_id } });
                console.log("\n\nUSER MASTER DETAILS======", JSON.stringify(userDetails, 0, 2));
                let customer_name = "";
                let laundryman_name = "";
                let laundryman_contactno = "";
                let laundryman_contactno2 = "";
                let laundryman_longitude = "";
                let laundryman_latitude = "";
                if (userDetails.login_type === 'Otp') {
                    customer_name = userDetails.user_fname
                }
                else {
                    customer_name = userDetails.user_fname + " " + userDetails.user_lname;
                }



                // here select emp_id from laundryman_order_mapping based on order_id found in order_header
                let laundrymanOrderMapping = await LaundrymanOrderMapping.findOne({ where: { order_id: element.order_id } });
                console.log("\n\nLAUNDRYMAN ORDER MAPPING DETAILS======", JSON.stringify(laundrymanOrderMapping, 0, 2));
                if (laundrymanOrderMapping) {
                    console.log("\n\nLAUNDRYMAN ORDER MAPPING EXIST======");
                    let employee = await Employee.findOne({ where: { emp_id: laundrymanOrderMapping.emp_id } });
                    console.log("\n\nEMPLOYEE DETAILS======", JSON.stringify(employee, 0, 2));
                    let employeeWorkLocation = await EmployeeWorkLocation.findOne({ where: { emp_id: laundrymanOrderMapping.emp_id } });
                    console.log("\n\nEMPLOYEE WORKLOCATION DETAILS======", JSON.stringify(employeeWorkLocation, 0, 2));
                    if (employee) {
                        console.log("\n\nLAUNDRYMAN EXIST======");
                        laundryman_name = employee.emp_fname + " " + employee.emp_lname;
                        laundryman_contactno = employee.emp_contact_no;
                        laundryman_contactno2 = employee.emp_contact_no2;
                        if (employeeWorkLocation) {
                            console.log("\n\nLAUNDRYMAN WORKLOACTION EXIST======");
                            laundryman_longitude = employeeWorkLocation.longitude;
                            laundryman_latitude = employeeWorkLocation.latitude;

                        }
                        else {
                            console.log("\n\nLAUNDRYMAN WORKLOCATION NOT EXIST======");
                            laundryman_longitude = "Not Available";
                            laundryman_latitude = "Not Available";

                        }
                    }
                    else {
                        console.log("\n\nLAUNDRYMAN NOT EXIST======");
                        laundryman_name = "Not Available";
                        laundryman_contactno = "Not Available";
                        laundryman_contactno2 = "Not Available";
                    }
                }
                else {
                    console.log("\n\nLAUNDRYMAN ORDER MAPPING NOT EXIST======");
                    laundryman_name = "Not Available";
                    laundryman_contactno = "Not Available";
                    laundryman_contactno2 = "Not Available";
                    laundryman_longitude = "Not Available";
                    laundryman_latitude = "Not Available";
                }



                await Promise.all(element.order_details.map(async (element_1) => {
                    console.log("\n\n INV ORDER Details======", JSON.stringify(element_1, 0, 2));

                    // here select price from price_master based on prod_id
                    // let product = await PriceMaster.findOne({ where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' } });
                    // console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
                    // console.log('\nproduct ' + element_1.prod_id + ' price of ' + product.prod_price);



                    // here select product_name from product_master based on prod_id
                    let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
                    console.log("PRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));


                    // let quantity = parseInt(element.quantity);
                    // console.log('\nProduct quantity ' + quantity);
                    // let total_price = parseFloat(product.prod_price) * quantity;

                    // console.log('\nTotal Price ' + total_price);
                    element_1.setDataValue('prod_name', product_master.prod_name);
                    element_1.setDataValue('prod_code', product_master.prod_code);
                    // element_1.setDataValue('per_unit_price', product.prod_price);
                    // element.setDataValue('total_price', total_price.toFixed(2));

                }));
                element.setDataValue('customer_name', customer_name);
                element.setDataValue('laundryman_name', laundryman_name);
                element.setDataValue('laundryman_contactno', laundryman_contactno);
                element.setDataValue('laundryman_contactno2', laundryman_contactno2);
                element.setDataValue('laundryman_worklocation', 'http://maps.google.com/?q=' + laundryman_latitude + ',' + laundryman_longitude);



            }));

            console.log("\n\nORDER LIST DETAILS======", JSON.stringify(orders, 0, 2));
            res.json({ status: 1, message: "accepted order details list of delivery boy", data: orders });

        }
        else {
            // res.json({ status: 1, message: "cart details", data: "no cart" });
            res.json({ status: 0, message: "empty order accepted list of delivery boy", data: {} });
        }
    }
    catch (error) {
        console.log(error);
        res.status(404).send(error);
    }
}


//======================================
//Delivery Boy Accepted Order List From Laundry Man
//=================================

exports.acceptedLaundrymanOrderList = async (req, res) => {
    try {
        console.log('\ndeliveryboy.controller.acceptedLaundrymanOrderList  triggered -->');
        const { OrderHeader, OrderDetails, OrderHistory, ProductMaster, UserMaster, Address, LaundryServiceFlow, CartDetails, CartHeader, db1Conn } = await getModels();
        //let attributes = ['OrderHeader.order_id', 'OrderHeader.user_id', 'OrderHeader.order_date', 'OrderHeader.item_count_upd', 'OrderHeader.expected_delivery_time'];
        let attributes = ['order_id', 'trans_id', 'user_id', 'order_date', 'status_id', 'item_count', 'total_price', 'address', 'expected_delivery_time', 'actual_delivery_time'];
        let odr_ids = [];
        let orderHistories;
        let orderids = await OrderHistory.findAll({
            where: { emp_id: req.body.emp_id, status_id: deliveryBoyDeliverCompleteStatusId }
        });

        console.log("\n\norderids======" + orderids);
        await Promise.all(orderids.map(async (element) => {
            console.log("\n\nORDER_ID======" + element.order_id);
            odr_ids.push(element.order_id);
        }));
        console.log("\n\nodr_ids======" + odr_ids);
        if (odr_ids.length > 0) {
            orderHistories = await OrderHistory.findAll({
                where: {
                    emp_id: req.body.emp_id, status_id: deliveryBoyAcceptOrderFromLmanStatusId,
                    [op.not]: [
                        { order_id: odr_ids },
                    ]

                }
            });
        }
        else {
            orderHistories = await OrderHistory.findAll({
                where: { emp_id: req.body.emp_id, status_id: deliveryBoyAcceptOrderFromLmanStatusId }
            });
        }


        let orders = [];
        console.log("\n\nORDER HISTORIES======", JSON.stringify(orderHistories, 0, 2));
        if (orderHistories) {

            await Promise.all(orderHistories.map(async (element) => {
                //         console.log('\nUser--' + element.user_id + ' having  orderId ' + element.order_id);
                //         // here select all order deatils list  from order_details  based on user_id and order_id
                //         let orderDetails = await OrderDetails.findAll({ where: { user_id: element.user_id, order_id: element.order_id } });
                console.log("\n\nORDER_ID======" + element.order_id);

                const order = await OrderHeader.findOne({
                    where: { order_id: element.order_id }, attributes: attributes,
                    include: [
                        { model: OrderDetails, as: 'order_details' },
                        { model: OrderHistory, where: { status_id: deliveryBoyAcceptOrderFromLmanStatusId }, as: 'order_history', attributes: ['status_date', 'status_time'] }
                    ]
                });
                orders.push(order);

            }));

            await Promise.all(orders.map(async (element) => {
                console.log("\n\n ORDER Details======", JSON.stringify(element.order_details, 0, 2));
                // here select customer_name from user_master based on user_id found in order_header
                let userDetails = await UserMaster.findOne({ where: { user_id: element.user_id } });
                console.log("\n\nUSER MASTER DETAILS======", JSON.stringify(userDetails, 0, 2));
                let customer_name = "";
                if (userDetails.login_type === 'Otp') {
                    customer_name = userDetails.user_fname
                }
                else {
                    customer_name = userDetails.user_fname + " " + userDetails.user_lname;
                }

                await Promise.all(element.order_details.map(async (element_1) => {
                    console.log("\n\n INV ORDER Details======", JSON.stringify(element_1, 0, 2));

                    // here select price from price_master based on prod_id
                    // let product = await PriceMaster.findOne({ where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' } });
                    // console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
                    // console.log('\nproduct ' + element_1.prod_id + ' price of ' + product.prod_price);


                    // here select product_name from product_master based on prod_id
                    let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
                    console.log("PRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));


                    // let quantity = parseInt(element.quantity);
                    // console.log('\nProduct quantity ' + quantity);
                    // let total_price = parseFloat(product.prod_price) * quantity;

                    // console.log('\nTotal Price ' + total_price);
                    element_1.setDataValue('prod_name', product_master.prod_name);
                    element_1.setDataValue('prod_code', product_master.prod_code);
                    // element_1.setDataValue('per_unit_price', product.prod_price);
                    // element.setDataValue('total_price', total_price.toFixed(2));

                }));
                element.setDataValue('customer_name', customer_name);
                element.setDataValue('customer_phoneno', userDetails.user_phoneno);
                element.setDataValue('customer_location', 'http://maps.google.com/?q=' + element.address.latitude + ',' + element.address.latitude.longitude);
            }));

            console.log("\n\nORDER LIST DETAILS======", JSON.stringify(orders, 0, 2));
            res.json({ status: 1, message: "accepted order details list from laundry man", data: orders });

        }
        else {
            // res.json({ status: 1, message: "cart details", data: "no cart" });
            res.json({ status: 0, message: "empty order accepted list from laundry man", data: {} });
        }
    }
    catch (error) {
        console.log("deliveryboy.controller.acceptedLaundrymanOrderList=========", error);
        res.status(404).send(error);
    }
}



exports.deliverOrder = async (req, res) => {
    const { LaundrymanOrderMapping, Login, EmployeeLogin, OrderNotifications, OrderHeader, PartnerOrderTrack, OrderDetails, OrderHistory, ProductMaster, Address, LaundryServiceFlow, CartDetails, CartHeader, EmployeeRoleMaster, db1Conn } = await getModels();
    //     let flag = 1;
    console.log('\ndeliveryboy.controller.deliverOrder  triggered -->');
    console.log("\n\n\n\nREQUEST BODY  %%%%===" + JSON.stringify(req.body, 0, 2));
    const t = await db1Conn.transaction();
    try {
        const { order_detail, emp_id } = req.body;
        // Here status_id 1050 comes from env file and module_id is  hard coded here, it will be dynamic in future
        let laundry_service_flow = await LaundryServiceFlow.findOne({ where: { status_id: deliveryboy_deliver_to_customer_status_id, module_id: 1 } });
        // fetch role_id of role_name=Deliveryboy
        let emp_role_master = await EmployeeRoleMaster.findOne({ where: { emp_role_name: 'Deliveryboy' } });
        console.log("\n\n EMP ROLE MASTER======", JSON.stringify(emp_role_master, 0, 2));

        console.log("\n\nCOMPLETE ORDER-ID======" + order_detail.order_id);
        console.log("\n\nCOMPLETE ORDER USER-ID======" + order_detail.user_id);
        console.log("\n\nITEM_COUNT======" + order_detail.item_count);
        console.log("\n\nTOTAL_PRICE======" + order_detail.total_price);

        const deliveryboy_deliver_order_moment = moment().utc().utcOffset(utcOffsetMins);
        var deliveryboy_deliver_order_date_time = deliveryboy_deliver_order_moment.format("DD/MM/YYYY hh:mm A");
        let date_time_array = deliveryboy_deliver_order_date_time.split(" ");
        let deliveryboy_deliver_order_date = date_time_array[0];
        let deliveryboy_deliver_order_time = date_time_array[1] + " " + date_time_array[2];


        await OrderHeader.update({ status_id: laundry_service_flow.status_id },
            { where: { order_id: order_detail.order_id, user_id: order_detail.user_id } }, { transaction: t });
        console.log("\n\n ORDER HEADER UPDATED WITH DELIVERY BOY ORDER DELIVER COMPLETE STATUS 1050======");

        const orderHistory = await OrderHistory.create(
            {
                order_id: order_detail.order_id, status_id: laundry_service_flow.status_id, status_date: deliveryboy_deliver_order_date, active_flag: laundry_service_flow.status_desc, user_id: order_detail.user_id, emp_id: emp_id, role_id: emp_role_master.emp_role_id, status_time: deliveryboy_deliver_order_time, item_count: order_detail.item_count,
                total_price: order_detail.total_price
            },
            { transaction: t });
        console.log("\n\n DELIVERY BOY DELIVER COMPLATE ORDER HISTORY CREATED======", JSON.stringify(orderHistory, 0, 2));

        const partnerOrderTrack = await PartnerOrderTrack.create(
            {
                order_id: order_detail.order_id, partner_id: emp_id, partner_type: 2, delivery_type: 2, date: deliveryboy_deliver_order_date, time: deliveryboy_deliver_order_time
            },
            { transaction: t });
        console.log("\n\n  PARTNER ORDER TRACK CREATED FOR ORDER DELIVER TO CUSTOMER======", JSON.stringify(partnerOrderTrack, 0, 2));



        t.commit();

        //=======================================================================================

        try {
            // let msg_body = "";
            let user_login = await Login.findOne({ where: { user_id: order_detail.user_id } });
            let lman_order_mapping = await LaundrymanOrderMapping.findOne({ where: { order_id: order_detail.order_id } });
            let lman_login = await EmployeeLogin.findOne({ where: { emp_id: lman_order_mapping.emp_id } });
            let dboy_login = await EmployeeLogin.findOne({ where: { emp_id: emp_id } });

            const msg_body = 'Order No  ' + order_detail.order_id +
                ' has been delivered successfully.';

            const userdorderNotifications = await OrderNotifications.create(
                {
                    order_id: order_detail.order_id,
                    status_id: deliveryBoyDeliverCompleteStatusId,
                    user_id: order_detail.user_id,
                    msg_title: 'Order delivered successfully',
                    msg_body: msg_body,
                    created_at: deliveryboy_deliver_order_date_time
                });


            const dboyorderNotifications = await OrderNotifications.create(
                {
                    order_id: order_detail.order_id,
                    status_id: deliveryBoyDeliverCompleteStatusId,
                    emp_id: emp_id,
                    role_id: 2,
                    msg_title: 'Order delivered successfully',
                    msg_body: msg_body,
                    created_at: deliveryboy_deliver_order_date_time
                });

            const lmanorderNotifications = await OrderNotifications.create(
                {
                    order_id: order_detail.order_id,
                    status_id: deliveryBoyDeliverCompleteStatusId,
                    emp_id: lman_order_mapping.emp_id,
                    role_id: 1,
                    msg_title: 'Order delivered successfully',
                    msg_body: msg_body,
                    created_at: deliveryboy_deliver_order_date_time
                });

            const ufetchBody = {
                notification: {
                    title: "iestre",
                    text: msg_body,
                    click_action: "com.purpuligo.laundry.NotificationActivity"
                },
                data: {
                    order_id: order_detail.order_id,
                    status_id: deliveryBoyDeliverCompleteStatusId,
                    user_id: order_detail.user_id,
                    msg_title: 'Order delivered successfully',
                    msg_body: msg_body,
                    created_at: deliveryboy_deliver_order_date_time
                },
                to: user_login.fcmToken
            };

            const dfetchBody = {
                notification: {
                    title: "iestre",
                    text: msg_body,
                    click_action: "com.maks.iestredeliveryboy.NotificationActivity"
                },
                data: {
                    order_id: order_detail.order_id,
                    status_id: deliveryBoyDeliverCompleteStatusId,
                    emp_id: emp_id,
                    role_id: 2,
                    msg_title: 'Order delivered successfully',
                    msg_body: msg_body,
                    created_at: deliveryboy_deliver_order_date_time
                },
                to: dboy_login.fcmToken
            };

            const lfetchBody = {
                notification: {
                    title: "iestre",
                    text: msg_body,
                    click_action: "com.maks.iestrelaundryman.NotificationActivity"
                },
                data: {
                    order_id: order_detail.order_id,
                    status_id: deliveryBoyDeliverCompleteStatusId,
                    emp_id: lman_order_mapping.emp_id,
                    role_id: 1,
                    msg_title: 'Order delivered successfully',
                    msg_body: msg_body,
                    created_at: deliveryboy_deliver_order_date_time
                },
                to: lman_login.fcmToken
            };
            let result_1 = pushToFirebase(ufetchBody, 3);
            console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO CUSTOMER AFTER ORDER DELIVERY COMPLETE");
            let result_2 = lpushToFirebase(lfetchBody, 3);
            console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO LAUNDERY AFTER ORDER DELIVERY COMPLETE");
            let y = 0;
            var notificationId = setInterval(async function () {
                y = y + 1;
                console.log("\n\n TRY TO SEND NOTIFICATION TO FIREBASE FOR DBOY AFTER ORDER DELIVERY COMPLETE :" + y);
                let result = dpushToFirebase(dfetchBody, 3);
                if (result) {
                    console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DBOY AFTER ORDER DELIVERY COMPLETE");
                    let res = await deleteNotification(order_detail.order_id);
                    console.log("\n\nNOTIFICATION RECORD DELETE COMPLETE :" + res);
                    clearInterval(notificationId);
                }
                // if (y === 1) {
                //     let res = await deleteNotification(order_detail.order_id);
                //     console.log("\n\nNOTIFICATION RECORD DELETE COMPLETE :" + res);
                //     // await OrderNotifications.destroy({ where: { order_id: order_detail.order_id } });
                //     console.log("\n\nNOTIFICATION NOT SEND TO DBOY AFTER ORDER DELIVERY COMPLETE");
                //     clearInterval(notificationId);
                // }
            }, 90000);


        }
        catch (error) {
            console.log("\n\n NOTIFICATION NOT SEND TO DBOY AFTER ORDER DELIVERY COMPLETE ERROR ", error);
        }




        //========================================================================================
        res.json({ status: 1, message: "order deliver by deliveryboy successfully", data: {} })

    }
    catch (error) {
        t.rollback();
        console.log('\ndeliveryboy.controller.deliverOrder', error)
        //res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        throw error;

    }
}




/*
exports.completedOrderList = async (req, res) => {
    try {
        console.log('\nlaundryman.controller.completedOrderList  triggered -->');
        const { OrderHeader, OrderDetails, OrderHistory, ProductMaster, Address, LaundryServiceFlow, CartDetails, CartHeader, db1Conn } = await getModels();
        //let attributes = ['OrderHeader.order_id', 'OrderHeader.user_id', 'OrderHeader.order_date', 'OrderHeader.item_count_upd', 'OrderHeader.expected_delivery_time'];
        let attributes = ['order_id', 'trans_id', 'user_id', 'order_date', 'status_id', 'item_count', 'total_price', 'expected_delivery_time', 'actual_delivery_time'];
        let odr_ids = [];
        let orderHistories;
        let orderids = await OrderHistory.findAll({
            where: { emp_id: req.body.emp_id, status_id: 1040 }
        });


        console.log("\n\norderids======" + orderids);
        await Promise.all(orderids.map(async (element) => {
            console.log("\n\nORDER_ID======" + element.order_id);
            odr_ids.push(element.order_id);
        }));
        console.log("\n\nodr_ids======" + odr_ids);
        if (odr_ids.length > 0) {
            orderHistories = await OrderHistory.findAll({
                where: {
                    emp_id: req.body.emp_id, status_id: 1030,
                    [op.not]: [
                        { order_id: odr_ids },
                    ]

                }
            });
        }
        else {
            orderHistories = await OrderHistory.findAll({
                where: { emp_id: req.body.emp_id, status_id: 1030 }
            });
        }


        let orders = [];
        console.log("\n\nCOMPLETE ORDERS FROM ORDER HISTORIES======", JSON.stringify(orderHistories, 0, 2));
        if (orderHistories) {

            await Promise.all(orderHistories.map(async (element) => {
                //         console.log('\nUser--' + element.user_id + ' having  orderId ' + element.order_id);
                //         // here select all order deatils list  from order_details  based on user_id and order_id
                //         let orderDetails = await OrderDetails.findAll({ where: { user_id: element.user_id, order_id: element.order_id } });
                console.log("\n\nORDER_ID======" + element.order_id);

                const order = await OrderHeader.findOne({
                    where: { order_id: element.order_id }, attributes: attributes,
                    include: [
                        { model: OrderDetails, as: 'order_details' },
                        { model: OrderHistory, where: { status_id: 1030 }, as: 'order_history', attributes: ['status_date', 'status_time'] }
                    ]
                });
                orders.push(order);

            }));

            await Promise.all(orders.map(async (element) => {
                console.log("\n\n COMPLETE ORDER Details======", JSON.stringify(element.order_details, 0, 2));

                await Promise.all(element.order_details.map(async (element_1) => {
                    console.log("\n\n INV ORDER Details======", JSON.stringify(element_1, 0, 2));

                    // here select price from price_master based on prod_id
                    // let product = await PriceMaster.findOne({ where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' } });
                    // console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
                    // console.log('\nproduct ' + element_1.prod_id + ' price of ' + product.prod_price);



                    // here select product_name from product_master based on prod_id
                    let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
                    console.log("PRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));


                    // let quantity = parseInt(element.quantity);
                    // console.log('\nProduct quantity ' + quantity);
                    // let total_price = parseFloat(product.prod_price) * quantity;

                    // console.log('\nTotal Price ' + total_price);
                    element_1.setDataValue('prod_name', product_master.prod_name);
                    element_1.setDataValue('prod_code', product_master.prod_code);
                    // element_1.setDataValue('per_unit_price', product.prod_price);
                    // element.setDataValue('total_price', total_price.toFixed(2));

                }));

            }));

            console.log("\n\nCOMPLETE ORDER LIST DETAILS======", JSON.stringify(orders, 0, 2));
            res.json({ status: 1, message: "complete order details", data: orders });

        }
        else {
            // res.json({ status: 1, message: "cart details", data: "no cart" });
            res.json({ status: 0, message: "empty order details list", data: {} });
        }
    }
    catch (error) {
        console.log(error);
        res.status(404).send(error);
    }
}

*/

let deleteNotification = async (order_id) => {
    const { OrderNotifications, db1Conn } = await getModels();
    let result = await OrderNotifications.destroy({ where: { order_id: order_id } });
    if (result) {
        return true;
    }
    else {
        return false;
    }

}