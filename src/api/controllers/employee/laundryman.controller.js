// global import
const HttpStatus = require('http-status-codes');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const axios = require('axios');
const Sequelize = require("sequelize");
const op = Sequelize.Op;
// local imports
const { getModels } = require('../../models');
const { pushToFirebase, dpushToFirebase, lpushToFirebase } = require('../../lib/fcm_helper');
const env = require('../../../env');
const utcOffsetMins = env.env.utcOffsetMins;
const jwtSecretKey = '123##$$)(***&';

const laundry_man_accept_status_id = env.service_flow.laundry_man_accept_status_id;
const laundry_man_complete_status_id = env.service_flow.laundry_man_complete_status_id;

/*
const env = require('../../../env');
const deliveryBoyDeliverCompleteStatusId = env.service_flow.deliveryboy_deliver_to_customer_status_id;// 1050
const deliveryBoyAcceptOrderFromLmanStatusId = env.service_flow.deliveryboy_accept_from_laundryman_status_id;// 1040
    order_creation_id: 1001,
    laundry_man_accept_status_id: 1020,
    laundry_man_complete_status_id: 1030,
    deliveryboy_accept_from_customer_status_id: 1010,
    deliveryboy_accept_from_laundryman_status_id: 1040,
    deliveryboy_deliver_to_customer_status_id: 1050

*/

// LaundryMan Login
exports.laundrymanLogin = async (req, res) => {
    const { EmployeeLogin, db1Conn } = await getModels();
    console.log('\nlaundryman.controller.laundrymanLogin  triggered -->');

    console.log(`req.body: `, req.body);
    // phoneno is same as otp here
    const { emp_login_id, emp_password, fcmToken } = req.body;
    // let fcmToken = "fcmToken";
    console.log(`Employee Login-Id:  `, emp_login_id);
    console.log(`Employee Password:  `, emp_password);
    console.log(`Employee fcmToken:  `, fcmToken);
    try {
        // Check employee given login_id and password match in employee_login table or not
        let employee = await EmployeeLogin.findOne({ where: { emp_login_id: emp_login_id, emp_password: emp_password, emp_role_id: 1 } });

        if (employee) {

            console.log(`\n\nEmployee details existing in emplolee_login table...`);

            let access_token1 = employee.access_token;
            console.log('\nPrevious Access Token Updating ...', access_token1);

            if (access_token1) {
                console.log('\nAccess Token Updating ...');
                // create an access token
                const access_token = jwt.sign({ id: employee.emp_id }, jwtSecretKey, {
                    expiresIn: '365d', // expires in X secs
                });
                let lastAccessDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm A");
                await EmployeeLogin.update(
                    { access_token: access_token, emp_last_login_date_time: lastAccessDate, fcmToken: fcmToken },
                    { where: { emp_id: employee.emp_id } }
                )

                let employeeLoginDetails = await EmployeeLogin.findOne({ where: { emp_id: employee.emp_id } });
                console.log('\nUpdating new Employee access token Returning ...');
                console.log(`\n\nEmployee Access Token: `, access_token);
                res.json({ status: 1, message: "employee_login details with existing access_token", data: employeeLoginDetails })

            } else {
                console.log(`\nEmployee access_token not exist in employee_login...`);
                // create an access token
                const access_token = jwt.sign({ id: employee.emp_id }, jwtSecretKey, {
                    expiresIn: '365d', // expires in X secs
                });

                let lastAccessDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm A");
                await EmployeeLogin.update(
                    { access_token: access_token, emp_last_login_date_time: lastAccessDate, fcmToken: fcmToken },
                    { where: { emp_id: employee.emp_id } }
                )

                let employeeLoginDetails = await EmployeeLogin.findOne({ where: { emp_id: employee.emp_id } });
                res.json({ status: 1, message: "employee_login details with new access_token", data: employeeLoginDetails })

            }
        }
        else {
            console.log('\n\n Employee given login_id and password does not match, Sending Error Response...');
            // const errorResponse = {
            //     status: 0,
            //     message: 'login_id  and password not match. You are unauthenticated',
            //     data: {}
            // };
            // return res.status(HttpStatus.BAD_REQUEST).send(errorResponse);
            res.json({ status: 0, message: "login_id  and password not match. You are unauthenticated", data: {} });
        }

    } catch (error) {
        console.log(error);
        //res.status(404).send(error);
        // res.json({ status: 0, message: "auth error", data: JSON.stringify(error, 0, 2) });
        throw error
    }
}


// LaundryMan Logout 
exports.laundrymanLogout = async (req, res) => {
    console.log('\nlaundryman.controller.logout triggered -->');
    const { EmployeeLogin, db1Conn } = await getModels();

    /*  {
        emp_id: req.emp_id
    }
    */
    try {
        let employee = await EmployeeLogin.findOne({ where: { emp_id: req.body.emp_id } });
        if (!employee) {
            const errorResponse = {
                status: 0,
                message: 'Invalid user',
                data: {}
            };
            return res.status(HttpStatus.BAD_REQUEST).send(errorResponse);
        }
        else {
            let deleteAccessToken = await EmployeeLogin.update(
                { access_token: null },
                { where: { emp_id: req.body.emp_id } }
            );
            // const successResponse = {
            //     status: 1,
            //     message: 'User logout successful.',
            //     data: deleteAccessToken
            // };
            // return res.status(HttpStatus.OK).send(successResponse);
            if (deleteAccessToken) {
                res.json({ status: 1, message: "'User logout successful", data: {} });
            }
            else {
                res.json({ status: 0, message: "'User already logout ", data: {} });
            }
        }

    }
    catch (error) {
        console.log('\nUserController.logout error', error)
        throw error;

    }
}

// Code To Be Block
exports.newOrderListXYZ = async (req, res) => {
    try {
        console.log('\nlaundryman.controller.newOrderList  triggered -->');
        const { ProductMaster, OrderHeader, OrderDetails, LaundrymanOrderMapping, db1Conn } = await getModels();
        let newOrderList = await LaundrymanOrderMapping.findAll({
            where: { emp_id: req.body.emp_id, active_status: '1' }
        });

        if (newOrderList) {
            await Promise.all(newOrderList.map(async (element) => {
                console.log('\nEmployeeId : ' + element.emp_id + 'mapping with  orderId :' + element.order_id);
                // here select all order deatils list  from order_details  based on user_id and order_id
                let orderDetails = await OrderDetails.findAll({ where: { order_id: element.order_id } });
                console.log("\n\nORDER DETAILS======", JSON.stringify(orderDetails, 0, 2));

                await Promise.all(orderDetails.map(async (element_1) => {
                    // here select product_name from product_master based on prod_id
                    let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
                    console.log("\n\nPRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));
                    let product = await PriceMaster.findOne({ where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' } });
                    console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
                    console.log('\nproduct ' + element_.prod_id + ' price of ' + product.prod_price);


                    element_1.setDataValue('prod_name', product_master.prod_name);
                    element_1.setDataValue('prod_code', product_master.prod_code);
                    element_1.setDataValue('unit_price', product.prod_price);

                }))
                element.setDataValue('order_details', orderDetails);
            }));
            res.json({ status: 1, message: "order details", data: newOrderList });

        }
        else {
            // res.json({ status: 1, message: "cart details", data: "no cart" });
            res.json({ status: 0, message: "no order details", data: {} });
        }
    }
    catch (error) {
        console.log(error);
        //res.status(404).send(error);
        //res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        throw error
    }
}



//======================================
//Laundyman New Order List
//=================================
exports.newOrderList = async (req, res) => {
    try {

        console.log('\nlaundryman.controller.newOrderList  triggered -->');
        const { PriceMaster, ProductMaster, OrderHeader, OrderDetails, LaundrymanOrderMapping, UserMaster, db1Conn } = await getModels();
        let newOrderList = await LaundrymanOrderMapping.findAll({
            where: { emp_id: req.body.emp_id, active_status: '1' }
        });  // retrive those records from laundryman_order_mapping table whose active_status=1 made by store_procedure

        if (newOrderList) {
            await Promise.all(newOrderList.map(async (element) => {
                console.log('\nEmployeeId : ' + element.emp_id + 'mapping with  orderId :' + element.order_id);
                // here select record  from order_header  based on  order_id
                let orderHeader = await OrderHeader.findOne({ where: { order_id: element.order_id } });
                console.log("\n\nORDER HEADER DETAILS======", JSON.stringify(orderHeader, 0, 2));

                // here select customer_name from user_master based on user_id found in order_header
                // let userDetails = await UserMaster.findOne({ where: { user_id: orderHeader.user_id } });
                // console.log("\n\nUSER MASTER DETAILS======", JSON.stringify(userDetails, 0, 2));
                // let customer_name = "";
                // if (userDetails.login_type === 'Otp') {
                //     customer_name = userDetails.user_fname
                // }
                // else {
                //     customer_name = userDetails.user_fname + " " + userDetails.user_lname;
                // }

                element.setDataValue('total_quantity', orderHeader.item_count);
                element.setDataValue('order_date', orderHeader.order_date);
                element.setDataValue('category_id', orderHeader.category_id);
                // element.setDataValue('customer_name', customer_name);
                let orderDetails = await OrderDetails.findAll({ where: { order_id: element.order_id } });
                console.log("\n\nORDER DETAILS======", JSON.stringify(orderDetails, 0, 2));

                await Promise.all(orderDetails.map(async (element_1) => {
                    // here select product_name from product_master based on prod_id
                    let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
                    console.log("\n\nPRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));
                    let product = await PriceMaster.findOne({ where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' } });
                    console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
                    console.log('\nproduct ' + element_1.prod_id + ' price of ' + product.prod_price);

                    element_1.setDataValue('prod_name', product_master.prod_name);
                    element_1.setDataValue('prod_code', product_master.prod_code);
                    element_1.setDataValue('unit_price', product.prod_price);


                }))
                element.setDataValue('order_details', orderDetails);

            }));
            res.json({ status: 1, message: "new order list details", data: newOrderList });

        }
        else {
            // res.json({ status: 1, message: "cart details", data: "no cart" });
            res.json({ status: 0, message: "no order details", data: {} });
        }
    }
    catch (error) {
        console.log(error);
        //res.status(404).send(error);
        //res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        throw error
    }
}








//acceptOrder
exports.acceptOrder = async (req, res) => {
    const { OrderHeader, OrderDetails, OrderHistory, LaundrymanOrderMapping, PriceMaster, LaundryServiceFlow, db1Conn, EmployeeRoleMaster } = await getModels();
    console.log('\nlaundryman.controller.acceptOrder  triggered -->');
    console.log(`\n req.body: `, req.body);

    const { order_details, order_id, emp_id, user_id } = req.body;
    console.log("\n\nORDER ID======", order_id);
    console.log("\n\nEMPLOYEE ID======", emp_id);
    console.log("\n\nORDER DETAILS======", JSON.stringify(order_details, 0, 2));


    let gtotal_price = 0;
    let gtotal_quantity = 0;

    const t = await db1Conn.transaction();

    try {

        var format = 'hh:mm:ss A'
        const startMoment_1 = moment('12:00:00 AM', format).utc().utcOffset(utcOffsetMins);
        const endMoment_1 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);

        console.log("\n\n startMoment_1===", startMoment_1);
        console.log("\n\n endMoment_1===", endMoment_1);
        const startMoment_2 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);
        const endMoment_2 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);

        const startMoment_3 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);;
        const endMoment_3 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);;

        const startMoment_4 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);;
        const endMoment_4 = moment('23:59:59 PM', format).utc().utcOffset(utcOffsetMins);;


        let delivery_order_date_time = "";
        const order_moment = moment().utc().utcOffset(utcOffsetMins);
        // const cancel_order_moment = order_moment.clone().add(15, 'minutes');

        if (order_moment.isSameOrAfter(startMoment_1) && order_moment.isBefore(endMoment_1)) {
            // const delivery_date_string = order_moment.clone().format("DD/MM/YYYY");
            // const delivery_time_string = '09:00 pm';
            // delivery_order_date_time = delivery_date_string + " " + delivery_time_string;
            delivery_order_date_time = "Your ironed clothes will be delivered between 5 pm and 8 pm today."
            console.log("\n\n BLOCK ======11111", delivery_order_date_time);
        }
        else if (order_moment.isSameOrAfter(startMoment_2) && order_moment.isBefore(endMoment_2)) {
            //const delivery_date_string = order_moment.clone().format("DD/MM/YYYY");
            //const delivery_time_string = '09:00 pm';
            // delivery_order_date_time = delivery_date_string + " " + delivery_time_string;
            delivery_order_date_time = "Your ironed clothes will be delivered between 5 pm and 8 pm today."
            console.log("\n\n BLOCK ======2222", delivery_order_date_time);
        }
        else if (order_moment.isSameOrAfter(startMoment_3) && order_moment.isBefore(endMoment_3)) {
            // const delivery_date_string = order_moment.clone().add(1, 'day').format("DD/MM/YYYY");
            // console.log("\n\n delivery_date_string ======333", delivery_date_string);
            // const delivery_time_string = '12:00 pm';
            // delivery_order_date_time = delivery_date_string + " " + delivery_time_string;
            delivery_order_date_time = "Your ironed clothes will be delivered by today 5 PM."

            console.log("\n\n BLOCK ======333", delivery_order_date_time);
        }
        else if (order_moment.isSameOrAfter(startMoment_4) && order_moment.isBefore(endMoment_4)) {
            // const delivery_date_string = order_moment.clone().add(1, 'day').format("DD/MM/YYYY");
            // console.log("\n\n delivery_date_string ======333", delivery_date_string);
            // const delivery_time_string = '09:00 pm';
            // delivery_order_date_time = delivery_date_string + " " + delivery_time_string;
            delivery_order_date_time = "Your ironed clothes will be delivered by today 5 PM."
            console.log("\n\n BLOCK ======333", delivery_order_date_time);
        }
        var order_date_time = order_moment.format("DD/MM/YYYY hh:mm A");
        let order_date_time_array = order_date_time.split(" ");
        let order_date = order_date_time_array[0];
        let order_time = order_date_time_array[1] + " " + order_date_time_array[2];

        // var cancel_date_time = cancel_order_moment.format("DD/MM/YYYY hh:mm a");
        // let cancel_date_time_array = cancel_date_time.split(" ");
        // let cancel_date = cancel_date_time_array[0];
        // let cancel_time = cancel_date_time_array[1] + " " + cancel_date_time_array[2];

        console.log("\n\n order_date_time ======", order_date_time);
        console.log("\n\n order_date  ======", order_date);

        console.log("\n\n order_time  ======", order_time);


        // console.log("\n\n cancel_date_time ======", cancel_date_time);
        // console.log("\n\n cancel_date  ======", cancel_date);

        // console.log("\n\n cancel_time  ======", cancel_time);


        console.log("\n\n ORDER MONENT ======", order_moment);
        // console.log("\n\nCANCEL  ORDER MONENT ======", cancel_order_moment);
        console.log("\n\n DELIVERY ORDER MONENT ======", delivery_order_date_time);



        // Here status_id comes from env file and module_id is  hard coded here, it will be dynamic in future
        let laundry_service_flow = await LaundryServiceFlow.findOne({ where: { status_id: laundry_man_accept_status_id, module_id: 1 } }, { transaction: t });
        console.log("\n\n LAUNDRY SERVICE FLOW======", JSON.stringify(laundry_service_flow, 0, 2));
        // fetch role_id of role_name=Laundryman
        let emp_role_master = await EmployeeRoleMaster.findOne({ where: { emp_role_name: 'Laundryman' } }, { transaction: t });
        console.log("\n\n EMP ROLE MASTER======", JSON.stringify(emp_role_master, 0, 2));


        const laundry_man_accept_order_moment = moment().utc().utcOffset(utcOffsetMins);
        var laundry_man_accept_order_date_time = laundry_man_accept_order_moment.format("DD/MM/YYYY hh:mm A");
        let date_time_array = laundry_man_accept_order_date_time.split(" ");
        let laundry_man_accept_order_date = date_time_array[0];
        let laundry_man_accept_order_time = date_time_array[1] + " " + date_time_array[2];

        await Promise.all(order_details.map(async (element) => {
            console.log("\n\n ELEMENT OF ORDER Details======", JSON.stringify(element, 0, 2));

            // here select price from price_master based on prod_id
            let product = await PriceMaster.findOne({ where: { prod_id: element.prod_id, prod_status: 'CurPrice' } }, { transaction: t });
            console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
            console.log('\nproduct ' + element.prod_id + ' price of ' + product.prod_price);
            let quantity = parseInt(element.quantity);
            console.log('\nProduct quantity ' + quantity);
            let total_price = parseFloat(product.prod_price) * quantity;
            console.log('\nTotal Price ' + total_price);
            gtotal_price = gtotal_price + total_price;
            gtotal_quantity = gtotal_quantity + quantity;
            if (Number(total_price) === total_price && total_price % 1 === 0) {
                console.log('\nINTEGER ');
            }
            else {
                console.log('\nFLOAT');
            }

            await OrderDetails.update(
                { lman_order_accept_quantity: quantity },
                { where: { order_id: order_id, prod_id: element.prod_id } }, { transaction: t });
            console.log("\n\n ORDER DETAILS LAUNDRY MAN ACCEPT ORDER QUANTITY UPDATED======");


            // element_1.setDataValue('prod_name', product_master.prod_name);
            // element_1.setDataValue('prod_code', product_master.prod_code);
            // element_1.setDataValue('per_unit_price', product.prod_price);
            // // element.setDataValue('total_price', total_price.toFixed(2));



        }));

        console.log('\nGRAND TOTAL PRICE ' + gtotal_price);
        console.log('\nGRAND TOTAL QUANTITY ' + gtotal_quantity);

        let gt_price = (Math.floor(gtotal_price * 100) / 100).toFixed(2);
        console.log('\nGRAND TOTAL PRICE gt_price' + gt_price);
        if (Number(gtotal_price) === gtotal_price && gtotal_price % 1 === 0) {
            console.log('\nGRAND TOTAL PRICE IS INTEGER ');
        }
        else {
            console.log('\nGRAND TOTAL PRICE IS FLOAT');
        }

        const orderHistory = await OrderHistory.create(
            {
                order_id: order_id, status_id: laundry_service_flow.status_id, status_date: laundry_man_accept_order_date, active_flag: laundry_service_flow.status_desc, user_id: user_id, emp_id: emp_id, role_id: emp_role_master.emp_role_id, status_time: laundry_man_accept_order_time, item_count: gtotal_quantity,
                total_price: gt_price
            },
            { transaction: t });
        console.log("\n\n ORDER HISTORY CREATED======", JSON.stringify(orderHistory, 0, 2));

        // await OrderHeader.update(
        //     { status_id: laundry_service_flow.status_id, item_count: gtotal_quantity, total_price: gt_price },
        //     { where: { order_id: order_id, user_id: user_id } }, { transaction: t });
        // console.log("\n\n ORDER HEADER UPDATED======");

        await OrderHeader.update(
            { status_id: laundry_service_flow.status_id, expected_delivery_time: delivery_order_date_time },
            { where: { order_id: order_id, user_id: user_id } }, { transaction: t });
        console.log("\n\n ORDER HEADER UPDATED======");


        await LaundrymanOrderMapping.update(
            { active_status: "0" },
            { where: { order_id: order_id, emp_id: emp_id } }, { transaction: t });
        console.log("\n\n Laundry Man Order Mapping UPDATED======");


        t.commit();
        res.json({ status: 1, message: "order accepted by laundryman successfully", data: {} })


    }
    catch (error) {
        t.rollback()
        console.log(error);
        // res.status(404).send(error);
        throw error;
    }
}

//======================================
//Laundyman Accepted Order List
//=================================

exports.acceptedOrderList = async (req, res) => {
    try {
        console.log('\nlaundryman.controller.acceptedOrderList  triggered -->');
        const { OrderHeader, OrderDetails, OrderHistory, ProductMaster, Address, LaundryServiceFlow, CartDetails, CartHeader, db1Conn } = await getModels();
        //let attributes = ['OrderHeader.order_id', 'OrderHeader.user_id', 'OrderHeader.order_date', 'OrderHeader.item_count_upd', 'OrderHeader.expected_delivery_time'];
        let attributes = ['order_id', 'trans_id', 'user_id', 'order_date', 'status_id', 'item_count', 'total_price', 'expected_delivery_time', 'actual_delivery_time', 'category_id'];
        let odr_ids = [];
        let orderHistories;
        let orderids = await OrderHistory.findAll({
            where: { emp_id: req.body.emp_id, status_id: laundry_man_complete_status_id }
        });

        console.log("\n\norderids======" + orderids);
        await Promise.all(orderids.map(async (element) => {
            console.log("\n\nORDER_ID======" + element.order_id);
            odr_ids.push(element.order_id);
        }));
        console.log("\n\nodr_ids======" + odr_ids);
        if (odr_ids.length > 0) {
            orderHistories = await OrderHistory.findAll({
                where: {
                    emp_id: req.body.emp_id, status_id: 1020,
                    [op.not]: [
                        { order_id: odr_ids },
                    ]

                }
            });
        }
        else {
            orderHistories = await OrderHistory.findAll({
                where: { emp_id: req.body.emp_id, status_id: 1020 }
            });
        }





        // let orderids = await OrderHistory.findAll({
        //     where: { emp_id: req.body.emp_id, status_id: 1030 }
        // });


        let orders = [];
        console.log("\n\nORDER HISTORIES======", JSON.stringify(orderHistories, 0, 2));
        if (orderHistories) {

            await Promise.all(orderHistories.map(async (element) => {
                //         console.log('\nUser--' + element.user_id + ' having  orderId ' + element.order_id);
                //         // here select all order deatils list  from order_details  based on user_id and order_id
                //         let orderDetails = await OrderDetails.findAll({ where: { user_id: element.user_id, order_id: element.order_id } });
                console.log("\n\nORDER_ID======" + element.order_id);

                const order = await OrderHeader.findOne({
                    where: { order_id: element.order_id }, attributes: attributes,
                    include: [
                        { model: OrderDetails, as: 'order_details' },
                        { model: OrderHistory, where: { status_id: 1020 }, as: 'order_history', attributes: ['status_date', 'status_time'] }
                    ]
                });
                orders.push(order);

            }));

            await Promise.all(orders.map(async (element) => {
                console.log("\n\n ORDER Details======", JSON.stringify(element.order_details, 0, 2));

                await Promise.all(element.order_details.map(async (element_1) => {
                    console.log("\n\n INV ORDER Details======", JSON.stringify(element_1, 0, 2));

                    // here select price from price_master based on prod_id
                    // let product = await PriceMaster.findOne({ where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' } });
                    // console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
                    // console.log('\nproduct ' + element_1.prod_id + ' price of ' + product.prod_price);



                    // here select product_name from product_master based on prod_id
                    let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
                    console.log("PRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));


                    // let quantity = parseInt(element.quantity);
                    // console.log('\nProduct quantity ' + quantity);
                    // let total_price = parseFloat(product.prod_price) * quantity;

                    // console.log('\nTotal Price ' + total_price);
                    element_1.setDataValue('prod_name', product_master.prod_name);
                    element_1.setDataValue('prod_code', product_master.prod_code);
                    // element_1.setDataValue('per_unit_price', product.prod_price);
                    // element.setDataValue('total_price', total_price.toFixed(2));

                }));

            }));

            console.log("\n\nORDER LIST DETAILS======", JSON.stringify(orders, 0, 2));
            res.json({ status: 1, message: "accepted order details list", data: orders });

        }
        else {
            // res.json({ status: 1, message: "cart details", data: "no cart" });
            res.json({ status: 0, message: "empty order accepted list ", data: {} });
        }
    }
    catch (error) {
        console.log(error);
        res.status(404).send(error);
    }
}



exports.completeOrder = async (req, res) => {
    const { Employee, EmployeeLogin, DelboyOrderMapping, OrderNotifications, OrderHeader, PartnerOrderTrack, OrderDetails, OrderHistory, ProductMaster, Address, LaundryServiceFlow, LaundrymanOrderMapping, CartHeader, EmployeeRoleMaster, db1Conn } = await getModels();
    //     let flag = 1;
    console.log('\nlaundryman.controller.completeOrder  triggered -->');
    console.log("\n\n\n\nREQUEST BODY  %%%%===" + JSON.stringify(req.body, 0, 2));
    const t = await db1Conn.transaction();
    try {
        const { order_detail, emp_id } = req.body;
        // Here status_id comes from env file and module_id is  hard coded here, it will be dynamic in future
        let laundry_service_flow = await LaundryServiceFlow.findOne({ where: { status_id: laundry_man_complete_status_id, module_id: 1 } });
        // fetch role_id of role_name=Laundryman
        let emp_role_master = await EmployeeRoleMaster.findOne({ where: { emp_role_name: 'Laundryman' } });
        console.log("\n\n EMP ROLE MASTER======", JSON.stringify(emp_role_master, 0, 2));

        console.log("\n\nCOMPLETE ORDER-ID======" + order_detail.order_id);
        console.log("\n\nCOMPLETE ORDER USER-ID======" + order_detail.user_id);
        console.log("\n\nITEM_COUNT======" + order_detail.item_count);
        console.log("\n\nTOTAL_PRICE======" + order_detail.total_price);

        const laundry_man_complete_order_moment = moment().utc().utcOffset(utcOffsetMins);
        var laundry_man_complete_order_date_time = laundry_man_complete_order_moment.format("DD/MM/YYYY hh:mm A");
        let date_time_array = laundry_man_complete_order_date_time.split(" ");
        let laundry_man_complete_order_date = date_time_array[0];
        let laundry_man_complete_order_time = date_time_array[1] + " " + date_time_array[2];


        const orderHeader = await OrderHeader.update({ status_id: laundry_service_flow.status_id },
            { where: { order_id: order_detail.order_id, user_id: order_detail.user_id } }, { transaction: t });
        console.log("\n\n ORDER HEADER UPDATED WITH LAUNDRYMAN ORDER COMPLETE STATUS 1030======");

        const orderHistory = await OrderHistory.create(
            {
                order_id: order_detail.order_id, status_id: laundry_service_flow.status_id, status_date: laundry_man_complete_order_date, active_flag: laundry_service_flow.status_desc, user_id: order_detail.user_id, emp_id: emp_id, role_id: emp_role_master.emp_role_id, status_time: laundry_man_complete_order_time, item_count: order_detail.item_count,
                total_price: order_detail.total_price
            },
            { transaction: t });
        console.log("\n\n LAUNDRU MAN COMPLATE ORDER HISTORY CREATED======", JSON.stringify(orderHistory, 0, 2));


        const partnerOrderTrack = await PartnerOrderTrack.create(
            {
                order_id: order_detail.order_id, partner_id: emp_id, partner_type: 1, delivery_type: 0, date: laundry_man_complete_order_date, time: laundry_man_complete_order_time
            },
            { transaction: t });
        console.log("\n\n  PARTNER ORDER TRACK CREATED FOR LAUNDRY MAN COMPLETE ORDER======", JSON.stringify(partnerOrderTrack, 0, 2));

        t.commit();

        let x = 0;
        var refreshId1 = setInterval(function () {
            x = x + 1;
            console.log("\n\n TRY TO SEND MESSAGE TO DELIVERY BOY AFTER LMAN ORDER COMPLETE FOR TIME :" + x);
            var result = sendSMS1(order_detail.order_id, emp_id);
            if (result) {
                console.log("\n\n MESSAGE SEND  SUCCESSFULLY TO DELBOY AND CUSTOMER");
                clearInterval(refreshId1);

            }
            if (x === 3) {
                console.log("\n\nTRY FOR TRHEE TIMES, MESSAGE NOT SEND  SUCCESSFULLY TO DELBOY AND CUSTOMER");
                clearInterval(refreshId1);
            }

        }, 5000);


        try {
            let delboy_name = "";
            let delboy_phoneno = "";
            let delby_fcmToken = "";
            let lman_name = "";
            let lman_phoneno = "";
            let lman_fcmToken = "";
            let msg_body_forDBoy = "";
            let msg_body_forLMan = "";
            let empl = await Employee.findOne({ where: { emp_id: emp_id } });
            lman_name = empl.emp_fname + " " + empl.emp_lname;
            lman_phoneno = empl.emp_contact_no;
            let empl_login = await EmployeeLogin.findOne({ where: { emp_id: emp_id } });
            lman_fcmToken = empl_login.fcmToken;


            let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: order_detail.order_id, delivery_type: 2 } });
            if (dboy_order_mapping) {
                let employee = await Employee.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                delboy_name = employee.emp_fname + " " + employee.emp_lname;
                delboy_phoneno = employee.emp_contact_no;
            }
            let employee_login = await EmployeeLogin.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
            delby_fcmToken = employee_login.fcmToken;

            msg_body_forDBoy = 'Pick up clothes for Order No -' + order_detail.order_id +
                ' from Laundery partner ' + lman_name + ', phone no-' + lman_phoneno;


            msg_body_forLMan = 'Order No -' + order_detail.order_id +
                'will be pick up by  ' + delboy_name + ', phone no-' + delboy_phoneno;
            console.log("\nMSG FOR DELIVERYBOY NOTIFICATION AFTER ORDER COMPLETE FROM LMAN :" + msg_body_forDBoy);
            console.log("\nDELIVERYBOY FCMTOKEN :" + delby_fcmToken);


            console.log("\nMSG FOR LMAN NOTIFICATION AFTER ORDER COMPLETE WHO DBOY WILL COLLECT :" + msg_body_forLMan);
            console.log("\nLMAN FCMTOKEN :" + lman_fcmToken);


            const lorderNotifications = await OrderNotifications.create(
                {
                    order_id: order_detail.order_id,
                    status_id: laundry_man_complete_status_id,
                    emp_id: emp_id,
                    role_id: 1,
                    msg_title: 'Order has to be pick up by delivery boy',
                    msg_body: msg_body_forLMan,
                    created_at: laundry_man_complete_order_date_time
                });


            const dorderNotifications = await OrderNotifications.create(
                {
                    order_id: order_detail.order_id,
                    status_id: laundry_man_complete_status_id,
                    emp_id: dboy_order_mapping.emp_id,
                    role_id: 2,
                    msg_title: 'Order has to be delivered by laundry man',
                    msg_body: msg_body_forDBoy,
                    created_at: laundry_man_complete_order_date_time
                });


            const fetchBody = {
                notification: {
                    title: "iestre",
                    text: msg_body_forLMan,
                    click_action: "com.maks.iestrelaundryman.NotificationActivity"
                },
                data: {
                    orderId: order_detail.order_id
                },
                to: lman_fcmToken
            };

            const fetchBody1 = {
                notification: {
                    title: "iestre",
                    text: msg_body_forDBoy,
                    click_action: "com.maks.iestredeliveryboy.NotificationActivity"
                },
                data: {
                    orderId: order_detail.order_id
                },
                to: delby_fcmToken
            };
            let result = dpushToFirebase(fetchBody1, 3);
            console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY FOR ORDER PICKUP AFTER ORDER COMPLETE BY LMAN");
            let y = 0;
            var notificationId = setInterval(function () {
                y = y + 1;
                console.log("\n\n TRY TO SEND NOTIFICATION TO FIREBASE :" + y);
                let result = lpushToFirebase(fetchBody, 3);
                if (result) {
                    console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO LAUNDRY MAN AFTER ORDER COMPLETE");

                    clearInterval(notificationId);
                }
                // if (y === 1) {
                //     console.log("\n\nNOTIFICATION NOT SEND  SUCCESSFULLY TO LAUNDRY MAN AFTER ORDER COMPLETE");
                //     clearInterval(notificationId);
                // }

            }, 120000);

        }
        catch (error) {
            console.log("\n\n NOTIFICATION ERROR ", error);
        }




        res.json({ status: 1, message: "order complete by laundryman successfully", data: {} })

    }
    catch (error) {
        t.rollback();
        console.log('\nlaundryman.controller.completeOrder error', error)
        //res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        throw error;

    }
}





exports.completedOrderList = async (req, res) => {
    try {
        console.log('\nlaundryman.controller.completedOrderList  triggered -->');
        const { OrderHeader, OrderDetails, OrderHistory, ProductMaster, Address, LaundryServiceFlow, CartDetails, CartHeader, db1Conn } = await getModels();
        //let attributes = ['OrderHeader.order_id', 'OrderHeader.user_id', 'OrderHeader.order_date', 'OrderHeader.item_count_upd', 'OrderHeader.expected_delivery_time'];
        let attributes = ['order_id', 'trans_id', 'user_id', 'order_date', 'status_id', 'item_count', 'total_price', 'expected_delivery_time', 'actual_delivery_time', 'category_id'];
        let odr_ids = [];
        let orderHistories;
        let orderids = await OrderHistory.findAll({
            where: { emp_id: req.body.emp_id, status_id: 1040 }
        });


        console.log("\n\norderids======" + orderids);
        await Promise.all(orderids.map(async (element) => {
            console.log("\n\nORDER_ID======" + element.order_id);
            odr_ids.push(element.order_id);
        }));
        console.log("\n\nodr_ids======" + odr_ids);
        if (odr_ids.length > 0) {
            orderHistories = await OrderHistory.findAll({
                where: {
                    emp_id: req.body.emp_id, status_id: laundry_man_complete_status_id,
                    [op.not]: [
                        { order_id: odr_ids },
                    ]

                }
            });
        }
        else {
            orderHistories = await OrderHistory.findAll({
                where: { emp_id: req.body.emp_id, status_id: laundry_man_complete_status_id }
            });
        }


        let orders = [];
        console.log("\n\nCOMPLETE ORDERS FROM ORDER HISTORIES======", JSON.stringify(orderHistories, 0, 2));
        if (orderHistories) {

            await Promise.all(orderHistories.map(async (element) => {
                //         console.log('\nUser--' + element.user_id + ' having  orderId ' + element.order_id);
                //         // here select all order deatils list  from order_details  based on user_id and order_id
                //         let orderDetails = await OrderDetails.findAll({ where: { user_id: element.user_id, order_id: element.order_id } });
                console.log("\n\nORDER_ID======" + element.order_id);

                const order = await OrderHeader.findOne({
                    where: { order_id: element.order_id }, attributes: attributes,
                    include: [
                        { model: OrderDetails, as: 'order_details' },
                        { model: OrderHistory, where: { status_id: laundry_man_complete_status_id }, as: 'order_history', attributes: ['status_date', 'status_time'] }
                    ]
                });
                orders.push(order);

            }));

            await Promise.all(orders.map(async (element) => {
                console.log("\n\n COMPLETE ORDER Details======", JSON.stringify(element.order_details, 0, 2));

                await Promise.all(element.order_details.map(async (element_1) => {
                    console.log("\n\n INV ORDER Details======", JSON.stringify(element_1, 0, 2));

                    // here select price from price_master based on prod_id
                    // let product = await PriceMaster.findOne({ where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' } });
                    // console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
                    // console.log('\nproduct ' + element_1.prod_id + ' price of ' + product.prod_price);



                    // here select product_name from product_master based on prod_id
                    let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
                    console.log("PRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));


                    // let quantity = parseInt(element.quantity);
                    // console.log('\nProduct quantity ' + quantity);
                    // let total_price = parseFloat(product.prod_price) * quantity;

                    // console.log('\nTotal Price ' + total_price);
                    element_1.setDataValue('prod_name', product_master.prod_name);
                    element_1.setDataValue('prod_code', product_master.prod_code);
                    // element_1.setDataValue('per_unit_price', product.prod_price);
                    // element.setDataValue('total_price', total_price.toFixed(2));

                }));

            }));

            console.log("\n\nCOMPLETE ORDER LIST DETAILS======", JSON.stringify(orders, 0, 2));
            res.json({ status: 1, message: "complete order details", data: orders });

        }
        else {
            // res.json({ status: 1, message: "cart details", data: "no cart" });
            res.json({ status: 0, message: "empty order details list", data: {} });
        }
    }
    catch (error) {
        console.log(error);
        res.status(404).send(error);
    }
}




// send sms to delivery boy after order completion
sendSMS1 = async (order_id, lman_emp_id) => {
    console.log('\nlaundryman.controller.sendSMS1 triggered -->');
    const { UserMaster, DelboyOrderMapping, Employee, db1Conn } = await getModels();
    console.log("\nORDER_ID :" + order_id);
    console.log("\nLAUNDRYMAN_ID :" + lman_emp_id);

    /*  {
        user_id: req.user_id
    }
    */
    try {
        let lman = await Employee.findOne({ where: { emp_id: lman_emp_id } });
        let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: order_id, delivery_type: 2 } });
        if (lman) {
            let lman_name = lman.emp_fname + " " + lman.emp_lname;
            let lman_phoneno = lman.emp_contact_no;
            let url;
            let url1;

            if (dboy_order_mapping) {
                let delboy = await Employee.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                let delboyname = delboy.emp_fname + " " + delboy.emp_lname;
                let delboy_phoneno = delboy.emp_contact_no;
                url = env.sms.URI + delboy_phoneno + '&text=Pick up clothes for order no- ' + order_id + 'from laundery man ' + lman_name + ', phone no-' + lman_phoneno + '&priority=ndnd&stype=normal';
                let resopnse = await axios.get(url);
                console.log("\nresopnse.data :" + resopnse.data);
                if (resopnse.data) {
                    console.log("\nMSG RETURN  :TRUE");
                    url1 = env.sms.URI + lman_phoneno + '&text=Order no ' + order_id + 'will be pick up by' + delboyname + ', phone no-' + delboy_phoneno + '&priority=ndnd&stype=normal';
                    let resopnse1 = await axios.get(url1);
                    console.log("\nresopnse1.data :" + resopnse1.data);
                    return true;
                }
                else {
                    console.log("\nMSG RETURN :FALSE");
                    return false;
                }

            }
            else {
                console.log("\nRETURN OUTER IF :FALSE");
                return false;
            }
        }
        else {
            console.log("\nRETURN OUTER MOST IF :FALSE");
            return false;
        }
    }
    catch (error) {
        console.log('\nlaundryman.controller.sendSMS1  error', error);
        throw error;

    }
}


// Set Laundry man work location
exports.addEditWorkLocation = async (req, res) => {
    const { Employee, EmployeeWorkLocation, db1Conn } = await getModels();
    let flag = 1;
    console.log('\nlaundryman.controller.addEditWorkLocation  triggered -->');
    console.log("\n\n\n\nREQUEST BODY  %%%%===" + JSON.stringify(req.body, 0, 2));
    console.log('\nlaundryman.controller.addEditWorkLocation  EMP_ID : ' + req.body.emp_id);

    // Check employee worklocation exist or not
    let worklocation = await EmployeeWorkLocation.findOne({ where: { emp_id: req.body.emp_id } });


    if (worklocation) {
        const t = await db1Conn.transaction();
        try {
            console.log("\nDELETE EXISTING EMPLOYEE WORK LOCATION");
            await EmployeeWorkLocation.destroy({ where: { emp_id: req.body.emp_id } }, { transaction: t });
            const employee_work_location = await EmployeeWorkLocation.create({ emp_id: req.body.emp_id, longitude: req.body.longitude, latitude: req.body.latitude, active_flag: 'Active' }, { transaction: t });
            t.commit()
            console.log("\nEMPLOYEE WORK LOCATION ADDED SUCCESSFULLY ")
            res.json({ status: 1, message: "work location editted successfully", data: employee_work_location })
        }
        catch (error) {
            t.rollback()
            console.log(error);
            throw error;
            //res.status(404).send(error);
            //res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        }
    }
    else {
        const t = await db1Conn.transaction();
        try {
            const employee_work_location = await EmployeeWorkLocation.create({ emp_id: req.body.emp_id, longitude: req.body.longitude, latitude: req.body.latitude, active_flag: 'Active' }, { transaction: t });
            t.commit()
            console.log("\nEMPLOYEE WORK LOCATION ADDED SUCCESSFULLY ")
            res.json({ status: 1, message: "worklocation added successfully", data: employee_work_location })

        }
        catch (error) {
            t.rollback()
            console.log(error);
            throw error;
            //res.status(404).send(error);
            //res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        }

    }

}
// View work location
exports.viewWorkLocation = async (req, res) => {
    console.log('\nlaundryman.controller.viewWorkLocation  triggered -->');
    const { Employee, EmployeeWorkLocation, db1Conn } = await getModels();
    try {
        const worklocation = await EmployeeWorkLocation.findOne({ where: { emp_id: req.body.emp_id } });
        if (worklocation.length > 0) {
            console.log("\n\n LAUNDRYMAN WORKLOCATION======", JSON.stringify(worklocation, 0, 2));
            res.json({ status: 1, message: "work location ", data: worklocation });
        }
        else {
            console.log("\n\n EMPTY LAUNDRYMAN WORKLOCATION======", JSON.stringify(worklocation, 0, 2));
            res.json({ status: 1, message: "empty address list ", data: worklocation });
        }
    }
    catch (error) {
        console.log("\n\n\nlaundryman.controller.viewWorkLocation ERROR==" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
    }
}


