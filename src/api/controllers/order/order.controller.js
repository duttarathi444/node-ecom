const moment = require('moment');
const HttpStatus = require('http-status-codes');
const axios = require('axios');
const https = require('https');
// local imports
const env = require('../../../env');
const { getModels, SequelizeOp } = require('../../models');
const { pushToFirebase, dpushToFirebase } = require('../../lib/fcm_helper');
const checksum_lib = require('../../lib/paytm/checksum');
let orderHeader;
let address;
const utcOffsetMins = env.env.utcOffsetMins;
const order_creation_id = env.service_flow.order_creation_id;
const mid = env.paytm.mid;
const mkey = env.paytm.mkey;

exports.getCurrentDateTime = async (req, res) => {
    console.log('\ncart.controller.getCurrentDateTime  triggered -->');
    date_time = {};
    const current_moment = moment().utc().utcOffset(utcOffsetMins);
    let current_date_time = current_moment.format("DD/MM/YYYY hh:mm A");
    let current_date_time_array = current_date_time.split(" ");
    let current_date = current_date_time_array[0];
    let current_time = current_date_time_array[1] + " " + current_date_time_array[2];
    date_time.current_date_time = current_date_time;
    date_time.current_date = current_date;
    date_time.current_time = current_time;


    var format = 'hh:mm:ss A'
    const startMoment_1 = moment('12:00:00 AM', format).utc().utcOffset(utcOffsetMins);
    const endMoment_1 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);

    console.log("\n\n startMoment_1===", startMoment_1);
    console.log("\n\n endMoment_1===", endMoment_1);
    const startMoment_2 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);
    const endMoment_2 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);

    const startMoment_3 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);
    const endMoment_3 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);

    const startMoment_4 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);
    const endMoment_4 = moment('23:59:59 PM ', format).utc().utcOffset(utcOffsetMins);


    let order_receive_date_time = "";
    const order_moment = moment().utc().utcOffset(utcOffsetMins);


    if (order_moment.isSameOrAfter(startMoment_1) && order_moment.isBefore(endMoment_1)) {
        // const delivery_date_string = order_moment.clone().format("DD/MM/YYYY");
        // const delivery_time_string = '10:00 am';
        // order_receive_date_time = delivery_date_string + " " + delivery_time_string;
        order_receive_date_time = "Thanks for your order. Your clothes will be picked up by our delivery partner today between 8:00 AM and 1:30 PM";
        console.log("\n\n order_receive_date_time =", order_receive_date_time);
    }
    else if (order_moment.isSameOrAfter(startMoment_2) && order_moment.isBefore(endMoment_2)) {
        //order_receive_date_time = order_moment.clone().add(90, 'minutes').format("DD/MM/YYYY hh:mm a");
        order_receive_date_time = "Thanks for your order. Your clothes will be picked up by our delivery partner today between 8:00 AM and 1:30 PM";
        console.log("\n\n order_receive_date_time==", order_receive_date_time);
    }
    else if (order_moment.isSameOrAfter(startMoment_3) && order_moment.isBefore(endMoment_3)) {
        //order_receive_date_time = order_moment.clone().add(90, 'minutes').format("DD/MM/YYYY hh:mm a");
        // order_receive_date_time = "Oops :( Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
        order_receive_date_time = "Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
        console.log("\n\n order_receive_date_time===", order_receive_date_time);
    }
    else if (order_moment.isSameOrAfter(startMoment_4) && order_moment.isBefore(endMoment_4)) {
        // const delivery_date_string = order_moment.clone().add(1, 'day').format("DD/MM/YYYY");
        // console.log("\n\n delivery_date_string ======333", delivery_date_string);
        // const delivery_time_string = '10:00 am';
        //order_receive_date_time = delivery_date_string + " " + delivery_time_string;
        order_receive_date_time = "Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
        console.log("\n\n order_receive_date_time ====", order_receive_date_time);
    }
    /* var order_date_time = order_moment.format("DD/MM/YYYY hh:mm a");
     let order_date_time_array = order_date_time.split(" ");
     let order_date = order_date_time_array[0];
     let order_time = order_date_time_array[1] + " " + order_date_time_array[2];
 
     var cancel_date_time = cancel_order_moment.format("DD/MM/YYYY hh:mm a");
     let cancel_date_time_array = cancel_date_time.split(" ");
     let cancel_date = cancel_date_time_array[0];
     let cancel_time = cancel_date_time_array[1] + " " + cancel_date_time_array[2];
 
     console.log("\n\n order_date_time ======", order_date_time);
     console.log("\n\n order_date  ======", order_date);
 
     console.log("\n\n order_time  ======", order_time);
 
 
     console.log("\n\n cancel_date_time ======", cancel_date_time);
     console.log("\n\n cancel_date  ======", cancel_date);
 
     console.log("\n\n cancel_time  ======", cancel_time);
 
 
     console.log("\n\n ORDER MONENT ======", order_moment);
     console.log("\n\nCANCEL  ORDER MONENT ======", cancel_order_moment);
     console.log("\n\n DELIVERY ORDER MONENT ======", order_receive_date_time); */

    date_time.msg = order_receive_date_time;



    return res.json({ status: 1, message: "Current Date Time", data: date_time });
}

// MLTIPLE ORDERS CREATED BY COD
exports.createMultipleOrders = async (req, res) => {
    console.log('\norder.controller.createMultipleOrders  triggered -->');

    const reqBody = req.body;
    const {
        OrderNotifications, Login, EmployeeLogin, UserMaster, DelboyOrderMapping, Employee,
        OrderHeader, OrderDetails, OrderHistory, Address, LaundryServiceFlow, CartDetails,
        CartHeader, ServiceAvailabilityPincode, db1Conn
    } = await getModels();
    const orderHeaders = [];
    // validate orderRequests
    const validationErros = await validateOrderRequests(reqBody, "create");
    console.log("\n\n validationErros======", validationErros);
    if (validationErros.length) {
        return res.send({ status: 0, message: validationErros[0], data: [{}] })
    }
    // generate unique paytm virtual id  
    const virtual_order_id = new Date().getTime();
    const t = await db1Conn.transaction();

    try {
        // create all new orders
        await Promise.all(reqBody.map(async (orderRequest) => {
            console.log("\n\n ORDER_REQUEST_DETAIL======", JSON.stringify(orderRequest, 0, 2));

            // Here status_id and module_id are hard coded, it will be dynamic in future
            let laundry_service_flow = await LaundryServiceFlow.findOne({
                where: { status_id: order_creation_id, module_id: 1 }
            }, { transaction: t });
            console.log("\n\n LAUNDRY SERVICE FLOW======", JSON.stringify(laundry_service_flow, 0, 2));
            const format = 'hh:mm:ss A';

            const startMoment_1 = moment('12:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_1 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            console.log("\n\n startMoment_1===", startMoment_1);
            console.log("\n\n endMoment_1===", endMoment_1);

            const startMoment_2 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_2 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);

            const startMoment_3 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_3 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);

            const startMoment_4 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_4 = moment('23:59:59 PM', format).utc().utcOffset(utcOffsetMins);

            let order_receive_date_time = "";
            const order_moment = moment().utc().utcOffset(utcOffsetMins);
            const cancel_order_moment = order_moment.clone().add(15, 'minutes');

            if (order_moment.isSameOrAfter(startMoment_1) && order_moment.isBefore(endMoment_1)) {
                // const delivery_date_string = order_moment.clone().format("DD/MM/YYYY");
                // const delivery_time_string = '10:00 am';
                // order_receive_date_time = delivery_date_string + " " + delivery_time_string;
                order_receive_date_time = "Thanks for your order. Your clothes will be picked up by our delivery partner today between 8:00 AM and 1:30 PM";
                console.log("\n\n order_receive_date_time=", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_2) && order_moment.isBefore(endMoment_2)) {
                // order_receive_date_time = order_moment.clone().add(90, 'minutes').format("DD/MM/YYYY hh:mm a");
                order_receive_date_time = "Thanks for your order. Your clothes will be picked up by our delivery partner today between 8:00 AM and 1:30 PM";
                console.log("\n\n order_receive_date_time==", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_3) && order_moment.isBefore(endMoment_3)) {
                // order_receive_date_time = order_moment.clone().add(90, 'minutes').format("DD/MM/YYYY hh:mm a");
                order_receive_date_time = "Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
                console.log("\n\n order_receive_date_time===", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_4) && order_moment.isBefore(endMoment_4)) {
                // const delivery_date_string = order_moment.clone().add(1, 'day').format("DD/MM/YYYY");
                // console.log("\n\n delivery_date_string ======333", delivery_date_string);
                // const delivery_time_string = '10:00 am';
                // order_receive_date_time = delivery_date_string + " " + delivery_time_string;
                order_receive_date_time = "Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
                console.log("\n\n order_receive_date_time====", order_receive_date_time);
            }
            let order_date_time = order_moment.format("DD/MM/YYYY hh:mm A");
            let order_date_time_array = order_date_time.split(" ");
            let order_date = order_date_time_array[0];
            let order_time = order_date_time_array[1] + " " + order_date_time_array[2];
            let cancel_date_time = cancel_order_moment.format("DD/MM/YYYY hh:mm A");
            let cancel_date_time_array = cancel_date_time.split(" ");
            let cancel_date = cancel_date_time_array[0];
            let cancel_time = cancel_date_time_array[1] + " " + cancel_date_time_array[2];
            console.log("\n\n order_date_time ======", order_date_time);
            console.log("\n\n order_date  ======", order_date);
            console.log("\n\n order_time  ======", order_time);
            console.log("\n\n cancel_date_time ======", cancel_date_time);
            console.log("\n\n cancel_date  ======", cancel_date);
            console.log("\n\n cancel_time  ======", cancel_time);
            console.log("\n\n ORDER MONENT ======", order_moment);
            console.log("\n\n CANCEL  ORDER MONENT ======", cancel_order_moment);
            console.log("\n\n DELIVERY ORDER MONENT ======", order_receive_date_time);

            // let statusDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm:ss a");
            // let date_time_array = statusDate.split(" ");
            // let date = date_time_array[0];
            // let time = date_time_array[1] + " " + date_time_array[2];

            const orderHeader = await OrderHeader.create({
                user_id: orderRequest.user_id, trans_id: orderRequest.trans_id, order_date: order_date_time,
                status_id: laundry_service_flow.status_id, item_count: orderRequest.grand_total_quantity,
                total_price: orderRequest.grand_total_price, address: orderRequest.address, expected_delivery_time: " ",
                actual_delivery_time: " ", pincode: orderRequest.address.pincode, virtual_order_id, order_flag: 1, category_id: orderRequest.order_details[0].category_id
            }, { transaction: t });
            console.log("\n\n ORDERHEADER CREATED ======", JSON.stringify(orderHeader, 0, 2));

            if (orderRequest.address.address_id) {
                let address = await Address.findOne({
                    where: { address_id: orderRequest.address.address_id },
                    attributes: ['address1', 'address2', 'address3', 'city', 'state', 'pincode',
                        'longitude', 'latitude', 'flag_default', 'phoneno']
                });
                console.log("\n\n EXISTING ADDRESS ======", JSON.stringify(address, 0, 2));

                await Address.update({ flag_default: 'secondary' },
                    { where: { user_id: orderRequest.user_id } }, { transaction: t });

                await Address.update({ flag_default: orderRequest.address.flag_default },
                    { where: { address_id: orderRequest.address.address_id } }, { transaction: t });
            }
            else {
                await Address.update({ flag_default: 'secondary' },
                    { where: { user_id: orderRequest.user_id } }, { transaction: t });

                const address = await Address.create({
                    address1: orderRequest.address.address1,
                    address2: orderRequest.address.address2, address3: orderRequest.address.address3,
                    city: orderRequest.address.city, state: orderRequest.address.state,
                    pincode: orderRequest.address.pincode, longitude: orderRequest.address.longitude,
                    latitude: orderRequest.address.latitude, flag_default: orderRequest.address.flag_default,
                    user_id: orderRequest.user_id, phoneno: orderRequest.address.phoneno
                }, { transaction: t });
                console.log("\n\n ADDRESS CREATED======", JSON.stringify(address, 0, 2));
            }

            await Promise.all(orderRequest.order_details.map(async (element) => {
                console.log("\n\n ORDER_DETAIL======", JSON.stringify(element, 0, 2));
                let orderDetails = await OrderDetails.create({
                    order_id: orderHeader.order_id, user_id: orderHeader.user_id,
                    prod_id: element.prod_id, customer_order_quantity: element.quantity, dboy_order_accept_quantity: 0,
                    lman_order_accept_quantity: 0
                }, { transaction: t });
                console.log("\n\n ORDER_DETAIL ADDED SUCCESSFULLY ")
            }));
            console.log("\n\n ORDERHEADER CREATED ======", JSON.stringify(orderHeader, 0, 2));
            console.log("\n\n ORDERHEADER ITEM COUNT ======", orderHeader.item_count);
            console.log("\n\n ORDERHEADER ITEM PRICE ======", orderHeader.total_price);
            console.log("\n\n ORDER DATE ======", order_date);
            console.log("\n\n ORDER TIME ======", order_time);

            const orderHistory = await OrderHistory.create({
                order_id: orderHeader.order_id, status_id: laundry_service_flow.status_id,
                status_date: order_date, active_flag: laundry_service_flow.status_desc,
                user_id: orderRequest.user_id, status_time: order_time, item_count: orderHeader.item_count,
                total_price: orderHeader.total_price
            }, { transaction: t });
            console.log("\n\n ORDER HISTORY CREATED======", JSON.stringify(orderHistory, 0, 2));

            console.log("\n\n ORDER ADDED SUCCESSFULLY ");
            orderHeader.setDataValue('cancel_date_time', cancel_date_time);
            orderHeader.setDataValue('order_receive_date_time', order_receive_date_time);

            orderHeaders.push(orderHeader);
        }));

        // delete the cart
        const cartDetails = await CartDetails.findAll({
            where: { cart_id: reqBody[0].cart_id }
        });
        await Promise.all(cartDetails.map(async (element) => {
            await CartDetails.destroy({ where: { cart_id: element.cart_id }, transaction: t });
        }));
        await CartHeader.destroy({
            where: { user_id: reqBody[0].user_id, cart_id: reqBody[0].cart_id }, transaction: t
        });

        // commit txn
        t.commit();

        // send all order related notifications
        await Promise.all(orderHeaders.map(async (orderHeader) => {
            try {
                let x = 0;
                let refreshId = setInterval(async function () {
                    x = x + 1;
                    console.log("\n\n TRY TO SEND MESSAGE FOR TIME :" + x);
                    let result = await sendSMS(orderHeader.order_id, orderHeader.user_id, orderHeader.address);
                    let result_1 = await sendSMSDemo(orderHeader.order_id, orderHeader.user_id, orderHeader.address);
                    if (result) {
                        console.log("\n\n MESSAGE SEND  SUCCESSFULLY TO DELBOY AND CUSTOMER");
                        console.log("\n\n MESSAGE SEND  SUCCESSFULLY TO ADMIN ", result_1);
                        clearInterval(refreshId);
                    }
                    if (x === 3) {
                        console.log("\n\nTRY FOR TRHEE TIMES, MESSAGE NOT SEND  SUCCESSFULLY TO DELBOY AND CUSTOMER");
                        clearInterval(refreshId);
                    }
                }, 5000);

                let empname = "";
                let emp_phoneno = "";
                let msg_body1 = "";
                let login = await Login.findOne({ where: { user_id: orderHeader.user_id } });
                let user = await UserMaster.findOne({ where: { user_id: orderHeader.user_id } });
                let dboy_order_mapping = await DelboyOrderMapping.findOne({
                    where: { order_id: orderHeader.order_id, delivery_type: 1 }
                });
                if (dboy_order_mapping) {
                    let employee = await Employee.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                    empname = employee.emp_fname + " " + employee.emp_lname;
                    emp_phoneno = employee.emp_contact_no;
                }

                let employee_login = await EmployeeLogin.findOne({
                    where: { emp_id: dboy_order_mapping.emp_id }
                });
                const msg_body = 'Thanks for ordering. Order no ' + orderHeader.order_id +
                    ' will be pick up by our delivery boy ' + empname + ', phone no-' + emp_phoneno;

                let customername = user.user_fname + " " + user.user_lname;
                let customerphoneno = "";
                if (user.user_phoneno) {
                    console.log("\nDELIVERYBOY NOTIFICATION USER PHONE NO :" + user.user_phoneno);
                    msg_body1 = 'Customer name-' + customername + ', order no ' + orderHeader.order_id + ', phone no-' + user.user_phoneno + ', address-http://maps.google.com/?q=' + orderHeader.address.latitude + ',' + orderHeader.address.longitude;
                    customerphoneno = user.user_phoneno;
                }
                else {
                    console.log("\nDELIVERYBOY NOTIFIACTION USER HAVING NO PHONE NO :");
                    msg_body1 = 'Customer name-' + customername + ', order no ' + orderHeader.order_id + ', address-http://maps.google.com/?q=' + orderHeader.address.latitude + ',' + orderHeader.address.longitude;
                    customerphoneno = "";
                }

                let laundry_service_flow = await LaundryServiceFlow.findOne({
                    where: { status_id: order_creation_id, module_id: 1 }
                });
                const order_noti_moment = moment().utc().utcOffset(utcOffsetMins);
                let order_noti_date_time = order_noti_moment.format("DD/MM/YYYY hh:mm A");
                console.log("\n NOTIFIACTION ENTRY FOR ORDER NO:", orderHeader.order_id);
                const dorderNotifications = await OrderNotifications.create({
                    order_id: orderHeader.order_id, status_id: laundry_service_flow.status_id,
                    emp_id: dboy_order_mapping.emp_id, role_id: 2, msg_title: 'Order has to be receive',
                    msg_body: msg_body1, created_at: order_noti_date_time,
                    latitude: orderHeader.address.latitude, longitude: orderHeader.address.longitude
                });
                const orderNotifications = await OrderNotifications.create({
                    order_id: orderHeader.order_id, status_id: laundry_service_flow.status_id,
                    user_id: orderHeader.user_id, msg_title: 'Order Created Successfully',
                    msg_body: msg_body, created_at: order_noti_date_time
                });

                const fetchBody = {
                    notification: {
                        title: "iestre",
                        text: msg_body,
                        click_action: "com.purpuligo.laundry.NotificationActivity"
                    },
                    data: {
                        deliveryBoyName: empname,
                        orderId: orderHeader.order_id,
                        delBoyPhoneNo: emp_phoneno
                    },
                    to: login.fcmToken
                };
                const fetchBody1 = {
                    notification: {
                        title: "iestre",
                        text: msg_body1,
                        click_action: "com.purpuligo.laundry.NotificationActivity"
                    },
                    data: {
                        customerName: customername,
                        orderId: orderHeader.order_id,
                        customerPhoneNo: customerphoneno
                    },
                    to: employee_login.fcmToken
                };

                let result1 = dpushToFirebase(fetchBody1, 3);
                console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY");

                let y = 0;
                var notificationId = setInterval(function () {
                    y = y + 1;
                    console.log("\n\n TRY TO SEND NOTIFICATION TO FIREBASE :" + y);
                    let result = pushToFirebase(fetchBody, 3);
                    if (result) {
                        console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO CUSTOMER");
                        //dpushToFirebase(fetchBody1, 3);
                        // console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY");
                        clearInterval(notificationId);
                    }
                    // if (y === 1) {
                    //     console.log("\n\nNOTIFICATION NOT SEND  SUCCESSFULLY TO DELBOY CUSTOMER");
                    //     clearInterval(notificationId);
                    // }
                }, 120000);
            }
            catch (error) {
                console.log("\n\n NOTIFICATION ERROR ", error);
            }
        }));

        // send response
        res.send({ status: 1, message: "orders created successfully", data: orderHeaders });
    } catch (err) {
        t.rollback()
        console.log(err);
        res.status(404).send(err);
    }
}

// RECREATE MULTIPLE ORDER FOR COD (Order Request Send After Failed|Pending|NA orderTxnStatus method )
exports.recreateMultipleOrders = async (req, res) => {
    console.log('\norder.controller.recreateMultipleOrders  triggered -->');
    const reqBody = req.body;
    const {
        OrderNotifications, Login, EmployeeLogin, UserMaster, DelboyOrderMapping, Employee,
        OrderHeader, OrderDetails, OrderHistory, Address, LaundryServiceFlow, CartDetails,
        CartHeader, ServiceAvailabilityPincode, db1Conn
    } = await getModels();
    const orderHeaders = [];
    // validate orderRequests
    const validationErros = await validateOrderRequests(reqBody);
    console.log("\n\n validationErros======", validationErros);
    if (validationErros.length) {
        return res.send({ status: 0, message: validationErros[0], data: [{}] })
    }
    // generate unique paytm virtual id  
    // virtual_order_id2 = new Date().getTime();
    const t = await db1Conn.transaction();

    try {
        // create all new orders
        await Promise.all(reqBody.map(async (orderRequest) => {
            console.log("\n\n ORDER_REQUEST_DETAIL======", JSON.stringify(orderRequest, 0, 2));

            // Here status_id and module_id are hard coded, it will be dynamic in future
            let laundry_service_flow = await LaundryServiceFlow.findOne({
                where: { status_id: order_creation_id, module_id: 1 }
            }, { transaction: t });
            console.log("\n\n LAUNDRY SERVICE FLOW======", JSON.stringify(laundry_service_flow, 0, 2));
            const format = 'hh:mm:ss A';

            const startMoment_1 = moment('12:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_1 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            console.log("\n\n startMoment_1===", startMoment_1);
            console.log("\n\n endMoment_1===", endMoment_1);

            const startMoment_2 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_2 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);

            const startMoment_3 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_3 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);

            const startMoment_4 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_4 = moment('23:59:59 PM', format).utc().utcOffset(utcOffsetMins);

            let order_receive_date_time = "";
            const order_moment = moment().utc().utcOffset(utcOffsetMins);
            const cancel_order_moment = order_moment.clone().add(15, 'minutes');

            if (order_moment.isSameOrAfter(startMoment_1) && order_moment.isBefore(endMoment_1)) {
                // const delivery_date_string = order_moment.clone().format("DD/MM/YYYY");
                // const delivery_time_string = '10:00 am';
                // order_receive_date_time = delivery_date_string + " " + delivery_time_string;
                order_receive_date_time = "Thanks for your order. Your clothes will be picked up by our delivery partner today between 8:00 AM and 1:30 PM";
                console.log("\n\n order_receive_date_time=", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_2) && order_moment.isBefore(endMoment_2)) {
                // order_receive_date_time = order_moment.clone().add(90, 'minutes').format("DD/MM/YYYY hh:mm a");
                order_receive_date_time = "Thanks for your order. Your clothes will be picked up by our delivery partner today between 8:00 AM and 1:30 PM";
                console.log("\n\n order_receive_date_time==", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_3) && order_moment.isBefore(endMoment_3)) {
                // order_receive_date_time = order_moment.clone().add(90, 'minutes').format("DD/MM/YYYY hh:mm a");
                order_receive_date_time = "Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
                console.log("\n\n order_receive_date_time===", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_4) && order_moment.isBefore(endMoment_4)) {
                // const delivery_date_string = order_moment.clone().add(1, 'day').format("DD/MM/YYYY");
                // console.log("\n\n delivery_date_string ======333", delivery_date_string);
                // const delivery_time_string = '10:00 am';
                // order_receive_date_time = delivery_date_string + " " + delivery_time_string;
                order_receive_date_time = "Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
                console.log("\n\n order_receive_date_time====", order_receive_date_time);
            }
            let order_date_time = order_moment.format("DD/MM/YYYY hh:mm A");
            let order_date_time_array = order_date_time.split(" ");
            let order_date = order_date_time_array[0];
            let order_time = order_date_time_array[1] + " " + order_date_time_array[2];
            let cancel_date_time = cancel_order_moment.format("DD/MM/YYYY hh:mm A");
            let cancel_date_time_array = cancel_date_time.split(" ");
            let cancel_date = cancel_date_time_array[0];
            let cancel_time = cancel_date_time_array[1] + " " + cancel_date_time_array[2];
            console.log("\n\n order_date_time ======", order_date_time);
            console.log("\n\n order_date  ======", order_date);
            console.log("\n\n order_time  ======", order_time);
            console.log("\n\n cancel_date_time ======", cancel_date_time);
            console.log("\n\n cancel_date  ======", cancel_date);
            console.log("\n\n cancel_time  ======", cancel_time);
            console.log("\n\n ORDER MONENT ======", order_moment);
            console.log("\n\n CANCEL  ORDER MONENT ======", cancel_order_moment);
            console.log("\n\n DELIVERY ORDER MONENT ======", order_receive_date_time);

            // let statusDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm:ss a");
            // let date_time_array = statusDate.split(" ");
            // let date = date_time_array[0];
            // let time = date_time_array[1] + " " + date_time_array[2];

            const orderHeader = await OrderHeader.update({
                order_date: order_date_time, item_count: orderRequest.grand_total_quantity,
                total_price: orderRequest.grand_total_price, address: orderRequest.address,
                pincode: orderRequest.address.pincode, payment_mode: 'COD', payment_status: 'NA', order_flag: 1, category_id: orderRequest.order_details[0].category_id
            },
                { where: { virtual_order_id: orderRequest.virtual_order_id, order_id: orderRequest.order_id, user_id: orderRequest.user_id } }, { transaction: t });
            console.log("\n\nRECREATED ORDERHEADER UPDATED ======", JSON.stringify(orderHeader, 0, 2));

            if (orderRequest.address.address_id) {
                let address = await Address.findOne({
                    where: { address_id: orderRequest.address.address_id },
                    attributes: ['address1', 'address2', 'address3', 'city', 'state', 'pincode',
                        'longitude', 'latitude', 'flag_default', 'phoneno']
                });
                console.log("\n\n RECREATE EXISTING ADDRESS ======", JSON.stringify(address, 0, 2));

                await Address.update({ flag_default: 'secondary' },
                    { where: { user_id: orderRequest.user_id } }, { transaction: t });

                await Address.update({ flag_default: orderRequest.address.flag_default },
                    { where: { address_id: orderRequest.address.address_id } }, { transaction: t });
            }
            else {
                await Address.update({ flag_default: 'secondary' },
                    { where: { user_id: orderRequest.user_id } }, { transaction: t });

                const address = await Address.create({
                    address1: orderRequest.address.address1,
                    address2: orderRequest.address.address2, address3: orderRequest.address.address3,
                    city: orderRequest.address.city, state: orderRequest.address.state,
                    pincode: orderRequest.address.pincode, longitude: orderRequest.address.longitude,
                    latitude: orderRequest.address.latitude, flag_default: orderRequest.address.flag_default,
                    user_id: orderRequest.user_id, phoneno: orderRequest.address.phoneno
                }, { transaction: t });
                console.log("\n\n RECREATE ADDRESS CREATED======", JSON.stringify(address, 0, 2));
            }

            await Promise.all(orderRequest.order_details.map(async (element) => {
                console.log("\n\n ORDER_DETAIL======", JSON.stringify(element, 0, 2));
                let orderDetails = await OrderDetails.update(
                    { prod_id: element.prod_id, customer_order_quantity: element.quantity },
                    { where: { order_id: orderRequest.order_id, user_id: orderRequest.user_id } }, { transaction: t });
                console.log("\n\n RECREATE ORDER_DETAIL UPDATED SUCCESSFULLY ")
            }));
            console.log("\n\n RECREATE ORDERHEADER CREATED ======", JSON.stringify(orderHeader, 0, 2));
            console.log("\n\nRECREATE ORDERHEADER ITEM COUNT ======", orderHeader.item_count);
            console.log("\n\n RECREATE ORDERHEADER ITEM PRICE ======", orderHeader.total_price);
            console.log("\n\n RECREATE ORDER DATE ======", order_date);
            console.log("\n\n RECREATE ORDER TIME ======", order_time);

            const orderHistory = await OrderHistory.update({
                status_date: order_date, status_time: order_time,
                item_count: orderHeader.item_count, total_price: orderHeader.total_price
            },
                { where: { order_id: orderRequest.order_id, user_id: orderRequest.user_id } }, { transaction: t });
            console.log("\n\n RECREATE ORDER HISTORY UPDATED======", JSON.stringify(orderHistory, 0, 2));

            console.log("\n\n RECREATE ORDER UPDATED SUCCESSFULLY ");
            orderHeader.setDataValue('cancel_date_time', cancel_date_time);
            orderHeader.setDataValue('order_receive_date_time', order_receive_date_time);

            orderHeaders.push(orderHeader);
        }));
        /* 
        // delete the cart
          const cartDetails = await CartDetails.findAll({
              where: { cart_id: reqBody[0].cart_id }
          });
          await Promise.all(cartDetails.map(async (element) => {
              await CartDetails.destroy({ where: { cart_id: element.cart_id }, transaction: t });
          }));
          await CartHeader.destroy({
              where: { user_id: reqBody[0].user_id, cart_id: reqBody[0].cart_id }, transaction: t
          }); 
          */

        // commit txn
        t.commit();

        // send all order related notifications
        await Promise.all(orderHeaders.map(async (orderHeader) => {
            try {
                let x = 0;
                let refreshId = setInterval(async function () {
                    x = x + 1;
                    console.log("\n\n RECREATE TRY TO SEND MESSAGE FOR TIME :" + x);
                    let result = await sendSMS(orderHeader.order_id, orderHeader.user_id, orderHeader.address);
                    let result_1 = await sendSMSDemo(orderHeader.order_id, orderHeader.user_id, orderHeader.address);
                    if (result) {
                        console.log("\n\n RECREATE MESSAGE SEND  SUCCESSFULLY TO DELBOY AND CUSTOMER");
                        console.log("\n\nRECREATE  MESSAGE SEND  SUCCESSFULLY TO ADMIN ", result_1);
                        clearInterval(refreshId);
                    }
                    if (x === 3) {
                        console.log("\n\nRECREATE TRY FOR TRHEE TIMES, MESSAGE NOT SEND  SUCCESSFULLY TO DELBOY AND CUSTOMER");
                        clearInterval(refreshId);
                    }
                }, 5000);

                let empname = "";
                let emp_phoneno = "";
                let msg_body1 = "";
                let login = await Login.findOne({ where: { user_id: orderHeader.user_id } });
                let user = await UserMaster.findOne({ where: { user_id: orderHeader.user_id } });
                let dboy_order_mapping = await DelboyOrderMapping.findOne({
                    where: { order_id: orderHeader.order_id, delivery_type: 1 }
                });
                if (dboy_order_mapping) {
                    let employee = await Employee.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                    empname = employee.emp_fname + " " + employee.emp_lname;
                    emp_phoneno = employee.emp_contact_no;
                }

                let employee_login = await EmployeeLogin.findOne({
                    where: { emp_id: dboy_order_mapping.emp_id }
                });
                const msg_body = 'Thanks for ordering. Order no ' + orderHeader.order_id +
                    ' will be pick up by our delivery boy ' + empname + ', phone no-' + emp_phoneno;

                let customername = user.user_fname + " " + user.user_lname;
                let customerphoneno = "";
                if (user.user_phoneno) {
                    console.log("\nDELIVERYBOY NOTIFICATION USER PHONE NO :" + user.user_phoneno);
                    msg_body1 = 'Customer name-' + customername + ', order no ' + orderHeader.order_id + ', phone no-' + user.user_phoneno + ', address-http://maps.google.com/?q=' + orderHeader.address.latitude + ',' + orderHeader.address.longitude;
                    customerphoneno = user.user_phoneno;
                }
                else {
                    console.log("\nDELIVERYBOY NOTIFIACTION USER HAVING NO PHONE NO :");
                    msg_body1 = 'Customer name-' + customername + ', order no ' + orderHeader.order_id + ', address-http://maps.google.com/?q=' + orderHeader.address.latitude + ',' + orderHeader.address.longitude;
                    customerphoneno = "";
                }

                const dorderNotifications = await OrderNotifications.create({
                    order_id: orderHeader.order_id, status_id: laundry_service_flow.status_id,
                    emp_id: dboy_order_mapping.emp_id, role_id: 2, msg_title: 'Order has to be receive',
                    msg_body: msg_body1, created_at: order_date_time,
                    latitude: orderHeader.address.latitude, longitude: orderHeader.address.longitude
                });
                const orderNotifications = await OrderNotifications.create({
                    order_id: orderHeader.order_id, status_id: laundry_service_flow.status_id,
                    user_id: orderRequest.user_id, msg_title: 'Order Created Successfully',
                    msg_body: msg_body, created_at: order_date_time
                });

                const fetchBody = {
                    notification: {
                        title: "iestre",
                        text: msg_body,
                        click_action: "com.purpuligo.laundry.NotificationActivity"
                    },
                    data: {
                        deliveryBoyName: empname,
                        orderId: orderHeader.order_id,
                        delBoyPhoneNo: emp_phoneno
                    },
                    to: login.fcmToken
                };
                const fetchBody1 = {
                    notification: {
                        title: "iestre",
                        text: msg_body1,
                        click_action: "com.purpuligo.laundry.NotificationActivity"
                    },
                    data: {
                        customerName: customername,
                        orderId: orderHeader.order_id,
                        customerPhoneNo: customerphoneno
                    },
                    to: employee_login.fcmToken
                };

                let result1 = dpushToFirebase(fetchBody1, 3);
                console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY");

                let y = 0;
                var notificationId = setInterval(function () {
                    y = y + 1;
                    console.log("\n\n TRY TO SEND NOTIFICATION TO FIREBASE :" + y);
                    let result = pushToFirebase(fetchBody, 3);
                    if (result) {
                        console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO CUSTOMER");
                        //dpushToFirebase(fetchBody1, 3);
                        // console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY");
                        clearInterval(notificationId);
                    }
                    // if (y === 1) {
                    //     console.log("\n\nNOTIFICATION NOT SEND  SUCCESSFULLY TO DELBOY CUSTOMER");
                    //     clearInterval(notificationId);
                    // }
                }, 120000);
            }
            catch (error) {
                console.log("\n\n NOTIFICATION ERROR ", error);
            }
        }));

        // send response
        res.send({ status: 1, message: "orders recreated successfully", data: orderHeaders });
    } catch (err) {
        t.rollback()
        console.log(err);
        res.status(404).send(err);
    }
}

// MLTIPLE ORDERS CREATED BY PAYTM
exports.createMultipleOrders2 = async (req, res) => {
    console.log('\norder.controller.createMultipleOrders2  triggered -->');
    console.log('\norder.controller.createMultipleOrders2  mid -->', mid);
    console.log('\norder.controller.createMultipleOrders2  mkey -->', mkey);

    const reqBody = req.body;
    const {
        OrderNotifications, Login, EmployeeLogin, UserMaster, DelboyOrderMapping, Employee,
        OrderHeader, OrderDetails, OrderHistory, Address, LaundryServiceFlow, CartDetails,
        CartHeader, ServiceAvailabilityPincode, OrderPaymentMapping, db1Conn
    } = await getModels();
    const orderHeaders = [];
    const orderHeadersRes = {};

    // validate orderRequests
    const validationErros = await validateOrderRequests2(reqBody, "create");
    console.log("\n\n validationErros======", validationErros);
    if (validationErros.length) {
        return res.send({ status: 0, message: validationErros[0], data: {} })
    }

    // generate unique paytm virtual id  
    orderHeadersRes.virtual_order_id = new Date().getTime();
    //attach order_total_price and user_id attached to orderHeadersRes
    orderHeadersRes.order_total_price = reqBody.order_total_price;
    orderHeadersRes.order_total_quantity = reqBody.order_total_quantity;
    orderHeadersRes.user_id = reqBody.user_id;
    orderHeadersRes.order_header_list = reqBody.order_header_list;
    orderHeadersRes.cart_id = reqBody.order_header_list[0].cart_id;
    const t = await db1Conn.transaction();
    try {
        // create all new orders
        await Promise.all(orderHeadersRes.order_header_list.map(async (orderRequest) => {
            console.log("\n\n ORDER_REQUEST_DETAIL======", JSON.stringify(orderRequest, 0, 2));

            // Here status_id and module_id are hard coded, it will be dynamic in future
            let laundry_service_flow = await LaundryServiceFlow.findOne({
                where: { status_id: order_creation_id, module_id: 1 }
            }, { transaction: t });
            console.log("\n\n LAUNDRY SERVICE FLOW======", JSON.stringify(laundry_service_flow, 0, 2));
            const format = 'hh:mm:ss A';

            const startMoment_1 = moment('12:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_1 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            console.log("\n\n startMoment_1===", startMoment_1);
            console.log("\n\n endMoment_1===", endMoment_1);

            const startMoment_2 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_2 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);

            const startMoment_3 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_3 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);

            const startMoment_4 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_4 = moment('23:59:59 PM', format).utc().utcOffset(utcOffsetMins);

            let order_receive_date_time = "";
            const order_moment = moment().utc().utcOffset(utcOffsetMins);
            const cancel_order_moment = order_moment.clone().add(15, 'minutes');

            if (order_moment.isSameOrAfter(startMoment_1) && order_moment.isBefore(endMoment_1)) {
                order_receive_date_time = "Thanks for your order. Your clothes will be picked up by our delivery partner today between 8:00 AM and 1:30 PM";
                console.log("\n\n order_receive_date_time=", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_2) && order_moment.isBefore(endMoment_2)) {
                order_receive_date_time = "Thanks for your order. Your clothes will be picked up by our delivery partner today between 8:00 AM and 1:30 PM";
                console.log("\n\n order_receive_date_time==", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_3) && order_moment.isBefore(endMoment_3)) {
                order_receive_date_time = "Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
                console.log("\n\n order_receive_date_time===", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_4) && order_moment.isBefore(endMoment_4)) {
                order_receive_date_time = "Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
                console.log("\n\n order_receive_date_time====", order_receive_date_time);
            }
            let order_date_time = order_moment.format("DD/MM/YYYY hh:mm A");
            let order_date_time_array = order_date_time.split(" ");
            let order_date = order_date_time_array[0];
            let order_time = order_date_time_array[1] + " " + order_date_time_array[2];
            let cancel_date_time = cancel_order_moment.format("DD/MM/YYYY hh:mm A");
            let cancel_date_time_array = cancel_date_time.split(" ");
            let cancel_date = cancel_date_time_array[0];
            let cancel_time = cancel_date_time_array[1] + " " + cancel_date_time_array[2];
            console.log("\n\n order_date_time ======", order_date_time);
            console.log("\n\n order_date  ======", order_date);
            console.log("\n\n order_time  ======", order_time);
            console.log("\n\n cancel_date_time ======", cancel_date_time);
            console.log("\n\n cancel_date  ======", cancel_date);
            console.log("\n\n cancel_time  ======", cancel_time);
            console.log("\n\n ORDER MONENT ======", order_moment);
            console.log("\n\n CANCEL  ORDER MONENT ======", cancel_order_moment);
            console.log("\n\n DELIVERY ORDER MONENT ======", order_receive_date_time);

            const orderHeader = await OrderHeader.create({
                user_id: orderRequest.user_id, trans_id: orderRequest.trans_id, order_date: order_date_time,
                status_id: laundry_service_flow.status_id, item_count: orderRequest.grand_total_quantity,
                total_price: orderRequest.grand_total_price, address: orderRequest.address, expected_delivery_time: " ",
                actual_delivery_time: " ", pincode: orderRequest.address.pincode, payment_mode: 'ONLINE', payment_status: 'UNPAID',
                virtual_order_id: orderHeadersRes.virtual_order_id, order_flag: 0, category_id: orderRequest.order_details[0].category_id
            }, { transaction: t });
            console.log("\n\n ORDERHEADER CREATED ======", JSON.stringify(orderHeader, 0, 2));

            if (orderRequest.address.address_id) {
                let address = await Address.findOne({
                    where: { address_id: orderRequest.address.address_id },
                    attributes: ['address1', 'address2', 'address3', 'city', 'state', 'pincode',
                        'longitude', 'latitude', 'flag_default', 'phoneno']
                });
                console.log("\n\n EXISTING ADDRESS ======", JSON.stringify(address, 0, 2));

                await Address.update({ flag_default: 'secondary' },
                    { where: { user_id: orderRequest.user_id } }, { transaction: t });

                await Address.update({ flag_default: orderRequest.address.flag_default },
                    { where: { address_id: orderRequest.address.address_id } }, { transaction: t });
            }
            else {
                await Address.update({ flag_default: 'secondary' },
                    { where: { user_id: orderRequest.user_id } }, { transaction: t });

                const address = await Address.create({
                    address1: orderRequest.address.address1,
                    address2: orderRequest.address.address2, address3: orderRequest.address.address3,
                    city: orderRequest.address.city, state: orderRequest.address.state,
                    pincode: orderRequest.address.pincode, longitude: orderRequest.address.longitude,
                    latitude: orderRequest.address.latitude, flag_default: orderRequest.address.flag_default,
                    user_id: orderRequest.user_id, phoneno: orderRequest.address.phoneno
                }, { transaction: t });
                console.log("\n\n ADDRESS CREATED======", JSON.stringify(address, 0, 2));
            }

            await Promise.all(orderRequest.order_details.map(async (element) => {
                console.log("\n\n ORDER_DETAIL======", JSON.stringify(element, 0, 2));
                let orderDetails = await OrderDetails.create({
                    order_id: orderHeader.order_id, user_id: orderHeader.user_id,
                    prod_id: element.prod_id, customer_order_quantity: element.quantity, dboy_order_accept_quantity: 0,
                    lman_order_accept_quantity: 0
                }, { transaction: t });
                console.log("\n\n ORDER_DETAIL ADDED SUCCESSFULLY ")
            }));
            console.log("\n\n ORDERHEADER CREATED ======", JSON.stringify(orderHeader, 0, 2));
            console.log("\n\n ORDERHEADER ITEM COUNT ======", orderHeader.item_count);
            console.log("\n\n ORDERHEADER ITEM PRICE ======", orderHeader.total_price);
            console.log("\n\n ORDER DATE ======", order_date);
            console.log("\n\n ORDER TIME ======", order_time);

            const orderHistory = await OrderHistory.create({
                order_id: orderHeader.order_id, status_id: laundry_service_flow.status_id,
                status_date: order_date, active_flag: laundry_service_flow.status_desc,
                user_id: orderRequest.user_id, status_time: order_time, item_count: orderHeader.item_count,
                total_price: orderHeader.total_price
            }, { transaction: t });
            console.log("\n\n ORDER HISTORY CREATED======", JSON.stringify(orderHistory, 0, 2));

            console.log("\n\n ORDER ADDED SUCCESSFULLY ");
            orderHeader.setDataValue('cancel_date_time', cancel_date_time);
            orderHeader.setDataValue('order_receive_date_time', order_receive_date_time);

            // order_payment_mapping table entery for orders
            const orderPaymentMapping = await OrderPaymentMapping.create(
                { order_id: orderHeader.order_id, virtual_order_id: orderHeadersRes.virtual_order_id }, { transaction: t });


            orderHeaders.push(orderHeader);
        }));

        orderHeadersRes.order_headers = orderHeaders;

        // delete the cart
        const cartDetails = await CartDetails.findAll({
            where: { cart_id: orderHeadersRes.cart_id }
        });
        await Promise.all(cartDetails.map(async (element) => {
            await CartDetails.destroy({ where: { cart_id: element.cart_id }, transaction: t });
        }));
        await CartHeader.destroy({ where: { user_id: orderHeadersRes.user_id, cart_id: orderHeadersRes.cart_id }, transaction: t });

        // create paytm txn
        // const mid = "zoTQbG62503921214041";// Staging
        // const mkey = "9k3kwpFqGWOwMNv3";// Staging
        // const mid = "bOFZLO71633079149423";// Production
        // const mkey = "F8TC2oyAOtRJqKoJ";// Production
        /* body parameters */
        const paytmParams = {};
        paytmParams.body = {
            /* for custom checkout value is 'Payment' and for intelligent router is 'UNI_PAY' */
            "requestType": "Payment",

            /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
            "mid": mid,

            /* Find your Website Name in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
            // "websiteName": "WEBSTAGING", //Staging
            "websiteName": "DEFAULT",
            /* Enter your unique order id */
            // "orderId": orderHeader.order_id,
            "orderId": orderHeadersRes.virtual_order_id,

            /* on completion of transaction, we will send you the response on this URL */
            // "callbackUrl": `https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=${orderHeader.order_id}`,
            "callbackUrl": `https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=${orderHeadersRes.virtual_order_id}`,
            //"enablePaymentMode": [{ "mode": "UPI", "channels": ["UPIPUSH"] }],

            /* Order Transaction Amount here */
            "txnAmount": {

                /* Transaction Amount Value */
                // "value": orderHeader.total_price,
                "value": orderHeadersRes.order_total_price,

                /* Transaction Amount Currency */
                "currency": "INR",
            },

            /* Customer Infomation here */
            "userInfo": {

                /* unique id that belongs to your customer */
                // "custId": orderHeader.user_id,
                "custId": orderHeadersRes.user_id,
            },
        };

        console.log("paytmParams.body ======", JSON.stringify(paytmParams.body, 0, 2));
        /**
         * Generate checksum by parameters we have in body
         * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
         */
        checksum_lib.genchecksumbystring(JSON.stringify(paytmParams.body), mkey, function (err, checksum) {
            if (err) throw err;
            console.log("checksum ======", checksum);
            /**
            * Verify Checksum
            * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
            */
            /* const isValidChecksum = checksum_lib.verifychecksumbystring(JSON.stringify(paytmParams.body), mkey, checksum);
            if (isValidChecksum) {
                console.log("Checksum Matched");
            } else {
                console.log("Checksum Mismatched");
            } */

            /* head parameters */
            paytmParams.head = {

                /* put generated checksum value here */
                "signature": checksum
            };

            console.log("paytmParams ======", paytmParams);
            /* prepare JSON string for request */
            const post_data = JSON.stringify(paytmParams);
            const options = {

                /* for production */
                hostname: 'securegw.paytm.in',

                /* for Production */
                // hostname: 'securegw.paytm.in',

                port: 443,
                // path: `/theia/api/v1/initiateTransaction?mid=${mid}&orderId=${orderHeader.order_id}`,
                path: `/theia/api/v1/initiateTransaction?mid=${mid}&orderId=${orderHeadersRes.virtual_order_id}`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': post_data.length
                }
            };
            // Set up the request
            let response = "";
            let post_req = https.request(options, function (post_res) {
                post_res.on('data', function (chunk) {
                    response += chunk;
                });

                post_res.on('end', function () {
                    console.log('Response: ', response);
                    const txnToken = JSON.parse(response).body.txnToken;
                    //--------------------------------------------------
                    console.log('response.body.txnToken: ', txnToken);
                    // orderHeader.setDataValue('txnToken', txnToken);
                    // orderHeader.setDataValue('mid', mid);
                    // orderHeader.setDataValue('callbackurl', `https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=${orderHeader.order_id}`);
                    orderHeadersRes.txnToken = txnToken;
                    orderHeadersRes.mid = mid;
                    orderHeadersRes.callbackurl = `https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=${orderHeadersRes.virtual_order_id}`;
                    /*
                    var checkPaymentStatusId = setInterval(function () {
                         console.log("\n\nTRY TO CHECK PAYTM PAYMENT_STATUS IN ORDERHEADER");
                         var result = checkPaymentStatus(orderHeader.order_id);
                         if (!result) {
                             console.log("\n\nDUE TO UNPAID PAYMENT-STATUS ROLLBACK CREATED ORDER AFTER 10 MINUTES FOR ORDER:-", orderHeader.order_id);
                             rollbackCreatedOrder(orderHeader.order_id);
                             clearInterval(checkPaymentStatusId);
                         }
                     }, 600000);
                     */

                    var paytmParams1 = {};
                    paytmParams1["MID"] = mid;

                    /* Enter your order id which needs to be check status for */
                    // paytmParams1["ORDERID"] = orderHeader.order_id;
                    paytmParams1["ORDERID"] = orderHeadersRes.virtual_order_id;

                    checksum_lib.genchecksum(JSON.stringify(paytmParams1), mkey, function (err, checksum) {
                        if (err) throw err;
                        // orderHeader.setDataValue('checksum', checksum);
                        orderHeadersRes.checksum = checksum;
                        orderHeadersRes.order_header_list = orderHeadersRes.order_headers;
                        orderHeadersRes.order_headers = undefined;
                        // commit txn
                        t.commit();
                        // res.json({ status: 1, message: "order added successfully", data: orderHeader });
                        return res.json({ status: 1, message: "order added successfully", data: orderHeadersRes });
                    });
                });
            });
            // post the data
            post_req.write(post_data);
            post_req.end();
        });
    } catch (err) {
        t.rollback();
        console.log(err);
        return res.status(404).send(err);
    }
}

// RECREATE MLTIPLE ORDERS CREATED BY PAYTM ( Order Request Send After Failed|Pending|NA orderTxnStatus method )
exports.recreateMultipleOrders2 = async (req, res) => {
    console.log('\norder.controller.recreateMultipleOrders2  triggered -->');

    const reqBody = req.body;
    const {
        OrderNotifications, Login, EmployeeLogin, UserMaster, DelboyOrderMapping, Employee,
        OrderHeader, OrderDetails, OrderHistory, Address, LaundryServiceFlow, CartDetails,
        CartHeader, ServiceAvailabilityPincode, OrderPaymentMapping, db1Conn
    } = await getModels();
    const orderHeaders = [];
    const orderHeadersRes = {};

    // validate orderRequests
    const validationErros = await validateOrderRequests2(reqBody);
    console.log("\n\n validationErros======", validationErros);
    if (validationErros.length) {
        return res.send({ status: 0, message: validationErros[0], data: {} });
    }

    // generate unique paytm virtual id  
    orderHeadersRes.virtual_order_id = new Date().getTime();
    // attach order_total_price and user_id attached to orderHeadersRes
    orderHeadersRes.order_total_price = reqBody.order_total_price;
    orderHeadersRes.order_total_quantity = reqBody.order_total_quantity;
    orderHeadersRes.user_id = reqBody.user_id;
    orderHeadersRes.order_header_list = reqBody.order_header_list;
    const t = await db1Conn.transaction();
    try {
        // create all new orders
        await Promise.all(orderHeadersRes.order_header_list.map(async (orderRequest) => {
            console.log("\n\n ORDER_REQUEST_DETAIL======", JSON.stringify(orderRequest, 0, 2));

            // Here status_id and module_id are hard coded, it will be dynamic in future
            let laundry_service_flow = await LaundryServiceFlow.findOne({
                where: { status_id: order_creation_id, module_id: 1 }
            }, { transaction: t });
            console.log("\n\n LAUNDRY SERVICE FLOW======", JSON.stringify(laundry_service_flow, 0, 2));
            const format = 'hh:mm:ss A';

            const startMoment_1 = moment('12:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_1 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            console.log("\n\n startMoment_1===", startMoment_1);
            console.log("\n\n endMoment_1===", endMoment_1);

            const startMoment_2 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_2 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);

            const startMoment_3 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_3 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);

            const startMoment_4 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_4 = moment('23:59:59 PM', format).utc().utcOffset(utcOffsetMins);

            let order_receive_date_time = "";
            const order_moment = moment().utc().utcOffset(utcOffsetMins);
            const cancel_order_moment = order_moment.clone().add(15, 'minutes');

            if (order_moment.isSameOrAfter(startMoment_1) && order_moment.isBefore(endMoment_1)) {
                order_receive_date_time = "Thanks for your order. Your clothes will be picked up by our delivery partner today between 8:00 AM and 1:30 PM";
                console.log("\n\n order_receive_date_time=", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_2) && order_moment.isBefore(endMoment_2)) {
                order_receive_date_time = "Thanks for your order. Your clothes will be picked up by our delivery partner today between 8:00 AM and 1:30 PM";
                console.log("\n\n order_receive_date_time==", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_3) && order_moment.isBefore(endMoment_3)) {
                order_receive_date_time = "Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
                console.log("\n\n order_receive_date_time===", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_4) && order_moment.isBefore(endMoment_4)) {
                order_receive_date_time = "Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
                console.log("\n\n order_receive_date_time====", order_receive_date_time);
            }
            let order_date_time = order_moment.format("DD/MM/YYYY hh:mm A");
            let order_date_time_array = order_date_time.split(" ");
            let order_date = order_date_time_array[0];
            let order_time = order_date_time_array[1] + " " + order_date_time_array[2];
            let cancel_date_time = cancel_order_moment.format("DD/MM/YYYY hh:mm A");
            let cancel_date_time_array = cancel_date_time.split(" ");
            let cancel_date = cancel_date_time_array[0];
            let cancel_time = cancel_date_time_array[1] + " " + cancel_date_time_array[2];
            console.log("\n\n order_date_time ======", order_date_time);
            console.log("\n\n order_date  ======", order_date);
            console.log("\n\n order_time  ======", order_time);
            console.log("\n\n cancel_date_time ======", cancel_date_time);
            console.log("\n\n cancel_date  ======", cancel_date);
            console.log("\n\n cancel_time  ======", cancel_time);
            console.log("\n\n ORDER MONENT ======", order_moment);
            console.log("\n\n CANCEL  ORDER MONENT ======", cancel_order_moment);
            console.log("\n\n DELIVERY ORDER MONENT ======", order_receive_date_time);

            const orderHeader = await OrderHeader.update({
                order_date: order_date_time, item_count: orderRequest.grand_total_quantity,
                total_price: orderRequest.grand_total_price, address: orderRequest.address,
                pincode: orderRequest.address.pincode, payment_mode: 'ONLINE', payment_status: 'UNPAID',
                virtual_order_id: orderHeadersRes.virtual_order_id, order_flag: 0, category_id: orderRequest.order_details[0].category_id
            },
                { where: { virtual_order_id: orderRequest.virtual_order_id, order_id: orderRequest.order_id, user_id: orderRequest.user_id } }, { transaction: t });
            console.log("\n\nRECREATED ORDERHEADER UPDATED ======", JSON.stringify(orderHeader, 0, 2));

            if (orderRequest.address.address_id) {
                let address = await Address.findOne({
                    where: { address_id: orderRequest.address.address_id },
                    attributes: ['address1', 'address2', 'address3', 'city', 'state', 'pincode',
                        'longitude', 'latitude', 'flag_default', 'phoneno']
                });
                console.log("\n\n RECREATE EXISTING ADDRESS ======", JSON.stringify(address, 0, 2));

                await Address.update({ flag_default: 'secondary' },
                    { where: { user_id: orderRequest.user_id } }, { transaction: t });

                await Address.update({ flag_default: orderRequest.address.flag_default },
                    { where: { address_id: orderRequest.address.address_id } }, { transaction: t });
            }
            else {
                await Address.update({ flag_default: 'secondary' },
                    { where: { user_id: orderRequest.user_id } }, { transaction: t });

                const address = await Address.create({
                    address1: orderRequest.address.address1,
                    address2: orderRequest.address.address2, address3: orderRequest.address.address3,
                    city: orderRequest.address.city, state: orderRequest.address.state,
                    pincode: orderRequest.address.pincode, longitude: orderRequest.address.longitude,
                    latitude: orderRequest.address.latitude, flag_default: orderRequest.address.flag_default,
                    user_id: orderRequest.user_id, phoneno: orderRequest.address.phoneno
                }, { transaction: t });
                console.log("\n\n RECREATE ADDRESS CREATED======", JSON.stringify(address, 0, 2));
            }

            await Promise.all(orderRequest.order_details.map(async (element) => {
                console.log("\n\n RECREATE ORDER_DETAIL======", JSON.stringify(element, 0, 2));
                let orderDetails = await OrderDetails.update(
                    { prod_id: element.prod_id, customer_order_quantity: element.quantity },
                    { where: { order_id: orderRequest.order_id, user_id: orderRequest.user_id } }, { transaction: t });
                console.log("\n\n RECREATE ORDER_DETAIL UPDATED SUCCESSFULLY ");
            }));
            console.log("\n\n RECREATE ORDERHEADER CREATED ======", JSON.stringify(orderHeader, 0, 2));
            console.log("\n\n RECREATE ORDERHEADER ITEM COUNT ======", orderHeader.item_count);
            console.log("\n\n RECREATE ORDERHEADER ITEM PRICE ======", orderHeader.total_price);
            console.log("\n\n RECREATE ORDER DATE ======", order_date);
            console.log("\n\n RECREATE ORDER TIME ======", order_time);

            const orderHistory = await OrderHistory.update({
                status_date: order_date, status_time: order_time,
                item_count: orderHeader.item_count, total_price: orderHeader.total_price
            },
                { where: { order_id: orderRequest.order_id, user_id: orderRequest.user_id } }, { transaction: t });
            console.log("\n\n RECREATE ORDER HISTORY UPDATED======", JSON.stringify(orderHistory, 0, 2));

            console.log("\n\n RECREATE ORDER UPDATED SUCCESSFULLY ");
            orderHeader.setDataValue('cancel_date_time', cancel_date_time);
            orderHeader.setDataValue('order_receive_date_time', order_receive_date_time);

            // order_payment_mapping table entery for orders
            const orderPaymentMapping = await OrderPaymentMapping.create(
                { order_id: orderHeader.order_id, virtual_order_id: orderHeadersRes.virtual_order_id }, { transaction: t });

            orderHeaders.push(orderHeader);
        }));

        orderHeadersRes.order_headers = orderHeaders;

        // create paytm txn
        // const mid = "zoTQbG62503921214041";// Staging
        // const mkey = "9k3kwpFqGWOwMNv3";// Staging
        // const mid = "bOFZLO71633079149423";// Production
        // const mkey = "F8TC2oyAOtRJqKoJ";// Production
        /* body parameters */
        const paytmParams = {};
        paytmParams.body = {
            /* for custom checkout value is 'Payment' and for intelligent router is 'UNI_PAY' */
            "requestType": "Payment",

            /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
            "mid": mid,

            /* Find your Website Name in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
            // "websiteName": "WEBSTAGING", //Staging
            "websiteName": "DEFAULT",
            /* Enter your unique order id */
            // "orderId": orderHeader.order_id,
            "orderId": orderHeadersRes.virtual_order_id,

            /* on completion of transaction, we will send you the response on this URL */
            // "callbackUrl": `https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=${orderHeader.order_id}`,
            "callbackUrl": `https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=${orderHeadersRes.virtual_order_id}`,
            //"enablePaymentMode": [{ "mode": "UPI", "channels": ["UPIPUSH"] }],

            /* Order Transaction Amount here */
            "txnAmount": {

                /* Transaction Amount Value */
                // "value": orderHeader.total_price,
                "value": orderHeadersRes.order_total_price,

                /* Transaction Amount Currency */
                "currency": "INR",
            },

            /* Customer Infomation here */
            "userInfo": {

                /* unique id that belongs to your customer */
                // "custId": orderHeader.user_id,
                "custId": orderHeadersRes.user_id,
            },
        };

        console.log("paytmParams.body ======", JSON.stringify(paytmParams.body, 0, 2));
        /**
         * Generate checksum by parameters we have in body
         * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
         */
        checksum_lib.genchecksumbystring(JSON.stringify(paytmParams.body), mkey, function (err, checksum) {
            if (err) throw err;
            console.log("checksum ======", checksum);
            /**
            * Verify Checksum
            * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
            */
            /* const isValidChecksum = checksum_lib.verifychecksumbystring(JSON.stringify(paytmParams.body), mkey, checksum);
            if (isValidChecksum) {
                console.log("Checksum Matched");
            } else {
                console.log("Checksum Mismatched");
            } */

            /* head parameters */
            paytmParams.head = {

                /* put generated checksum value here */
                "signature": checksum
            };

            console.log("paytmParams ======", paytmParams);
            /* prepare JSON string for request */
            const post_data = JSON.stringify(paytmParams);
            const options = {

                /* for production */
                hostname: 'securegw.paytm.in',

                /* for Production */
                // hostname: 'securegw.paytm.in',

                port: 443,
                // path: `/theia/api/v1/initiateTransaction?mid=${mid}&orderId=${orderHeader.order_id}`,
                path: `/theia/api/v1/initiateTransaction?mid=${mid}&orderId=${orderHeadersRes.virtual_order_id}`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': post_data.length
                }
            };
            // Set up the request
            let response = "";
            let post_req = https.request(options, function (post_res) {
                post_res.on('data', function (chunk) {
                    response += chunk;
                });

                post_res.on('end', function () {
                    console.log('Response: ', response);
                    const txnToken = JSON.parse(response).body.txnToken;
                    //--------------------------------------------------
                    console.log('response.body.txnToken: ', txnToken);
                    // orderHeader.setDataValue('txnToken', txnToken);
                    // orderHeader.setDataValue('mid', mid);
                    // orderHeader.setDataValue('callbackurl', `https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=${orderHeader.order_id}`);
                    orderHeadersRes.txnToken = txnToken;
                    orderHeadersRes.mid = mid;
                    orderHeadersRes.callbackurl = `https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=${orderHeadersRes.virtual_order_id}`;
                    /*
                    var checkPaymentStatusId = setInterval(function () {
                         console.log("\n\nTRY TO CHECK PAYTM PAYMENT_STATUS IN ORDERHEADER");
                         var result = checkPaymentStatus(orderHeader.order_id);
                         if (!result) {
                             console.log("\n\nDUE TO UNPAID PAYMENT-STATUS ROLLBACK CREATED ORDER AFTER 10 MINUTES FOR ORDER:-", orderHeader.order_id);
                             rollbackCreatedOrder(orderHeader.order_id);
                             clearInterval(checkPaymentStatusId);
                         }
                     }, 600000);
                     */

                    var paytmParams1 = {};
                    paytmParams1["MID"] = mid;

                    /* Enter your order id which needs to be check status for */
                    // paytmParams1["ORDERID"] = orderHeader.order_id;
                    paytmParams1["ORDERID"] = orderHeadersRes.virtual_order_id;

                    checksum_lib.genchecksum(JSON.stringify(paytmParams1), mkey, function (err, checksum) {
                        if (err) throw err;
                        // orderHeader.setDataValue('checksum', checksum);
                        orderHeadersRes.checksum = checksum;
                        orderHeadersRes.order_header_list = orderHeadersRes.order_headers;
                        orderHeadersRes.order_headers = undefined;
                        // commit txn
                        t.commit();
                        // res.json({ status: 1, message: "order added successfully", data: orderHeader });
                        return res.json({ status: 1, message: "order added successfully", data: orderHeadersRes });
                    });
                });
            });
            // post the data
            post_req.write(post_data);
            post_req.end();
        });
    } catch (err) {
        t.rollback();
        console.log(err);
        return res.status(404).send(err);
    }
}

// Cancel individual Order Of COD
/*
{
"user_id":12,
"order_id":1010,
"virtual_order_id":123244434343
}
*/
// Cancel Single Order for COD
exports.cancelOrder = async (req, res) => {
    console.log('\norder.controller.cancelOrder  triggered -->');

    const { OrderNotifications, UserMaster, EmployeeLogin, OrderHeader, OrderDetails, OrderHistory, CancelOrderHistory, DelboyOrderMapping, Address, LaundryServiceFlow, CartDetails, CartHeader, db1Conn } = await getModels();
    const reqBody = req.body;

    const order = await OrderHeader.findOne({
        where: { user_id: req.body.user_id, order_id: req.body.order_id },
        include: [
            { model: OrderDetails, as: 'order_details' },
            { model: OrderHistory, as: 'order_history' }
        ],
        order: [['order_id', 'DESC']]
    });

    const order_date_time = order.order_date;
    var format = 'DD/MM/YYYY hh:mm A';
    const db_order_moment = moment.utc(order_date_time, format, true)
        .utcOffset(utcOffsetMins)
        .subtract(utcOffsetMins, 'minutes');
    const cancel_order_moment = db_order_moment.clone().add(15, 'minutes');
    const current_cancel_moment = moment().utc().utcOffset(utcOffsetMins);

    if (current_cancel_moment.isSameOrBefore(cancel_order_moment)) {
        const t = await db1Conn.transaction();
        try {
            /*  await OrderHeader.destroy({ where: { user_id: reqBody.user_id, order_id: reqBody.order_id }, transaction: t });
             let orderDetails = await OrderDetails.findAll({ where: { user_id: reqBody.user_id, order_id: reqBody.order_id } });
             await Promise.all(orderDetails.map(async (element) => {
                 await OrderDetails.destroy({ where: { user_id: element.user_id, order_id: element.order_id }, transaction: t });
             }));
 
             await OrderHistory.destroy({ where: { order_id: reqBody.order_id }, transaction: t });
             let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: reqBody.order_id, delivery_type: 1 } });
             await DelboyOrderMapping.destroy({ where: { order_id: reqBody.order_id }, transaction: t });
          */
            //  let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: reqBody.order_id, delivery_type: 1 } });
            await OrderHeader.update({ order_flag: 2, status_id: 1060 },
                { where: { virtual_order_id: reqBody.virtual_order_id, order_id: reqBody.order_id } }, { transaction: t });

            let cancelOrderDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm A");
            //let order_date_time = order_moment.format("DD/MM/YYYY hh:mm a");
            let cancel_order_date_time_array = cancelOrderDate.split(" ");
            let cancel_order_date = cancel_order_date_time_array[0];
            let cancel_order_time = cancel_order_date_time_array[1] + " " + cancel_order_date_time_array[2];
            await CancelOrderHistory.create({
                cancel_user_id: order.user_id, cancel_order_id: order.order_id, cancel_order_details: order,
                cancel_order_date: cancelOrderDate, virtual_order_id: reqBody.virtual_order_id
            }, { transaction: t });
            await OrderHistory.create({
                order_id: order.order_id, status_id: 1060, status_date: cancel_order_date,
                active_flag: "Order Canceled By Customer", user_id: order.user_id, status_time: cancel_order_time,
                item_count: order.item_count, total_price: order.total_price
            }, { transaction: t });
            t.commit()
            console.log("\n\n ORDER CANCELEDED SUCCESSFULLY ");

            let userDetails = await UserMaster.findOne({ where: { user_id: order.user_id } });
            let customer_name = "";
            let delby_fcmToken = "";
            let msg_body_forDBoy = "";
            if (userDetails.login_type === 'Otp') {
                customer_name = userDetails.user_fname
            }
            else {
                customer_name = userDetails.user_fname + " " + userDetails.user_lname;
            }
            let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: reqBody.order_id, delivery_type: 1 } });
            console.log("\n\n CANCEL DP  ID:", dboy_order_mapping.emp_id);
            console.log("\n\n CANCEL ORDER -NO :", reqBody.order_id);
            if (dboy_order_mapping) {
                let employee_login = await EmployeeLogin.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                delby_fcmToken = employee_login.fcmToken;
                msg_body_forDBoy = 'Order No -' + order.order_id +
                    ' of customer  name -' + customer_name + ' has been canceled.';
            }

            const fetchBody = {
                notification: {
                    title: "iestre",
                    text: msg_body_forDBoy,
                    click_action: "com.maks.iestredeliveryboy.NotificationActivity"
                },
                data: {
                    orderId: order.order_id
                },
                to: delby_fcmToken
            };
            let result = dpushToFirebase(fetchBody, 3);
            console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY FOR ORDER PICKUP AFTER ORDER COMPLETE BY LMAN");
            console.log("\n\n CANCEL ORDER NOTIFICTION FOR DP  ID:", dboy_order_mapping.emp_id);
            console.log("\n\nORDER - NO:", order.order_id);
            const dorderNotifications = await OrderNotifications.create(
                {
                    order_id: order.order_id,
                    status_id: 1060,
                    user_id: order.user_id,
                    emp_id: dboy_order_mapping.emp_id,
                    role_id: 2,
                    msg_title: 'Order has been canceled',
                    msg_body: msg_body_forDBoy,
                    created_at: cancelOrderDate
                });

            /* const order1 = await OrderHeader.findOne({
                where: { user_id: reqBody.user_id, order_id: reqBody.order_id, virtual_order_id: reqBody.virtual_order_id },
                include: [
                    { model: OrderDetails, as: 'order_details' },
                    { model: OrderHistory, as: 'order_history' }
                ],
                order: [['order_id', 'DESC']]
            }); */

            console.log("\n\n CANCEL NOTIFICATION ENTERY  SUCCESSFULLY ");
            return res.json({ status: 1, message: "order canceled successfully", data: {} });
        }
        catch (error) {
            t.rollback();
            console.log("\n\n NOTIFICATION NOT SEND  SUCCESSFULLY TO DELIVERY BOY FOR CANCEL ORDER ERROR " + error);
            return res.status(404).send(error);
        }
    }
    else {
        console.log('\n\n Order cancel time exceed, Sending Error Response...');
        return res.json({ status: 0, message: "Order cancel time exceed", data: {} })
    }
}

// Cancel multiple Orders for COD (To be implemented)
exports.cancelMultipleOrder = async (req, res) => {
}


/* Cancel either single or multiple Orders For ONLINE PAYMENT
{
"user_id":12
"virtual_order_id":123244434343,
"order_ids":[1010,1011,1011]
}
*/
exports.cancelMultipleOrder2 = async (req, res) => {
    console.log('\norder.controller.cancelMultipleOrder2  triggered -->');
    const { OrderNotifications, UserMaster, EmployeeLogin, OrderHeader, OrderDetails, OrderHistory, CancelOrderHistory, DelboyOrderMapping, Address, LaundryServiceFlow, CartDetails, CartHeader, db1Conn } = await getModels();
    const reqBody = req.body;
    const virtual_order_id = reqBody.virtual_order_id;
    const orderIds = reqBody.order_ids;
    const user_id = reqBody.user_id;
    const orders = [];
    const order_date_time = order.order_date;
    var format = 'DD/MM/YYYY hh:mm A';
    // const db_order_moment = moment(order_date_time, format).utc().utcOffset(utcOffsetMins);
    // const cancel_order_moment = db_order_moment.clone().add(15, 'minutes');
    //const db_order_moment = moment.utc(order_date_time, format, true).utcOffset(utcOffsetMins);

    const db_order_moment = moment.utc(order_date_time, format, true)
        .utcOffset(utcOffsetMins)
        .subtract(utcOffsetMins, 'minutes');

    const cancel_order_moment = db_order_moment.clone().add(15, 'minutes');
    const current_cancel_moment = moment().utc().utcOffset(utcOffsetMins);
    if (current_cancel_moment.isSameOrBefore(cancel_order_moment)) {
        const t = await db1Conn.transaction();
        try {
            /* await OrderHeader.destroy({ where: { user_id: reqBody.user_id, order_id: reqBody.order_id }, transaction: t });
            let orderDetails = await OrderDetails.findAll({ where: { user_id: reqBody.user_id, order_id: reqBody.order_id } });
            await Promise.all(orderDetails.map(async (element) => {
                await OrderDetails.destroy({ where: { user_id: element.user_id, order_id: element.order_id }, transaction: t });
            }));

            await OrderHistory.destroy({ where: { order_id: reqBody.order_id }, transaction: t });
            let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: reqBody.order_id, delivery_type: 1 } });
            await DelboyOrderMapping.destroy({ where: { order_id: reqBody.order_id }, transaction: t });
            
            */
            await Promise.all(orderIds.map(async (order_id) => {
                const order = await OrderHeader.findOne({
                    where: { user_id, order_id },
                    include: [
                        { model: OrderDetails, as: 'order_details' },
                        { model: OrderHistory, as: 'order_history' }
                    ],
                    order: [['order_id', 'DESC']]
                });

                await OrderHeader.update({ order_flag: 2 },
                    { where: { virtual_order_id: virtual_order_id, order_id: order_id } }, { transaction: t });

                let cancelOrderDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm:ss A");
                await CancelOrderHistory.create({
                    cancel_user_id: user_id, cancel_order_id: order_id, cancel_order_details: order,
                    cancel_order_date: cancelOrderDate, virtual_order_id: virtual_order_id
                }, { transaction: t });
                t.commit();

                console.log("\n\n ORDER CANCELEDED SUCCESSFULLY ");
                let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: order_id, delivery_type: 1 } });
                let userDetails = await UserMaster.findOne({ where: { user_id } });
                let customer_name = "";
                let delby_fcmToken = "";
                let msg_body_forDBoy = "";
                if (userDetails.login_type === 'Otp') {
                    customer_name = userDetails.user_fname
                }
                else {
                    customer_name = userDetails.user_fname + " " + userDetails.user_lname;
                }

                if (dboy_order_mapping) {
                    let employee_login = await EmployeeLogin.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                    delby_fcmToken = employee_login.fcmToken;
                    msg_body_forDBoy = 'Order No -' + order.order_id +
                        ' of customer  name -' + customer_name + ' has been canceled.';
                }

                const fetchBody = {
                    notification: {
                        title: "iestre",
                        text: msg_body_forDBoy,
                        click_action: "com.maks.iestredeliveryboy.NotificationActivity"
                    },
                    data: {
                        orderId: order_id
                    },
                    to: delby_fcmToken
                };
                let result = dpushToFirebase(fetchBody, 3);
                console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY FOR ORDER PICKUP AFTER ORDER COMPLETE BY LMAN");

                const dorderNotifications = await OrderNotifications.create(
                    {
                        order_id: order_id,
                        status_id: 1060,
                        user_id: user_id,
                        emp_id: dboy_order_mapping.emp_id,
                        role_id: 2,
                        msg_title: 'Order has been canceled',
                        msg_body: msg_body_forDBoy,
                        created_at: cancelOrderDate
                    });


                console.log("\n\n CANCEL NOTIFICATION ENTERY  SUCCESSFULLY ");
                orders.push(order)
            }));


            return res.json({ status: 1, message: "order canceled successfully", data: orders });
        }
        catch (error) {
            t.rollback();
            console.log("\n\nCANCEL UNSUCCESS AND  NOTIFICATION NOT SEND  SUCCESSFULLY TO DELIVERY BOY FOR CANCEL ORDER ERROR " + error);
            return res.status(404).send(error);
        }
    }
    else {
        console.log("\n\n HIIIIIIIIIIIIIIIIIIIIIIIIII");
        console.log('\n\n Order cancel time exceed, Sending Error Response...');
        // const errorResponse = {
        //     status: 0,
        //     message: 'Order cancel time exceed',
        // };
        // return res.status(HttpStatus.BAD_REQUEST).send(errorResponse);
        return res.json({ status: 0, message: "Order cancel time exceed", data: {} })
    }

}

exports.listOrderDetails = async (req, res) => {
    console.log('\norder.controller.listOrderDetails  triggered -->');

    const reqBody = req.body;
    const { OrderHeader, OrderDetails, OrderHistory, ProductMaster, PriceMaster } = await getModels();
    try {
        const orders = await OrderHeader.findAll({
            where: {
                user_id: req.params.user_id,
                order_flag: 1,
                status_id: {
                    [SequelizeOp.or]: [1001, 1010, 1020, 1030, 1040]
                },
            },
            include: [
                { model: OrderDetails, as: 'order_details' },
                { model: OrderHistory, as: 'order_history' }
            ],
            order: [['order_id', 'DESC']]
        });

        console.log("\n\n ORDER LIST======", JSON.stringify(orders, 0, 2));

        await Promise.all(orders.map(async (element) => {
            console.log("\n\n ORDER Details======", JSON.stringify(element.order_details, 0, 2));

            await Promise.all(element.order_details.map(async (element_1) => {
                console.log("\n\n INV ORDER Details======", JSON.stringify(element_1, 0, 2));

                // here select price from price_master based on prod_id
                let price_master = await PriceMaster.findOne({ where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' } });
                console.log("PRODUCT DETAILS======", JSON.stringify(price_master, 0, 2));
                console.log('\nproduct ' + element_1.prod_id + ' price of ' + price_master.prod_price);

                // here select product_name from product_master based on prod_id
                let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
                console.log("PRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));

                // let quantity = parseInt(element.quantity);
                // console.log('\nProduct quantity ' + quantity);
                // let total_price = parseFloat(product.prod_price) * quantity;

                // console.log('\nTotal Price ' + total_price);
                element_1.setDataValue('prod_name', product_master.prod_name);
                element_1.setDataValue('prod_code', product_master.prod_code);
                element_1.setDataValue('per_unit_price', price_master.prod_price);
                // element.setDataValue('total_price', total_price.toFixed(2));
            }));
        }));

        res.json({ status: 1, message: "list of orders ", data: orders })
    }
    catch (error) {
        console.log("ERROR==" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
    }
}

exports.completedOrderList = async (req, res) => {
    console.log('\norder.controller.completedOrderList  triggered -->');

    const reqBody = req.body;

    try {
        const { OrderHeader, OrderDetails, OrderHistory, ProductMaster, PriceMaster, Address, LaundryServiceFlow, CartDetails, CartHeader, db1Conn } = await getModels();
        //let attributes = ['OrderHeader.order_id', 'OrderHeader.user_id', 'OrderHeader.order_date', 'OrderHeader.item_count_upd', 'OrderHeader.expected_delivery_time'];


        let orderHistories = await OrderHistory.findAll({
            where: {
                user_id: reqBody.user_id,
                status_id: {
                    [SequelizeOp.or]: [1050, 1060]
                },
            },
            order: [['status_date', 'DESC'],
            ['status_time', 'DESC']]
        });

        let orders = [];
        console.log("\n\nCOMPLETE ORDERS FROM ORDER HISTORIES======", JSON.stringify(orderHistories, 0, 2));
        if (orderHistories) {
            await Promise.all(orderHistories.map(async (element) => {
                //         console.log('\nUser--' + element.user_id + ' having  orderId ' + element.order_id);
                //         // here select all order deatils list  from order_details  based on user_id and order_id
                //         let orderDetails = await OrderDetails.findAll({ where: { user_id: element.user_id, order_id: element.order_id } });
                console.log("\n\nORDER_ID======" + element.order_id);

                const order = await OrderHeader.findOne({
                    where: { order_id: element.order_id },
                    include: [
                        { model: OrderDetails, as: 'order_details' },
                        { model: OrderHistory, as: 'order_history' }
                    ]
                });
                orders.push(order);
            }));

            await Promise.all(orders.map(async (element) => {
                console.log("\n\n COMPLETE ORDER Details======", JSON.stringify(element.order_details, 0, 2));

                await Promise.all(element.order_details.map(async (element_1) => {
                    console.log("\n\n INV ORDER Details======", JSON.stringify(element_1, 0, 2));

                    // here select price from price_master based on prod_id
                    let price_master = await PriceMaster.findOne({
                        where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' }
                    });
                    // console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
                    console.log('\nproduct ' + element_1.prod_id + ' price of ' + price_master.prod_price);

                    // here select product_name from product_master based on prod_id
                    let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
                    console.log("PRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));

                    // let quantity = parseInt(element.quantity);
                    // console.log('\nProduct quantity ' + quantity);
                    // let total_price = parseFloat(product.prod_price) * quantity;

                    // console.log('\nTotal Price ' + total_price);
                    element_1.setDataValue('prod_name', product_master.prod_name);
                    element_1.setDataValue('prod_code', product_master.prod_code);
                    element_1.setDataValue('per_unit_price', price_master.prod_price);
                    // element.setDataValue('total_price', total_price.toFixed(2));
                }));
            }));

            console.log("\n\nCOMPLETE ORDER LIST DETAILS======", JSON.stringify(orders, 0, 2));
            res.json({ status: 1, message: "complete order details", data: orders });
        }
        else {
            // res.json({ status: 1, message: "cart details", data: "no cart" });
            res.json({ status: 0, message: "empty order details list", data: {} });
        }
    }
    catch (error) {
        console.log(error);
        res.status(404).send(error);
    }
}

const sendSMSDemo = async (order_id, user_id, address) => {
    console.log('\norder.controller.sendSMSDemo triggered -->');
    console.log('\By order creation SMS sends to three admin -->');
    const { UserMaster, db1Conn } = await getModels();
    try {
        let user = await UserMaster.findOne({ where: { user_id: user_id } });
        if (user) {
            let customername = user.user_fname + " " + user.user_lname;
            let url;
            let url_1;
            let url_2;
            let url_3;

            if (user.user_phoneno) {
                console.log("\nUSER PHONE NO :" + user.user_phoneno);
                url = env.sms.URI + "9730452025" + '&text=Order created by ' + customername + ', order no ' + order_id + ', phone no-' + user.user_phoneno + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude + '&priority=ndnd&stype=normal';
                url_1 = env.sms.URI + "7499671650" + '&text=Order created by ' + customername + ', order no ' + order_id + ', phone no-' + user.user_phoneno + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude + '&priority=ndnd&stype=normal';
                url_2 = env.sms.URI + "9903053297" + '&text=Order created by ' + customername + ', order no ' + order_id + ', phone no-' + user.user_phoneno + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude + '&priority=ndnd&stype=normal';
                url_3 = env.sms.URI + "9503214767" + '&text=Order created by ' + customername + ', order no ' + order_id + ', phone no-' + user.user_phoneno + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude + '&priority=ndnd&stype=normal';

            }
            else {
                console.log("\nUSER HAVING NO PHONE NO :");
                url = env.sms.URI + "9730452025" + '&text=Order created by ' + customername + ', order no ' + order_id + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude + '&priority=ndnd&stype=normal';
                url_1 = env.sms.URI + "7499671650" + '&text=Order created by ' + customername + ', order no ' + order_id + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude + '&priority=ndnd&stype=normal';
                url_2 = env.sms.URI + "9903053297" + '&text=Order created by ' + customername + ', order no ' + order_id + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude + '&priority=ndnd&stype=normal';
                url_3 = env.sms.URI + "9503214767" + '&text=Order created by ' + customername + ', order no ' + order_id + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude + '&priority=ndnd&stype=normal';

            }
            let resopnse = await axios.get(url);
            let resopnse_1 = await axios.get(url_1);
            let resopnse_2 = await axios.get(url_2);
            let resopnse_3 = await axios.get(url_2);

            if (resopnse.data || resopnse_1.data || resopnse_2.data || resopnse_3.data) {
                console.log("\nresopnse.data :" + resopnse.data);
                console.log("\nresopnse_1.data :" + resopnse_1.data);
                console.log("\nresopnse_2.data :" + resopnse_2.data);
                console.log("\nresopnse_3.data :" + resopnse_3.data);
                console.log("\nRETURN  :TRUE");
                return true;
            }
            else {
                console.log("\nRETURN sendSMSDemo   :FALSE");
                return false;
            }
        }
        else {
            console.log("\nNO USER EXIST HAVING USER-ID----", user_id);
            console.log("\nRETURN OUTER IF :FALSE");
            return false;
        }
    }
    catch (error) {
        console.log('\norder.controller.sendSMSDemo  error', error);
        throw error;

    }

}


const sendSMSDemo1 = async (orderId, user_id, address) => {
    console.log('\norder.controller.sendSMSDemo1 triggered -->');
    console.log('\By order creation SMS sends to three admin using text local -->');
    const { UserMaster, db1Conn } = await getModels();
    try {
        let user = await UserMaster.findOne({ where: { user_id: user_id } });
        if (user) {
            let customername = user.user_fname + " " + user.user_lname;
            if (user.user_phoneno) {
                console.log("\nUSER PHONE NO :" + user.user_phoneno);
                //msg = ' Order created by ' + customername + ', order no ' + order_id + ', phone no - ' + user.user_phoneno + ', address - http://maps.google.com/?q=' + address.latitude + ',' + address.longitude;
                //msg = `Order created by ${customername}, order no ${order_id}, phone no - ${user.user_phoneno} , address - http://maps.google.com/?q=${address.latitude},${address.longitude}`;
                msg = urlencode(`Order created by ${customername}, order no ${orderId}, phone no - ${user.user_phoneno} , address - http://maps.google.com/?q= ${address.latitude}, ${address.longitude}`);

                var toNumbers = new Array('9503214767', '9730452025', '8308297574', '9503214767', '7499671650')
                toNumbers.forEach(function (toNumber, index) {
                    var username = 'technologiesmaks@gmail.com';
                    var hash = 'cd9926d0f0631eb9106f631e5cc21dfe705d57be4be87900b36fc673ead09e5a';
                    var sender = 'MAKTEC';
                    var data = 'username=' + username + '&hash=' + hash + '&sender=' + sender + '&numbers=' + toNumber + '&message=' + msg;
                    var options = {
                        host: 'api.textlocal.in', path: '/send?' + data
                    };
                    http.request(options, sendMsgFromTextLocal).end();
                });
            }
            else {
                console.log("\nUSER HAVING NO PHONE NO :");
                // msg = 'Order created by ' + customername + ', order no ' + order_id + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude;
                // msg = `Order created by  ${customername} , order no ${order_id} , address-http://maps.google.com/?q=${address.latitude},${address.longitude}`;
                msg = urlencode(`Order created by ${customername} , order no ${orderId} , address- http://maps.google.com/?q= ${address.latitude}, ${address.longitude}`);
                var toNumbers = new Array('9503214767', '9730452025', '8308297574', '9503214767', '7499671650');
                toNumbers.forEach(function (toNumber, index) {
                    var username = 'technologiesmaks@gmail.com';
                    var hash = 'cd9926d0f0631eb9106f631e5cc21dfe705d57be4be87900b36fc673ead09e5a';
                    var sender = 'MAKTEC';
                    var data = 'username=' + username + '&hash=' + hash + '&sender=' + sender + '&numbers=' + toNumber + '&message=' + msg;
                    var options = {
                        host: 'api.textlocal.in', path: '/send?' + data
                    };
                    http.request(options, sendMsgFromTextLocal).end();
                });

            }
            return true;
        }
        else {
            console.log("\nRETURN sendSMSDemo1   :FALSE");
            console.log("\nNO USER EXIST WITH THAT USER ID");
            return false;
        }

    }
    catch (error) {
        console.log('\norder.controller.sendSMSDemo1  error', error);
        throw error;

    }

}

const sendSMS1 = async (orderId, userId, address) => {
    console.log('\norder.controller.sendSMS1 triggered -->');
    const { UserMaster, DelboyOrderMapping, Employee, db1Conn } = await getModels();
    console.log("\nORDER_ID :" + orderId);
    console.log("\nUSER_ID :" + userId);
    console.log("\n\nADDRESS :", JSON.stringify(address, 0, 2));
    /*  {
        user_id: req.user_id
    }
    */
    try {
        let user = await UserMaster.findOne({ where: { user_id: userId } });
        let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: orderId, delivery_type: 1 } });
        if (user) {
            let customername = user.user_fname + " " + user.user_lname;
            let msg;
            let msg1;
            let userPhoneno = user.user_phoneno;

            if (dboy_order_mapping) {
                let employee = await Employee.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                let empname = employee.emp_fname + " " + employee.emp_lname;
                let empPhoneno = employee.emp_contact_no;
                if (user.user_phoneno) {
                    console.log("\nUSER PHONE NO :" + user.user_phoneno);
                    msg = urlencode(`Order created by ${customername}, order no ${orderId}, phone no - ${userPhoneno} , address - http://maps.google.com/?q= ${address.latitude}, ${address.longitude}`);
                }
                else {
                    console.log("\nUSER HAVING NO PHONE NO :");
                    /// msg = `Order created by- ${customername}, order no ${orderId},payment-mode(${payment_mode}), address-http://maps.google.com/?q=${address.latitude},${address.longitude}`;
                    msg = urlencode(`Order created by ${customername} , order no ${orderId} , address- http://maps.google.com/?q= ${address.latitude}, ${address.longitude}`);
                }
                // let resopnse = await axios.get(url);
                var username = 'technologiesmaks@gmail.com';
                var hash = 'cd9926d0f0631eb9106f631e5cc21dfe705d57be4be87900b36fc673ead09e5a';
                var sender = 'MAKTEC';
                var data = 'username=' + username + '&hash=' + hash + '&sender=' + sender + '&numbers=' + empPhoneno + '&message=' + msg;
                var options = {
                    host: 'api.textlocal.in', path: '/send?' + data
                };
                http.request(options, sendMsgFromTextLocal).end();


                if (user.user_phoneno) {
                    //  msg1 = `Thanks for ordering, Order no ${orderId} will be pick up by our delivery boy ${empname}, phone no ${emp_phoneno} `;
                    msg1 = urlencode(`Thanks for ordering. Order no ${orderId} will be pick up by our delivery boy ${empname}, phone no- ${empPhoneno}`);
                    let username = 'technologiesmaks@gmail.com';
                    let hash = 'cd9926d0f0631eb9106f631e5cc21dfe705d57be4be87900b36fc673ead09e5a';
                    let sender = 'MAKTEC';
                    let data = 'username=' + username + '&hash=' + hash + '&sender=' + sender + '&numbers=' + empPhoneno + '&message=' + msg1;
                    let options = {
                        host: 'api.textlocal.in', path: '/send?' + data
                    };
                    http.request(options, sendMsgFromTextLocal).end();
                }
                console.log("\nRETURN  :TRUE");
                return true;

            }
            else {
                console.log("\nRETURN OUTER IF :FALSE");
                return false;
            }
        }
        else {
            console.log("\nNO USER EXIST HAVING USER-ID----", user_id);
            console.log("\nRETURN OUTER IF :FALSE");
            return false;
        }
    }
    catch (error) {
        console.log('\norder.controller.sendSMS1  error', error);
        throw error;

    }
}
const sendSMS = async (order_id, user_id, address) => {
    console.log('\norder.controller.sendSMS triggered -->');
    const { UserMaster, DelboyOrderMapping, Employee, db1Conn } = await getModels();
    console.log("\nORDER_ID :" + order_id);
    console.log("\nUSER_ID :" + user_id);
    console.log("\n\nADDRESS :", JSON.stringify(address, 0, 2));
    /*  {
        user_id: req.user_id
    }
    */
    try {
        let user = await UserMaster.findOne({ where: { user_id: user_id } });
        let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: order_id, delivery_type: 1 } });
        if (user) {
            let customername = user.user_fname + " " + user.user_lname;
            let url1;
            let url;

            if (dboy_order_mapping) {
                let employee = await Employee.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                let empname = employee.emp_fname + " " + employee.emp_lname;
                let emp_phoneno = employee.emp_contact_no;
                if (user.user_phoneno) {
                    console.log("\nUSER PHONE NO :" + user.user_phoneno);
                    url = env.sms.URI + emp_phoneno + '&text=Customer name-' + customername + ', order no ' + order_id + ', phone no-' + user.user_phoneno + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude + '&priority=ndnd&stype=normal';
                }
                else {
                    console.log("\nUSER HAVING NO PHONE NO :");
                    url = env.sms.URI + emp_phoneno + '&text=Customer name-' + customername + ', order no ' + order_id + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude + '&priority=ndnd&stype=normal';
                }
                let resopnse = await axios.get(url);

                if (resopnse.data) {
                    console.log("\nresopnse.data :" + resopnse.data);
                    if (user.user_phoneno) {
                        url1 = env.sms.URI + user.user_phoneno + '&text=Thanks for ordering. Order no ' + order_id + ' will be pick up by our delivery boy ' + empname + ', phone no-' + emp_phoneno + '&priority=ndnd&stype=normal';
                        let resopnse1 = await axios.get(url1);
                        console.log("\nresopnse1.data :" + resopnse1.data);
                    }
                    console.log("\nRETURN  :TRUE");
                    return true;
                }
                else {
                    console.log("\nRETURN  :FALSE");
                    return false;
                }
            }
            else {
                console.log("\nRETURN OUTER IF :FALSE");
                return false;
            }
        }
        else {
            console.log("\nNO USER EXIST HAVING USER-ID----", user_id);
            console.log("\nRETURN OUTER IF :FALSE");
            return false;
        }
    }
    catch (error) {
        console.log('\norder.controller.sendSMS  error', error);
        throw error;

    }
}

const sendSMSOnFailureOrPendingTransaction = async (user_id, order_id, virtual_order_id) => {
    console.log('\norder.controller.sendSMSOnFailureOrPendingTransaction triggered -->');
    const { UserMaster, db1Conn } = await getModels();
    console.log("\nORDER_ID :" + order_id);
    console.log("\nVIRTUAL_ORDER_ID :" + virtual_order_id);
    console.log("\nUSER_ID :" + user_id);
    /*  {
        user_id: req.user_id
    }
    */
    try {
        let user = await UserMaster.findOne({ where: { user_id: user_id } });
        if (user) {
            let url;
            if (user.user_phoneno) {
                url = env.sms.URI + user.user_phoneno + '&text=Thanks for using our app. Your transaction is not success, Please try again or go for COD. ' + '&priority=ndnd&stype=normal';
                let resopnse = await axios.get(url);
                console.log("\nresopnse.data :" + resopnse.data);
                if (resopnse.data) {
                    console.log("\nRETURN  :TRUE");
                    return true;
                }
                else {
                    console.log("\nRETURN  :FALSE");
                    return false;
                }
            }
        }
        else {
            console.log("\nNO USER EXIST HAVING USER-ID----", user_id);
            console.log("\nRETURN OUTER IF :FALSE");
            return false;
        }
    }
    catch (error) {
        console.log('\norder.controller.sendSMS  error', error);
        throw error;

    }
}

//PAYTM  TEST
exports.createOrder3 = async (req, res) => {

    console.log('\ncart.controller.createOrder3  triggered -->');
    const reqBody = req.body;
    // user_id: reqBody.user_id, order_id: reqBody.order_id
    const { user_id, order_id } = reqBody;
    //const mid = "zoTQbG62503921214041";// Staging
    //const mkey = "9k3kwpFqGWOwMNv3";// Staging
    const mid = "bOFZLO71633079149423";// Production
    const mkey = "F8TC2oyAOtRJqKoJ";// Production
    try {

        /* initialize an object */
        var paytmParams = {};

        /* body parameters */
        paytmParams.body = {

            /* for custom checkout value is 'Payment' and for intelligent router is 'UNI_PAY' */
            "requestType": "Payment",

            /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
            "mid": mid,

            /* Find your Website Name in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
            //"websiteName": "WEBSTAGING", //Staging
            "websiteName": "DEFAULT",
            /* Enter your unique order id */
            "orderId": order_id,

            /* on completion of transaction, we will send you the response on this URL */
            "callbackUrl": `https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=${order_id}`,
            //"enablePaymentMode": [{ "mode": "UPI", "channels": ["UPIPUSH"] }],

            /* Order Transaction Amount here */
            "txnAmount": {

                /* Transaction Amount Value */
                "value": "1.00",

                /* Transaction Amount Currency */
                "currency": "INR",
            },

            /* Customer Infomation here */
            "userInfo": {

                /* unique id that belongs to your customer */
                "custId": user_id,
            },
        };

        /**
        * Generate checksum by parameters we have in body
        * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
        */
        checksum_lib.genchecksumbystring(JSON.stringify(paytmParams.body), mkey, function (err, checksum) {

            console.log("checksum ======", checksum);


            /**
            * Verify Checksum
            * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
            */
            var isValidChecksum = checksum_lib.verifychecksumbystring(JSON.stringify(paytmParams.body), mkey, checksum);
            if (isValidChecksum) {
                console.log("Checksum Matched");
            } else {
                console.log("Checksum Mismatched");
            }

            /* head parameters */
            paytmParams.head = {

                /* put generated checksum value here */
                "signature": checksum
            };

            console.log("paytmParams ======", paytmParams);
            /* prepare JSON string for request */
            var post_data = JSON.stringify(paytmParams);
            var options = {

                /* for production */
                hostname: 'securegw.paytm.in',

                /* for Production */
                // hostname: 'securegw.paytm.in',

                port: 443,
                path: `/theia/api/v1/initiateTransaction?mid=${mid}&orderId=${order_id}`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': post_data.length
                }
            };

            // Set up the request
            var response = "";
            var post_req = https.request(options, function (post_res) {
                post_res.on('data', function (chunk) {
                    response += chunk;
                });

                post_res.on('end', function () {
                    console.log('Response: ', response);
                    const txnToken = JSON.parse(response).body.txnToken
                    // res.send(response);

                    res.json({
                        status: 1,
                        message: "paytm txn details ",
                        data: {
                            orderId: order_id,
                            custId: user_id,
                            txnToken: txnToken,
                            txnAmount: "1.00",
                            callbackUrl: `https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=${order_id}`
                        }
                    });
                });
            });

            // post the data
            post_req.write(post_data);
            post_req.end();
        });
    }
    catch (error) {
        console.log(error);
        res.status(404).send(error);
    }

}

//PAYTM  response
exports.orderTxnStatus = async (req, res) => {
    const reqBody = req.body;
    console.log('\norder.controller.orderTxnStatus  triggered -->');
    // user_id: reqBody.user_id, order_id: reqBody.order_id (OLD REQUEST)
    // user_id: reqBody.user_id, order_id: reqBody.virtual_order_id (NEW REQUEST)
    const { OrderPaymentMapping, OrderNotifications, Login, EmployeeLogin, UserMaster, DelboyOrderMapping, Employee, OrderHeader, OrderDetails, OrderHistory, Address, LaundryServiceFlow, CartDetails, CartHeader, ServiceAvailabilityPincode, db1Conn } = await getModels();
    //const { order_id, order_transaction_id, bank_transaction_id, payment_through, bank_name, payment_mode, amount, refund_amount, txn_status, txn_date, txn_response } = reqBody;
    const { virtual_order_id, user_id } = reqBody;
    console.log('orderTxnStatus request ======', reqBody);
    console.log('orderTxnStatus request  VIRTUAL_ORDER_ID======', virtual_order_id);
    console.log('orderTxnStatus request  USER_ID======', user_id);
    var paytmParams = {};
    // Here status_id and module_id are hard coded, it will be dynamic in future
    let laundry_service_flow = await LaundryServiceFlow.findOne({
        where: { status_id: order_creation_id, module_id: 1 }
    });
    paytmParams["MID"] = mid;
    paytmParams["ORDERID"] = virtual_order_id;
    checksum_lib.genchecksum(JSON.stringify(paytmParams), mkey, function (err, checksum) {
        paytmParams["CHECKSUMHASH"] = checksum;
        var post_data = JSON.stringify(paytmParams);

        var options = {
            hostname: 'securegw.paytm.in',
            port: 443,
            path: '/order/status',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': post_data.length
            }
        };
        var response = "";
        var post_req = https.request(options, function (post_res) {
            post_res.on('data', function (chunk) {
                response += chunk;
            });

            post_res.on('end', async function () {
                console.log('Response: ', response);
                const orderHeaders = [];
                const orderStatusResponse = JSON.parse(response);
                let paytm_order_payment_status = "";
                if (orderStatusResponse.STATUS == 'TXN_SUCCESS') {
                    paytm_order_payment_status = "SUCCESS";
                }
                else if (orderStatusResponse.STATUS == 'TXN_FAILURE') {
                    paytm_order_payment_status = "FAILURE";
                }
                else if (orderStatusResponse.STATUS == 'PENDING') {
                    paytm_order_payment_status = "PENDING";
                }
                else {
                    paytm_order_payment_status = "NA";
                }
                console.log('Paytm order status : ', orderStatusResponse.STATUS);
                console.log('paytm_order_payment_status : ', paytm_order_payment_status);
                let order_transaction_id, amount, txn_status, txn_date, txn_response;
                const { TXNID, TXNAMOUNT, TXNDATE } = orderStatusResponse;
                order_transaction_id = TXNID;
                amount = TXNAMOUNT;
                txn_status = paytm_order_payment_status;
                txn_date = TXNDATE;
                txn_response = orderStatusResponse;
                const t = await db1Conn.transaction();
                try {
                    if (paytm_order_payment_status == "SUCCESS") {
                        const orderPaymentMappings = await OrderPaymentMapping.findAll({ where: { virtual_order_id } })
                        await Promise.all(orderPaymentMappings.map(async (orderPaymentMapping) => {
                            const { order_id } = orderPaymentMapping;
                            const orderHeader = await OrderHeader.update(
                                { payment_status: 'PAID', order_flag: 1 },
                                { where: { order_id } }, { transaction: t });
                            await OrderPaymentMapping.update(
                                { order_transaction_id, amount, txn_status, txn_date, txn_response },
                                { where: { order_id, virtual_order_id } }, { transaction: t });

                            // orderHeaders.push(orderHeader);
                            const updatedOrderHeader = await OrderHeader.findOne({ where: { order_id } });
                            orderHeaders.push(updatedOrderHeader);
                        }));
                        t.commit();
                        // send all order related notifications
                        await Promise.all(orderHeaders.map(async (orderHeader) => {
                            let x = 0;
                            var refreshId = setInterval(async function () {
                                x = x + 1;
                                console.log("\n\n TRY TO SEND MESSAGE FOR TIME :" + x);
                                var result = await sendSMS(orderHeader.order_id, orderHeader.user_id, orderHeader.address);
                                var result_1 = await sendSMSDemo(orderHeader.order_id, orderHeader.user_id, orderHeader.address);
                                if (result) {
                                    console.log("\n\n MESSAGE SEND  SUCCESSFULLY TO DELBOY AND CUSTOMER");
                                    console.log("\n\n MESSAGE SEND  SUCCESSFULLY TO ADMIN ", result_1);
                                    clearInterval(refreshId);
                                }
                                if (x === 3) {
                                    console.log("\n\nTRY FOR TRHEE TIMES, MESSAGE NOT SEND  SUCCESSFULLY TO DELBOY AND CUSTOMER");
                                    clearInterval(refreshId);
                                }

                            }, 5000);


                            let empname = "";
                            let emp_phoneno = "";
                            let msg_body1 = "";
                            let login = await Login.findOne({ where: { user_id: orderHeader.user_id } });
                            let user = await UserMaster.findOne({ where: { user_id: orderHeader.user_id } });
                            let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: orderHeader.order_id, delivery_type: 1 } });
                            if (dboy_order_mapping) {
                                let employee = await Employee.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                                empname = employee.emp_fname + " " + employee.emp_lname;
                                emp_phoneno = employee.emp_contact_no;
                            }


                            let employee_login = await EmployeeLogin.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                            const msg_body = 'Thanks for ordering. Order no ' + orderHeader.order_id +
                                ' will be pick up by our delivery boy ' + empname + ', phone no-' + emp_phoneno;

                            let customername = user.user_fname + " " + user.user_lname;
                            let customerphoneno = "";
                            if (user.user_phoneno) {
                                console.log("\nDELIVERYBOY NOTIFICATION USER PHONE NO :" + user.user_phoneno);
                                msg_body1 = 'Customer name-' + customername + ', order no ' + orderHeader.order_id + ', phone no-' + user.user_phoneno + ', address-http://maps.google.com/?q=' + orderHeader.address.latitude + ',' + orderHeader.address.longitude;
                                customerphoneno = user.user_phoneno;
                            }
                            else {
                                console.log("\nDELIVERYBOY NOTIFIACTION USER HAVING NO PHONE NO :");
                                msg_body1 = 'Customer name-' + customername + ', order no ' + orderHeader.order_id + ', address-http://maps.google.com/?q=' + orderHeader.address.latitude + ',' + orderHeader.address.longitude;
                                customerphoneno = "";
                            }


                            const dorderNotifications = await OrderNotifications.create(
                                {
                                    order_id: orderHeader.order_id,
                                    status_id: laundry_service_flow.status_id,
                                    emp_id: dboy_order_mapping.emp_id,
                                    role_id: 2,
                                    msg_title: 'Order has to be receive',
                                    msg_body: msg_body1,
                                    created_at: orderHeader.order_date,
                                    latitude: orderHeader.address.latitude,
                                    longitude: orderHeader.address.longitude
                                });


                            const orderNotifications = await OrderNotifications.create(
                                {
                                    order_id: orderHeader.order_id,
                                    status_id: laundry_service_flow.status_id,
                                    user_id: reqBody.user_id,
                                    msg_title: 'Order Created Successfully',
                                    msg_body: msg_body,
                                    created_at: orderHeader.order_date
                                });


                            const fetchBody = {
                                notification: {
                                    title: "iestre",
                                    text: msg_body,
                                    click_action: "com.purpuligo.laundry.NotificationActivity"
                                },
                                data: {
                                    deliveryBoyName: empname,
                                    orderId: orderHeader.order_id,
                                    delBoyPhoneNo: emp_phoneno
                                },
                                to: login.fcmToken
                            };

                            const fetchBody1 = {
                                notification: {
                                    title: "iestre",
                                    text: msg_body1,
                                    click_action: "com.purpuligo.laundry.NotificationActivity"
                                },
                                data: {
                                    customerName: customername,
                                    orderId: orderHeader.order_id,
                                    customerPhoneNo: customerphoneno
                                },
                                to: employee_login.fcmToken
                            };
                            let result1 = dpushToFirebase(fetchBody1, 3);
                            console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY");

                            let y = 0;
                            var notificationId = setInterval(function () {
                                y = y + 1;
                                console.log("\n\n TRY TO SEND NOTIFICATION TO FIREBASE :" + y);
                                let result = pushToFirebase(fetchBody, 3);
                                if (result) {
                                    console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO CUSTOMER");
                                    //dpushToFirebase(fetchBody1, 3);
                                    // console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY");
                                    clearInterval(notificationId);
                                }
                                // if (y === 1) {
                                //     console.log("\n\nNOTIFICATION NOT SEND  SUCCESSFULLY TO DELBOY CUSTOMER");
                                //     clearInterval(notificationId);
                                // }

                            }, 120000);
                        }));
                        const orderHeaderResponse = await calculateOrderPrices(virtual_order_id);

                        return res.json({ status: 1, message: "order payment successfull", data: orderHeaderResponse })
                    }
                    else if (paytm_order_payment_status == "FAILURE" || paytm_order_payment_status == 'PENDING' || paytm_order_payment_status == "NA") {
                        const orderPaymentMappings = await OrderPaymentMapping.findAll({ where: { virtual_order_id } });
                        await Promise.all(orderPaymentMappings.map(async (orderPaymentMapping) => {
                            const { order_id } = orderPaymentMapping;
                            await OrderHeader.update(
                                { payment_status: 'UNPAID' },
                                { where: { order_id } }, { transaction: t });
                            await OrderPaymentMapping.update(
                                { order_transaction_id, amount, txn_status, txn_date, txn_response },
                                { where: { order_id, virtual_order_id } }, { transaction: t });

                            const updatedOrderHeader = await OrderHeader.findOne({ where: { order_id } });
                            orderHeaders.push(updatedOrderHeader);
                        }));
                        const orderHeaderResponse = await calculateOrderPrices(virtual_order_id);
                        t.commit();
                        // send all order related notifications
                        await Promise.all(orderHeaders.map(async (orderHeader) => {
                            let x = 0;
                            var refreshId = setInterval(async function () {
                                x = x + 1;
                                console.log("\n\n TRY TO SEND MESSAGE FOR TIME :" + x);
                                var result = await sendSMSOnFailureOrPendingTransaction(orderHeader.user_id, orderHeader.order_id, orderHeader.virtual_order_id);
                                if (result) {
                                    console.log("\n\n MESSAGE SEND  SUCCESSFULLY TO CUSTOMER ON FAILURE TRANSACTION");
                                    clearInterval(refreshId);
                                }
                                if (x === 3) {
                                    console.log("\n\nTRY FOR TRHEE TIMES, MESSAGE NOT SEND  SUCCESSFULLY TO CUSTOMER ON FAILURE TRANSACTION");
                                    clearInterval(refreshId);
                                }
                            }, 5000);

                            /* 
                                    let empname = "";
                                    let emp_phoneno = "";
                                    let msg_body1 = "";
                                    let login = await Login.findOne({ where: { user_id: orderHeader.user_id } });
                                    let user = await UserMaster.findOne({ where: { user_id: orderHeader.user_id } });
                                    let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: orderHeader.order_id, delivery_type: 1 } });
                                    if (dboy_order_mapping) {
                                        let employee = await Employee.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                                        empname = employee.emp_fname + " " + employee.emp_lname;
                                        emp_phoneno = employee.emp_contact_no;
                                    }        
        
                                    let employee_login = await EmployeeLogin.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                                    const msg_body = 'Thanks for ordering. Order no ' + orderHeader.order_id +
                                        ' will be pick up by our delivery boy ' + empname + ', phone no-' + emp_phoneno;
        
                                    let customername = user.user_fname + " " + user.user_lname;
                                    let customerphoneno = "";
                                    if (user.user_phoneno) {
                                        console.log("\nDELIVERYBOY NOTIFICATION USER PHONE NO :" + user.user_phoneno);
                                        msg_body1 = 'Customer name-' + customername + ', order no ' + orderHeader.order_id + ', phone no-' + user.user_phoneno + ', address-http://maps.google.com/?q=' + orderHeader.address.latitude + ',' + orderHeader.address.longitude;
                                        customerphoneno = user.user_phoneno;
                                    }
                                    else {
                                        console.log("\nDELIVERYBOY NOTIFIACTION USER HAVING NO PHONE NO :");
                                        msg_body1 = 'Customer name-' + customername + ', order no ' + orderHeader.order_id + ', address-http://maps.google.com/?q=' + orderHeader.address.latitude + ',' + orderHeader.address.longitude;
                                        customerphoneno = "";
                                    }
        
        
                                    const dorderNotifications = await OrderNotifications.create(
                                        {
                                            order_id: orderHeader.order_id,
                                            status_id: laundry_service_flow.status_id,
                                            emp_id: dboy_order_mapping.emp_id,
                                            role_id: 2,
                                            msg_title: 'Order has to be receive',
                                            msg_body: msg_body1,
                                            created_at: order_date_time,
                                            latitude: orderHeader.address.latitude,
                                            longitude: orderHeader.address.longitude
                                        });
                
                                    const orderNotifications = await OrderNotifications.create(
                                        {
                                            order_id: orderHeader.order_id,
                                            status_id: laundry_service_flow.status_id,
                                            user_id: reqBody.user_id,
                                            msg_title: 'Order Created Successfully',
                                            msg_body: msg_body,
                                            created_at: order_date_time
                                        });        
        
                                    const fetchBody = {
                                        notification: {
                                            title: "iestre",
                                            text: msg_body,
                                            click_action: "com.purpuligo.laundry.NotificationActivity"
                                        },
                                        data: {
                                            deliveryBoyName: empname,
                                            orderId: orderHeader.order_id,
                                            delBoyPhoneNo: emp_phoneno
                                        },
                                        to: login.fcmToken
                                    };
        
                                    const fetchBody1 = {
                                        notification: {
                                            title: "iestre",
                                            text: msg_body1,
                                            click_action: "com.purpuligo.laundry.NotificationActivity"
                                        },
                                        data: {
                                            customerName: customername,
                                            orderId: orderHeader.order_id,
                                            customerPhoneNo: customerphoneno
                                        },
                                        to: employee_login.fcmToken
                                    };
                                    let result1 = dpushToFirebase(fetchBody1, 3);
                                    console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY");
        
                                    let y = 0;
                                    var notificationId = setInterval(function () {
                                        y = y + 1;
                                        console.log("\n\n TRY TO SEND NOTIFICATION TO FIREBASE :" + y);
                                        let result = pushToFirebase(fetchBody, 3);
                                        if (result) {
                                            console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO CUSTOMER");
                                            clearInterval(notificationId);
                                        }
                                       
                                    }, 120000);
                                     */
                        }));

                        return res.json({ status: 0, message: "order payment fail", data: orderHeaderResponse });
                    }
                }
                catch (error) {
                    console.log('order.controller.paytmResponse Error ======', error);
                    return res.status(404).send(error);
                }
                //res.send(response);
            });
        });
        // post the data
        post_req.write(post_data);
        post_req.on('error', (e) => {
            console.error(e);
            return res.status(404).send(e);
        });
        post_req.end();
    });
}


async function validateOrderRequests2(orderRequests, createType) {
    console.log('\norder.controller.validateOrderRequests2 triggered -->');
    const errors = [];
    const { CategoryPrices, ServiceAvailabilityPincode, CartHeader, db1Conn } = await getModels();
    const orderHeaderList = orderRequests.order_header_list;

    if (createType == 'create') {
        // Cart-Id validation
        let cartFlag = false;
        let cartErrMsg = "";
        for (let i = 0; i < orderHeaderList.length; i++) {
            const cart_id = orderHeaderList[i].cart_id;
            const user_id = orderHeaderList[i].user_id;
            let cartHeader = await CartHeader.findOne({ where: { cart_id, user_id } });
            if (!cartHeader) {
                cartFlag = false;
                cartErrMsg = `Given user_id :${user_id} and cart_id:${cart_id} does not exist in cart_header table `
                break;
            }
            cartFlag = true;
        }

        if (!cartFlag) {
            console.log(cartFlag);
            errors.push(cartErrMsg);
        }
    }
    // service-availability(pincode) validation
    let pincodeFlag = false;
    let pincodeErrMsg = "";
    for (let i = 0; i < orderHeaderList.length; i++) {
        const pincode = orderHeaderList[i].address.pincode;
        let serviceAvailable = await ServiceAvailabilityPincode.findOne({ where: { pincode } });
        if (!serviceAvailable) {
            pincodeFlag = false;
            pincodeErrMsg = `service not available ${pincode} `
            break;
        }
        pincodeFlag = true;
    }

    if (!pincodeFlag) {
        console.log(pincodeFlag);
        errors.push(pincodeErrMsg);
    }

    // price validation
    const categoryDetails = await CategoryPrices.findAll();

    const categroyMinPrices = {};
    categoryDetails.forEach(categoryDetail => {
        categroyMinPrices[categoryDetail.category_id] = {
            name: categoryDetail.category_name,
            value: categoryDetail.min_price,
        };
    });
    console.log("categroyMinPrices===", categroyMinPrices);
    /* const categoryMinPrices = {
    "1": {
        name: "Ironing",
        value: 99,
    },
    "2": {
        name: "Wash & Ironing",
        value: 199,
    },
    "3": {
        name: "Dry Cleaning",
        value: 399,
    },
    };
    */
    const orderTotalPrice = orderRequests.order_total_price;
    let orderTotalPrice2 = 0;
    let minPriceFlag = false;
    let priceErrMsg = "";
    let orderTotalPriceErrMsg = "";
    for (let i = 0; i < orderHeaderList.length; i++) {
        const categoryId = orderHeaderList[i].order_details[0].category_id;
        orderTotalPrice2 = orderTotalPrice2 + orderHeaderList[i].grand_total_price;
        if (orderHeaderList[i].grand_total_price < categroyMinPrices[categoryId].value) {
            minPriceFlag = false;
            priceErrMsg = `Minimum price of category ${categroyMinPrices[categoryId].name} is Rs. ${categroyMinPrices[categoryId].value}  `
            break;
        }
        minPriceFlag = true;
    }
    if (!minPriceFlag) {
        console.log(priceErrMsg);
        errors.push(priceErrMsg);
    }
    console.log(`\nONLINE Order Grand Total Price ${orderTotalPrice}`);
    console.log(`\nONLINE Order Sub Total Sum Price ${orderTotalPrice2}`);
    /*
        // order total price validation
        if (orderTotalPrice != orderTotalPrice2) {
            console.log(`Order Grand Total Price ${orderTotalPrice}`);
            console.log(`Order  Subtotal Price ${orderTotalPrice2}`);
            orderTotalPriceErrMsg = 'Invalid Order Total Price Value';
            console.log(orderTotalPriceErrMsg);
            errors.push(orderTotalPriceErrMsg);
        }
    */
    return errors;
}

async function validateOrderRequests(orderRequests, createType) {
    console.log('\norder.controller.validateOrderRequests triggered -->');
    const errors = [];
    const { CategoryPrices, ServiceAvailabilityPincode, CartHeader, db1Conn } = await getModels();

    if (createType == 'create') {
        // Cart-Id validation
        let cartFlag = false;
        let cartErrMsg = "";
        for (let i = 0; i < orderRequests.length; i++) {
            const cart_id = orderRequests[i].cart_id;
            const user_id = orderRequests[i].user_id;
            let cartHeader = await CartHeader.findOne({ where: { cart_id, user_id } });
            if (!cartHeader) {
                cartFlag = false;
                cartErrMsg = `Given user_id :${user_id} and cart_id:${cart_id} does not exist in cart_header table `
                break;
            }
            cartFlag = true;
        }

        if (!cartFlag) {
            console.log(cartFlag);
            errors.push(cartErrMsg);
        }
    }
    // service-availability(pincode) validation
    let pincodeFlag = false;
    let pincodeErrMsg = "";
    for (let i = 0; i < orderRequests.length; i++) {
        const pincode = orderRequests[i].address.pincode;
        let serviceAvailable = await ServiceAvailabilityPincode.findOne({ where: { pincode } });
        if (!serviceAvailable) {
            pincodeFlag = false;
            pincodeErrMsg = `service not available ${pincode} `
            break;
        }
        pincodeFlag = true;
    }

    if (!pincodeFlag) {
        console.log(pincodeFlag);
        errors.push(pincodeErrMsg);
    }

    // price validation
    const categoryDetails = await CategoryPrices.findAll();

    const categroyMinPrices = {};
    categoryDetails.forEach(categoryDetail => {
        categroyMinPrices[categoryDetail.category_id] = {
            name: categoryDetail.category_name,
            value: categoryDetail.min_price,
        };
    });
    console.log("categroyMinPrices===", categroyMinPrices);
    /* const categoryMinPrices = {
    "1": {
        name: "Ironing",
        value: 99,
    },
    "2": {
        name: "Wash & Ironing",
        value: 199,
    },
    "3": {
        name: "Dry Cleaning",
        value: 399,
    },
    };
    */
    let minPriceFlag = false;
    let priceErrMsg = "";
    for (let i = 0; i < orderRequests.length; i++) {
        const categoryId = orderRequests[i].order_details[0].category_id;
        // Rs sign ₹
        if (orderRequests[i].grand_total_price < categroyMinPrices[categoryId].value) {
            minPriceFlag = false;
            priceErrMsg = `Minimum price of category ${categroyMinPrices[categoryId].name}  is Rs. ${categroyMinPrices[categoryId].value}`
            break;
        }
        minPriceFlag = true;
    }
    if (!minPriceFlag) {
        console.log(priceErrMsg);
        errors.push(priceErrMsg);
    }

    return errors;

}

async function calculateOrderPrices(virtual_order_id) {
    //exports.calculateOrderPrices = async (req, res) =>{
    console.log('\norder.controller.calculateOrderPrices triggered -->');
    // const { virtual_order_id } = req.body;
    const { OrderHeader, OrderDetails, ProductMaster, PriceMaster, ServiceAvailabilityPincode, CartHeader, db1Conn } = await getModels();
    const orderHeaderRes = {};
    // fetching order list
    const orderHeaderList = await OrderHeader.findAll({
        where: { virtual_order_id },
        include: [
            { model: OrderDetails, as: 'order_details' }
        ]
    });

    // calculating order prices
    let order_total_price = 0;
    let order_total_quantity = 0;
    await Promise.all(orderHeaderList.map(async (orderHeader) => {
        const orderDetailsList = orderHeader.order_details;
        let grand_total_price = 0;
        let grand_total_quantity = 0;
        await Promise.all(orderDetailsList.map(async (element) => {
            // here select product_name from product_master based on prod_id
            const product_master = await ProductMaster.findOne({ where: { prod_id: element.prod_id } });
            if (product_master) {
                console.log("PRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));
                // here select price from price_master based on prod_id
                let price_master = await PriceMaster.findOne({ where: { prod_id: element.prod_id, prod_status: 'CurPrice' } });
                console.log("PRICE MASTER DETAILS======", JSON.stringify(price_master, 0, 2));
                console.log('\nproduct ' + element.prod_id + ' price of ' + price_master.prod_price);
                let quantity = parseInt(element.customer_order_quantity);
                console.log('\nProduct quantity ' + quantity);
                let total_price = parseFloat(price_master.prod_price) * quantity;
                console.log('\nTotal Price ' + total_price);
                element.setDataValue('prod_name', product_master.prod_name);
                element.setDataValue('prod_code', product_master.prod_code);
                element.setDataValue('category_id', product_master.parent_id);
                element.setDataValue('per_unit_price', price_master.prod_price);
                element.setDataValue('total_price', total_price.toFixed(2));
                element.setDataValue('quantity', quantity);
                total_price = parseFloat(total_price.toFixed(2));
                grand_total_price = total_price + grand_total_price;
                grand_total_quantity = grand_total_quantity + quantity;
            }
            else {
                console.log("PRODUCT and PRICE MASTER DETAILS NOT EXIST FOR PRODUCT-ID======", element.prod_id);
            }
        }));
        orderHeader.setDataValue('grand_total_price', grand_total_price);
        orderHeader.setDataValue('grand_total_quantity', grand_total_quantity);
        order_total_price = order_total_price + grand_total_price;
        order_total_quantity = order_total_quantity + grand_total_quantity;
    }));
    order_total_price = parseFloat(order_total_price.toFixed(2));
    orderHeaderRes.order_header_list = orderHeaderList;
    orderHeaderRes.order_total_price = order_total_price.toFixed(2);
    orderHeaderRes.order_total_quantity = order_total_quantity;
    // return res.send(orderHeaderRes);
    return orderHeaderRes;
}
