const fetch = require('node-fetch');
const moment = require('moment');
// global imports
const Sequelize = require("sequelize");
const op = Sequelize.Op;

const env = require('../../../env');
const utcOffsetMins = env.env.utcOffsetMins;
const { getModels } = require('../../models');
// getUserNotifications OLD
exports.getUserNotifications = async (req, res) => {
    console.log('\norder.notifications.controller.getUserNotifications triggered -->');
    const { OrderNotifications, db1Conn } = await getModels();
    console.log(`\n req.body: `, req.body);

    const { user_id } = req.body;
    console.log(`\n User_Id: `, user_id);
    try {

        let userOrderNotifications = await OrderNotifications.findAll({ where: { user_id: user_id, emp_id: 0 }, order: [['order_id', 'DESC']] });
        if (userOrderNotifications) {
            console.log("\n\nUserOrderNotifications======", JSON.stringify(userOrderNotifications, 0, 2));
            res.json({ status: 1, message: "Notification Send Successfully", data: userOrderNotifications })
        }
        else {

            res.json({ status: 1, message: "Notification Unsuccessfully", data: {} });
        }


    }
    catch (error) {
        console.log('\norder.notifications.controller.getUserNotifications Error', error)
        throw error;

    }
}

// getUserNotifications NEW
exports.getUserNotifications1 = async (req, res) => {
    console.log('\norder.notifications.controller.getUserNotifications triggered -->');
    const { OrderNotifications, AppUpdateNotification, db1Conn } = await getModels();
    console.log(`\n req.body: `, req.body);

    const { user_id } = req.body;
    console.log(`\n User_Id: `, user_id);
    let userOrderNotifications = [];
    let newAppUpdateNotification = {};

    //var paytmParams1 = {};
    //paytmParams1["MID"] = mid;

    try {
        let appUpdateNotification = await AppUpdateNotification.findOne({ where: { user_id: user_id }, order: [['id', 'DESC']] });
        let userOrderNotifications_1 = await OrderNotifications.findAll({ where: { user_id: user_id, emp_id: 0 }, order: [['order_id', 'DESC']] });
        //  if (userOrderNotifications_1.length > 0) {
        //     userOrderNotifications = [...userOrderNotifications_1];
        // } 
        if (appUpdateNotification) {
            newAppUpdateNotification["order_notifications_id"] = 0;
            newAppUpdateNotification["order_id"] = 0;
            newAppUpdateNotification["status_id"] = 0;
            newAppUpdateNotification["user_id"] = appUpdateNotification.user_id;
            newAppUpdateNotification["emp_id"] = 0;
            newAppUpdateNotification["role_id"] = 0;
            newAppUpdateNotification["msg_title"] = "app update";
            newAppUpdateNotification["msg_body"] = appUpdateNotification.msg;
            newAppUpdateNotification["created_at"] = appUpdateNotification.created_at;
            newAppUpdateNotification["latitude"] = "0.0";
            newAppUpdateNotification["longitude"] = "0.0";

            userOrderNotifications = userOrderNotifications.concat(newAppUpdateNotification);
        }
        if (userOrderNotifications_1.length > 0) {
            userOrderNotifications = userOrderNotifications.concat(userOrderNotifications_1)
        }
        if (userOrderNotifications.length > 0) {
            console.log("\n\nUserOrderNotifications======", JSON.stringify(userOrderNotifications, 0, 2));
            res.json({ status: 1, message: "Notification Send Successfully", data: userOrderNotifications })
        }
        else {

            res.json({ status: 0, message: "No Notification", data: {} });
        }


    }
    catch (error) {
        console.log('\norder.notifications.controller.getUserNotifications Error', error)
        throw error;

    }
}
// getAppUpdateNotifications
exports.getAppUpdateNotifications = async (req, res) => {
    console.log('\norder.notifications.controller.getAppUpdateNotifications triggered -->');
    const { AppUpdateNotification, db1Conn } = await getModels();
    console.log(`\n req.body: `, req.body);

    const { user_id } = req.body;
    console.log(`\n App Update User_Id: `, user_id);
    try {

        let appUpdateNotification = await AppUpdateNotification.findOne({ where: { user_id: user_id }, order: [['id', 'DESC']] });
        if (appUpdateNotification) {
            console.log("\n\nUser App Update Notifications======", JSON.stringify(appUpdateNotification, 0, 2));
            res.json({ status: 1, message: "App Update Notification Send Successfully", data: appUpdateNotification })
        }
        else {

            res.json({ status: 1, message: "App Update Notification Unsuccessfully", data: {} });
        }


    }
    catch (error) {
        console.log('\norder.notifications.controller.getAppUpdateNotifications Error', error)
        throw error;

    }
}



exports.pushAddhNotificationToCustomer = async (req, res) => {
    console.log('order.notifications.controller.pushAddhNotificationToCustomer call triggered ');
    try {
        console.log('order.notifications.controller.pushAddhNotificationToCustomer try triggered ');
        const { Login, db1Conn } = await getModels();

        console.log(`\n req.body: `, req.body);
        const { msg, image_path } = req.body;
        console.log("\n\nMESSAGE======", msg);
        console.log("\n\nIMAGE-PATH======", image_path);

        const fcmTokens = await Login.findAll();

        await Promise.all(fcmTokens.map(async (element) => {
            if (element.fcmToken) {
                console.log("\n\n FCM TOKEN ======", JSON.stringify(element.fcmToken, 0, 2));
                const fetchBody = {
                    notification: {
                        title: "iestre",
                        text: msg,
                        image: image_path,
                        click_action: "com.purpuligo.laundry.NotificationActivity"
                    },
                    data: {

                    },
                    to: element.fcmToken
                };


                const fetchOptions = {
                    method: 'post',
                    body: JSON.stringify(fetchBody),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'key=AAAA-tdBRfY:APA91bGt3dulilg-Fl9fncZLbDAmsl5jZDg4ggY89L7pYgHX1lgEj191sRl5-BHUoE4FaIdKUszbx82TqGTTWPknHBb69iecnVfmD9kmrSEn8Fp-ATxWXhQuPIhWlSBPSG05nvwrEvoO'
                    },
                };

                let flag = 3, flag2 = 0;
                do {
                    console.log('do triggered ');
                    try {
                        console.log('try triggered ');
                        let res = await fetch('https://fcm.googleapis.com/fcm/send', fetchOptions);
                        let json = await res.json();
                        console.log('Result data ', json);
                        flag = 3;
                        flag2 = 1;
                    } catch (err) {
                        console.log('order.notifications.controller.pushAddhNotificationToCustomer catch block.....')
                        console.log('catch triggered ');
                        console.log('err: ', err)
                        ++flag;
                        flag2 = 0;
                    }
                } while (flag < 3);

                return flag2;

            }

        }));
        res.json({ status: 0, message: "Notifications Sends To Customers", data: {} });
    }
    catch (error) {
        console.log('order.notifications.controller.pushAddhNotificationToCustomer catch triggered ');
        console.log("ERROR==" + error);
        throw error;
    }
}

// getDeliveryBoyrNotifications
exports.getDeliveryBoyNotifications = async (req, res) => {
    console.log('\norder.notifications.controller.getDeliveryBoyrNotifications triggered -->');
    const { OrderNotifications, db1Conn } = await getModels();
    console.log(`\n req.body: `, req.body);

    const { emp_id } = req.body;
    console.log(`\n Delboy_emp_Id: `, emp_id);
    try {

        let dboyOrderNotifications = await OrderNotifications.findAll({ where: { emp_id: emp_id, role_id: 2, user_id: 0 }, order: [['order_id', 'DESC']] });
        if (dboyOrderNotifications) {
            console.log("\n\nDeliveryBoy OrderNotifications======", JSON.stringify(dboyOrderNotifications, 0, 2));
            res.json({ status: 1, message: "Notification Send Successfully", data: dboyOrderNotifications })
        }
        else {

            res.json({ status: 1, message: "Notification Unsuccessfully", data: {} });
        }


    }
    catch (error) {
        console.log('\norder.notifications.controller.getDeliveryBoyrNotifications Error', error)
        throw error;

    }
}


// getLaundryManNotifications
exports.getLaundryManNotifications = async (req, res) => {
    console.log('\norder.notifications.controller.getLaundryManNotifications triggered -->');
    const { OrderNotifications, db1Conn } = await getModels();
    console.log(`\n req.body: `, req.body);

    const { emp_id } = req.body;
    console.log(`\n LaundryMan emp_Id: `, emp_id);
    try {

        let lmanOrderNotifications = await OrderNotifications.findAll({ where: { emp_id: emp_id, role_id: 1, user_id: 0 }, order: [['order_id', 'DESC']] });
        if (lmanOrderNotifications) {
            console.log("\n\nLaundryMan OrderNotifications======", JSON.stringify(lmanOrderNotifications, 0, 2));
            res.json({ status: 1, message: "Notification Send Successfully", data: lmanOrderNotifications })
        }
        else {

            res.json({ status: 1, message: "Notification Unsuccessfully", data: {} });
        }


    }
    catch (error) {
        console.log('\norder.notifications.controller.lmanOrderNotifications Error', error)
        throw error;

    }
}


exports.pushAppUpdateNotificationToCustomer = async (req, res) => {
    console.log('order.notifications.controller.pushAppUpdateNotificationToCustomer call triggered ');
    try {
        console.log('order.notifications.controller.pushAppUpdateNotificationToCustomer try triggered ');
        const { Login, AppVersion, AppUpdateNotification, db1Conn } = await getModels();

        console.log(`\n req.body: `, req.body);
        const { msg, image_path } = req.body;
        console.log("\n\nMESSAGE======", msg);
        console.log("\n\nIMAGE-PATH======", image_path);

        let appDetails = await AppVersion.findOne({ where: { app_version_type: 'Current' } });
        const versionId = appDetails.app_version_id;

        const fcmTokens = await Login.findAll({
            where: {
                app_version_id: {
                    [op.lte]: versionId
                }
            }
        });
        /* 
               const fcmTokens = await Login.findAll({
                   where: {
                       app_version_id: {
                           [op.eq]: 8
                       }
                   }
               });
        */
        await Promise.all(fcmTokens.map(async (element) => {
            if (element.fcmToken) {
                console.log("\n\n FCM TOKEN ======", JSON.stringify(element.fcmToken, 0, 2));
                console.log("\n\n APP UPDATE NOTIFICATION SEND TO CUSTOMER ID ======", JSON.stringify(element.user_id, 0, 2));
                const appupdate_notf_moment = moment().utc().utcOffset(utcOffsetMins);
                let appupdate_notf_moment_date_time = appupdate_notf_moment.format("DD/MM/YYYY hh:mm A");
                console.log("\n\n USER AppUpdateNotification ======", element.user_id);
                await AppUpdateNotification.destroy({ where: { user_id: element.user_id } });
                const res = await AppUpdateNotification.create({
                    user_id: element.user_id, msg: msg,
                    created_at: appupdate_notf_moment_date_time
                });
                console.log("\n\n INSERTED FOR USER AppUpdateNotification ======", res);
                const fetchBody = {
                    notification: {
                        title: "iestre",
                        text: msg,
                        image: image_path,
                        click_action: "com.purpuligo.laundry.NotificationActivity"
                    },
                    data: {

                    },
                    to: element.fcmToken
                };


                const fetchOptions = {
                    method: 'post',
                    body: JSON.stringify(fetchBody),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'key=AAAA-tdBRfY:APA91bGt3dulilg-Fl9fncZLbDAmsl5jZDg4ggY89L7pYgHX1lgEj191sRl5-BHUoE4FaIdKUszbx82TqGTTWPknHBb69iecnVfmD9kmrSEn8Fp-ATxWXhQuPIhWlSBPSG05nvwrEvoO'
                    },
                };
                let flag = 3;
                do {
                    console.log('do triggered ');
                    try {
                        console.log('try triggered ');
                        let res = await fetch('https://fcm.googleapis.com/fcm/send', fetchOptions);
                        let json = await res.json();
                        console.log('Result data ', json);
                        flag = 3;
                    }
                    catch (err) {
                        console.log('order.notifications.controller.pushAppUpdateNotificationToCustomer catch block.....')
                        console.log('catch triggered ');
                        console.log('err: ', err)
                        ++flag;
                    }
                } while (flag < 3);

            }

        }));
        res.json({ status: 0, message: "App Update Notifications Sends To Customers", data: {} });
    }
    catch (error) {
        console.log('order.notifications.controller.pushAppUpdateNotificationToCustomer catch triggered ');
        console.log("ERROR==" + error);
        throw error;
    }
}

