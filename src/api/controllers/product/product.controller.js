const sequelize = require("sequelize");
const Op = sequelize.Op;
// local imports
const { getModels } = require('../../models');
const env = require('../../../env');
const img_url = env.img.img_url;

// This method retrive only top level product category like Ironing,Dry Cleaning, Wash and Ironing
exports.findCategory = async (req, res) => {
    console.log('\nproduct.controller.findCategory  triggered -->');
    const { ProductMaster } = await getModels();

    ProductMaster.findAll({
        where: { parent_id: 0 }
    }).then(products => {
        res.json(products);
    })
        .catch(error => {
            console.log(error);
            res.status(404).send(error);
        })
}

exports.findCategory1 = async (req, res) => {
    console.log('\nproduct.controller.findCategory1  triggered -->');
    const { ProductMaster } = await getModels();

    ProductMaster.findAll({
        where: {
            [Op.or]: [
                { parent_id: 0 },
                { parent_id: 10000 }
            ]
        }
    }).then(products => {
        res.json(products);
    })
        .catch(error => {
            console.log(error);
            res.status(404).send(error);
        })
}

// find products by parent category_id 
exports.findCategoryProducts = async (req, res) => {
    try {
        const { ProductMaster, PriceMaster, ProductImage } = await getModels();
        console.log('\nproduct.controller.findCategoryProducts  triggered -->');
        const product_details = [];
        const categories = await ProductMaster.findAll({ where: { parent_id: req.params.id } });
        await Promise.all(categories.map(async (category) => {
            const products = await ProductMaster.findAll({
                where: { parent_id: category.prod_category_id },
                include: [
                    { model: PriceMaster, as: 'price_details' },
                    { model: ProductImage, as: 'image_details' },
                ]
            });
            /*  const productsWithCategory = {
                 category_id: category.prod_category_id,
                 category_name: category.prod_name,
                 products
             }; */


            await Promise.all(products.map(async (product) => {
                product.image_details.setDataValue('prod_image_path', `${img_url}${product.image_details.prod_image_path}`);

            }));

            category.setDataValue('products', products);
            product_details.push(category);
        }));
        res.json({ status: 1, message: "list of products with categories ", data: product_details });
    }
    catch (error) {
        console.log(error);
        res.status(404).json({ status: 0, message: "unable to fetch products, try again " });
    }
}

exports.findAllProducts = async (req, res) => {
    const { ProductMaster, PriceMaster, ProductImage } = await getModels();
    console.log('\nproduct.controller.findAllProducts  triggered -->');
    ProductMaster.findAll({
        include: [
            { model: PriceMaster, as: 'price_details' },
            { model: ProductImage, as: 'image_details' },
        ]
    })
        .then(products => {
            res.json(products);
        })
        .catch(error => {
            console.log(error);
            res.status(404).send(error);
        })
}

exports.findProduct = async (req, res) => {
    console.log('\nproduct.controller.findProduct  triggered -->');
    const { ProductMaster, PriceMaster, ProductImage } = await getModels();

    ProductMaster.findOne({
        where: { prod_id: req.params.id },
        include: [
            { model: PriceMaster, as: 'price_details' },
            { model: ProductImage, as: 'image_details' },
        ]
    }).then(product => {
        res.json(product);
    })
        .catch(error => {
            console.log(error);
            res.status(404).send(error);
        })
}




/*
exports.findProductsByCategory = async (req, res) => {
    console.log('\nproduct.controller.findProductsByCategory  triggered -->');
    const { ProductMaster, PriceMaster, ProductImage } = await getModels();

    ProductMaster.findAll({
        where: { prod_category_id: req.params.id },
        include: [
            { model: PriceMaster, as: 'price_details' },
            { model: ProductImage, as: 'image_details' },
        ],
        order: [[{ PriceMaster, as: 'price_details' }, 'prod_price', 'ASC']]
    }).then(product => {
        //res.json(product);
        res.json({ status: 1, message: "products by category", data: product })
    })
        .catch(error => {
            console.log(error);
            // res.status(404).send(error);
            res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        })
}
*/

exports.findProductsByCategory = async (req, res) => {
    console.log('\nproduct.controller.findProductsByCategory  triggered -->');
    const { ProductMaster, PriceMaster, ProductImage } = await getModels();

    ProductMaster.findAll({
        where: { prod_category_id: req.params.id },
        include: [
            { model: PriceMaster, as: 'price_details' },
            { model: ProductImage, as: 'image_details' },
        ]
        //,order: [[{ PriceMaster, as: 'price_details' }, 'prod_price', 'ASC']]
    }).then(product => {
        //res.json(product);
        res.json({ status: 1, message: "products by category", data: product })
    })
        .catch(error => {
            console.log(error);
            // res.status(404).send(error);
            res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        })
}


// find sliders images 
exports.findSliders = async (req, res) => {
    try {
        const { SliderImage } = await getModels();
        console.log('\nproduct.controller.findSliders  triggered -->');
        const product_details = [];
        const sliderImages = await SliderImage.findAll();
        if (sliderImages.length > 0) {
            res.json({ status: 1, message: "list of slider images ", data: sliderImages });
        }
        else {
            res.json({ status: 0, message: "empty list of slider images ", data: [] });
        }
    }
    catch (error) {
        console.log(error);
        res.status(404).json({ status: 0, message: "unable to fetch products, try again " });
    }
}