
//xdYhOe66465083624026
//qTxtsAyC!I!KN57Y

// Import the top-level function of express
const express = require('express');
const https = require('https');
const checksum_lib = require('../lib/paytm/checksum');
// Creates an Express application using the top-level function
const app = express();
// Define port number as 3000
const port = 8000;
const orderId = "5558";
const mid = "bOFZLO71633079149423";
// Routes HTTP GET requests to the specified path "/" with the specified callback function
app.get('/', function (req, res) {
    // res.send('Hello, World!\n');
    console.log("url - get / =====");

    /* initialize an object */
    var paytmParams = {};

    /* body parameters */
    paytmParams.body = {

        /* for custom checkout value is 'Payment' and for intelligent router is 'UNI_PAY' */
        "requestType": "Payment",

        /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
        "mid": mid,

        /* Find your Website Name in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
        "websiteName": "DEFAULT",

        /* Enter your unique order id */
        "orderId": orderId,

        /* on completion of transaction, we will send you the response on this URL */
        "callbackUrl": `http://localhost:8000/paytm/callback?orderId=${orderId}`,
        //"enablePaymentMode": [{ "mode": "UPI", "channels": ["UPIPUSH"] }],

        /* Order Transaction Amount here */
        "txnAmount": {

            /* Transaction Amount Value */
            "value": "1.00",

            /* Transaction Amount Currency */
            "currency": "INR",
        },

        /* Customer Infomation here */
        "userInfo": {

            /* unique id that belongs to your customer */
            "custId": "2001",
        },
        "enablePaymentMode": [{ "mode": "UPI" }, { "mode": "NET_BANKING" }, { "mode": "DEBIT_CARD" }]
    };

    /**
    * Generate checksum by parameters we have in body
    * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
    */
    checksum_lib.genchecksumbystring(JSON.stringify(paytmParams.body), "F8TC2oyAOtRJqKoJ", function (err, checksum) {

        console.log("checksum ======", checksum);


        /**
        * Verify Checksum
        * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
        */
        var isValidChecksum = checksum_lib.verifychecksumbystring(JSON.stringify(paytmParams.body), "F8TC2oyAOtRJqKoJ", checksum);
        if (isValidChecksum) {
            console.log("Checksum Matched");
        } else {
            console.log("Checksum Mismatched");
        }

        /* head parameters */
        paytmParams.head = {

            /* put generated checksum value here */
            "signature": checksum
        };

        console.log("paytmParams ======", paytmParams);
        /* prepare JSON string for request */
        var post_data = JSON.stringify(paytmParams);

        var options = {

            /* for Staging */
            hostname: 'securegw.paytm.in',

            /* for Production */
            // hostname: 'securegw.paytm.in',

            port: 443,
            path: `/theia/api/v1/initiateTransaction?mid=${mid}&orderId=${orderId}`,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': post_data.length
            }
        };

        // Set up the request
        var response = "";
        var post_req = https.request(options, function (post_res) {
            post_res.on('data', function (chunk) {
                response += chunk;
            });

            post_res.on('end', function () {
                console.log('Response: ', response);
                const txnToken = JSON.parse(response).body.txnToken
                // res.send(response);
                /* Prepare HTML Form and Submit to Paytm */
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(`<html>
   <head>
      <title>Show Payment Page</title>
   </head>
   <body>
      <center>
         <h1>Please do not refresh this page...</h1>
      </center>
      <form method="post" action="https://securegw.paytm.in/theia/api/v1/showPaymentPage?mid=${mid}&orderId=${orderId}" name="paytm">
         <table border="1">
            <tbody>
               <input type="hidden" name="mid" value="${mid}">
               <input type="hidden" name="orderId" value="${orderId}">
               <input type="hidden" name="txnToken" value="${txnToken}">
            </tbody>
         </table>
         <script type="text/javascript"> document.paytm.submit(); </script>
      </form>
   </body>
</html>`);
                res.end();
            });
        });

        // post the data
        post_req.write(post_data);
        post_req.end();
    });

});

app.post('/paytm/callback', function (req, res) {
    console.log("url - post /paytm/callback =====");
    console.log(req.body);
    console.log("OrderId =====", req.query);


    /* initialize an object */
    var paytmParams = {};

    /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
    paytmParams["MID"] = mid;

    /* Enter your order id which needs to be check status for */
    paytmParams["ORDERID"] = req.query.orderId;

    /**
    * Generate checksum by parameters we have
    * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
    */
    checksum_lib.genchecksum(paytmParams, "F8TC2oyAOtRJqKoJ", function (err, checksum) {

        /* put generated checksum value here */
        paytmParams["CHECKSUMHASH"] = checksum;

        /* prepare JSON string for request */
        var post_data = JSON.stringify(paytmParams);

        var options = {

            /* for Staging */
            hostname: 'securegw.paytm.in',

            /* for Production */
            // hostname: 'securegw.paytm.in',

            port: 443,
            path: '/order/status',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': post_data.length
            }
        };

        // Set up the request
        var response = "";
        var post_req = https.request(options, function (post_res) {
            post_res.on('data', function (chunk) {
                response += chunk;
            });

            post_res.on('end', function () {
                console.log('Response: ', response);
                res.send(response);
            });
        });

        // post the data
        post_req.write(post_data);
        post_req.end();
    });





});
// Make the app listen on port 3000
app.listen(port, function () {
    console.log('Server listening on http://localhost:' + port);
});