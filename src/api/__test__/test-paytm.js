checksum_lib.genchecksumbystring(JSON.stringify(paytmParams.body), "YOUR_KEY_HERE", function (err, checksum) {

    /* head parameters */
    paytmParams.head = {

        /* put generated checksum value here */
        "signature": checksum
    };

    /* prepare JSON string for request */
    var post_data = JSON.stringify(paytmParams);

    var options = {

        /* for Staging */
        hostname: 'securegw-stage.paytm.in',

        /* for Production */
        // hostname: 'securegw.paytm.in',

        port: 443,
        path: '/theia/api/v1/initiateTransaction?mid=YOUR_MID_HERE&orderId=YOUR_ORDER_ID',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': post_data.length
        }
    };

    // Set up the request
    var response = "";
    var post_req = https.request(options, function (post_res) {
        post_res.on('data', function (chunk) {
            response += chunk;
        });

        post_res.on('end', function () {
            console.log('Response: ', response);
        });
    });

    // post the data
    post_req.write(post_data);
    post_req.end();
});