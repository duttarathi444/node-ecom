'use strict';

const moment = require('moment');
const momentTz = require('moment-timezone');

/******** Time Format and Value ********/
const formats = {
    openingHour: 'HH:mm A',
    closingHour: 'HH:mm A',
};
const values = {
    openingHour: '09:30 AM',
    closingHour: '08:30 PM',
};

/******** Local Time Zone ********/
const localTimeZoneOffset = moment().format('Z');
const localTimeZone = moment.tz.guess(true);
console.log('\nlocalTimeZoneOffset: ', localTimeZoneOffset);
console.log('localTimeZone: ', localTimeZone);

const openingMomentLocal = moment(values.openingHour, formats.openingHour, true);
const closingMomentLocal = moment(values.closingHour, formats.closingHour, true);
const compareLocal = closingMomentLocal.isAfter(openingMomentLocal);

console.log('\nopeningMomentLocal: ', openingMomentLocal);
console.log('closingMomentLocal: ', closingMomentLocal);
console.log('closingMomentLocal.isAfter(openingMomentLocal): ', compareLocal);

/******** UTC Time Zone ********/
const openingMomentUTC = moment.utc(values.openingHour, formats.openingHour, true);
console.log('\nopeningMomentUTC: ', openingMomentUTC);

/******** Custom Time Zone ********/
/* Using Moment */
const customTimeZoneOffset = 330; // default: 0, NaN

const customTimeZone = 'Asia/Kolkata';
console.log('\ncustomTimeZoneOffset: ', customTimeZoneOffset);
console.log('customTimeZone: ', customTimeZone);

const openingMomentCustom = moment
    .utc(values.openingHour, formats.openingHour, true)
    .utcOffset(customTimeZoneOffset)
    .subtract(customTimeZoneOffset, 'minutes');
console.log('\nopeningMomentCustom: ', openingMomentCustom);
/* Using Moment TimeZone */
const openingMomentTzCustom = moment.tz(values.openingHour, formats.openingHour, true, customTimeZone);
console.log('\nopeningMomentTzCustom: ', openingMomentTzCustom);

/******** Current Time ********/
const currentMoment = moment().add(120, 'minutes');
const currentMoment2 = moment().utc()
    .utcOffset(customTimeZoneOffset)
    .add(120, 'minutes');

console.log('\ncurrentMoment: ', currentMoment);
console.log('currentMoment2: ', currentMoment2);

/******** Current Day ********/
// const today = moment().format('dddd');
const today = moment.utc()
    .utcOffset(customTimeZoneOffset)
    .format('dddd');
console.log('\ntoday: ', today);