const fetch = require('node-fetch');

exports.pushToFirebase = async (fcmToken) => {
    console.log('pushToFirebase call triggered ');
    const msg_body = 'Thanks for registering in i-estre';
    const fetchBody = {
        notification: {
            title: "iestre",
            text: msg_body,
            img_url: "https://iestre.s3.ap-south-1.amazonaws.com/iestre.png",
            image: "https://iestre.s3.ap-south-1.amazonaws.com/iestre.png",
            url: "https://iestre.s3.ap-south-1.amazonaws.com/iestre.png",
            click_action: "com.purpuligo.laundry.NotificationActivity"
        },
        data: {

        },
        to: fcmToken
    };

    const fetchOptions = {
        method: 'post',
        body: JSON.stringify(fetchBody),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'key=AAAA-tdBRfY:APA91bGt3dulilg-Fl9fncZLbDAmsl5jZDg4ggY89L7pYgHX1lgEj191sRl5-BHUoE4FaIdKUszbx82TqGTTWPknHBb69iecnVfmD9kmrSEn8Fp-ATxWXhQuPIhWlSBPSG05nvwrEvoO'
        },
    };

    let flag = 3, flag2 = 0;
    do {
        console.log('do triggered ');
        try {
            console.log('try triggered ');
            let res = await fetch('https://fcm.googleapis.com/fcm/send', fetchOptions);
            let json = await res.json();
            console.log('data ', json);
            flag = 3;
            flag2 = 1;
        } catch (err) {
            console.log('catch triggered ');
            console.log('err: ', err)
            ++flag;
            flag2 = 0;
        }
    } while (flag < 3);

    return flag2;
}

