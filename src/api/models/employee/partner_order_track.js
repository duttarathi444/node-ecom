const Sequelize = require("sequelize");

const partnerOrderTrackSchema = {
    schema: {
        // attributes
        order_track_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        order_id: {
            type: Sequelize.INTEGER
        },
        partner_id: {
            type: Sequelize.INTEGER
        },
        partner_type: {
            type: Sequelize.INTEGER
        },
        delivery_type: {
            type: Sequelize.INTEGER
        },
        date: {
            type: Sequelize.STRING
        },
        time: {
            type: Sequelize.STRING
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = partnerOrderTrackSchema;