const Sequelize = require("sequelize");

const employeeLoginSchema = {
    schema: {
        // attributes
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        emp_id: {
            type: Sequelize.INTEGER
        },
        emp_login_id: {
            type: Sequelize.STRING,
            isAlphanumeric: true,
            len: [3, 10]
        },
        emp_password: {
            type: Sequelize.STRING
        },
        access_token: {
            type: Sequelize.STRING
        },
        emp_last_login_date_time: {
            type: Sequelize.STRING
        },
        emp_last_logout_date_time: {
            type: Sequelize.STRING
        },
        emp_role_id: {
            type: Sequelize.INTEGER
        },
        fcmToken: {
            type: Sequelize.STRING
        }

    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = employeeLoginSchema;