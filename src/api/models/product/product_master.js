const Sequelize = require("sequelize");

const productMasterSchema = {
    schema: {
        // attributes
        prod_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        prod_code: {
            type: Sequelize.STRING,
            isAlphanumeric: true,
            required: true,
            allowNull: false,
            len: [3, 10]
        },
        prod_name: {
            type: Sequelize.STRING,
            required: true,
            allowNull: false,
            len: [3, 30]
        },
        prod_desc: {
            type: Sequelize.STRING,
            required: true,
            allowNull: false,
            len: [3, 20]
        },
        prod_category_id: {
            type: Sequelize.INTEGER
        },
        parent_id: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = productMasterSchema;