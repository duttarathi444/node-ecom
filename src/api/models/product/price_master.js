const Sequelize = require("sequelize");

const priceMasterSchema = {
    schema: {
        // attributes
        price_master_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        prod_price: {
            type: Sequelize.DECIMAL,
            required: true
        },
        prod_status: {
            type: Sequelize.STRING,
            required: true,
            allowNull: false
        },
        prod_id: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = priceMasterSchema;