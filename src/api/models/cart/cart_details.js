const Sequelize = require("sequelize");

const cartDetailsSchema = {
    schema: {
        // attributes
        cart_details_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        cart_id: {
            type: Sequelize.INTEGER,
            required: true
        },
        prod_id: {
            type: Sequelize.INTEGER
        },
        quantity: {
            type: Sequelize.INTEGER
        },
        category_id: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = cartDetailsSchema;