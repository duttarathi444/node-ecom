const Sequelize = require("sequelize");

const orderPaymentMappingSchema = {
    schema: {
        // attributes
        order_payment_mapping_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        virtual_order_id: {
            type: Sequelize.STRING
        },
        order_id: {
            type: Sequelize.INTEGER
        },
        order_transaction_id: {
            type: Sequelize.STRING
        },
        amount: {
            type: Sequelize.DECIMAL
        },
        txn_status: {
            type: Sequelize.STRING
        },
        txn_date: {
            type: Sequelize.STRING
        },
        txn_response: {
            type: Sequelize.JSON
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = orderPaymentMappingSchema;