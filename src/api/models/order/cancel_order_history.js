const Sequelize = require("sequelize");

const cancelOrderHistorySchema = {
    schema: {
        // attributes
        cancel_order_hitory_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        cancel_user_id: {
            type: Sequelize.INTEGER
        }
        ,
        cancel_order_id: {
            type: Sequelize.INTEGER
        },
        cancel_order_details: {
            type: Sequelize.JSON
        },
        cancel_order_date: {
            type: Sequelize.STRING
        },
        virtual_order_id: {
            type: Sequelize.STRING
        }

    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = cancelOrderHistorySchema;