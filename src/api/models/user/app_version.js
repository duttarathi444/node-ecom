const Sequelize = require("sequelize");

const appVersionSchema = {
    schema: {
        // attributes
        app_version_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        app_version_name: {
            type: Sequelize.STRING
        },
        app_version_code: {
            type: Sequelize.STRING
        },
        app_version_type: {
            type: Sequelize.STRING
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = appVersionSchema;