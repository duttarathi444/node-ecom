const Sequelize = require("sequelize");

const appUpdateNotificationsSchema = {
    schema: {
        // attributes
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: Sequelize.INTEGER
        },
        msg: {
            type: Sequelize.STRING
        },
        created_at: {
            type: Sequelize.STRING
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = appUpdateNotificationsSchema;