const Sequelize = require("sequelize");

const serviceAvailabilityPincodeSchema = {
    schema: {
        // attributes
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        pincode: {
            type: Sequelize.STRING
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = serviceAvailabilityPincodeSchema;