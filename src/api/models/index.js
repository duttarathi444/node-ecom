'use strict';

// local imports
const { createConnections, SequelizeOp } = require("../datasources").MysqlDataSource;

const categoryPricesSchema = require('./product/category_prices');
const productMasterSchema = require('./product/product_master');
const priceMasterSchema = require('./product/price_master');
const productImageSchema = require('./product/product_image');
const sliderImageSchema = require('./product/slider_images');

const cartHeaderSchema = require('./cart/cart_header');
const cartDetailsSchema = require('./cart/cart_details');


const orderHeaderSchema = require('./order/order_header');
const orderDetailsSchema = require('./order/order_details');
const orderHistorySchema = require('./order/order_history');
const cancelOrderHistorySchema = require('./order/cancel_order_history');
const orderNotificationsSchema = require('./order/order_notifications');
const orderPaymentMappingSchema = require('./order/order_payment_mapping');
const addressSchema = require('./address/address');
const laundryServiceFlowSchema = require('./master_data/laundry_service_flow');
const customerMasterSchema = require('./user/customer_master');
const loginSchema = require('./user/login');
const phonenoOtpSchema = require('./user/phoneno_otp');
const employeeLoginSchema = require('./employee/employee_login');
const laundrymanOrderMappingSchema = require('./employee/laundryman_order_mapping');
const delboyOrderMappingSchema = require('./employee/delboy_order_mapping');
const serviceAvailabilityPincodeSchema = require('./user/service_availability_pincode');
const employeeRoleMasterSchema = require('./employee/employee_role_master');
const employeeSchema = require('./employee/employee');
const employeeWorkLocationSchema = require('./employee/employee_work_location');
const partnerOrderTrackSchema = require('./employee/partner_order_track');

const misUserSchema = require('./employee/mis_user');
const misUserLoginSchema = require('./employee/mis_user_login');
const appVersionSchema = require('./user/app_version');
const appUpdateNotificationsSchema = require('./user/app_update_notifications');
let dbModels;
let dbRef;
/**
 * helper functions
 */
async function createDatabases() {
    console.log(`\nIndexModel.createDatabases triggered`);
    // create all Database Connections
    const dbs = await createConnections(['db1']);
    const { db1 } = dbs;

    // slider image model
    const SliderImage = db1.define('slider_images', sliderImageSchema.schema, sliderImageSchema.options);


    // app_update_notifications model
    const AppUpdateNotification = db1.define('app_update_notifications', appUpdateNotificationsSchema.schema, appUpdateNotificationsSchema.options);


    // app_version model
    const AppVersion = db1.define('app_version', appVersionSchema.schema, appVersionSchema.options);

    // category_price model
    const CategoryPrices = db1.define('category_prices', categoryPricesSchema.schema, categoryPricesSchema.options);

    // order_payment_mapping model
    const OrderPaymentMapping = db1.define('order_payment_mapping', orderPaymentMappingSchema.schema, orderPaymentMappingSchema.options);

    // mis_user model
    const MisUser = db1.define('mis_user', misUserSchema.schema, misUserSchema.options);

    // mis_user_login model
    const MisUserLogin = db1.define('mis_user_login', misUserLoginSchema.schema, misUserLoginSchema.options);

    // order_notifications model
    const OrderNotifications = db1.define('order_notifications', orderNotificationsSchema.schema, orderNotificationsSchema.options);

    // partner_order_track model
    const PartnerOrderTrack = db1.define('partner_order_track', partnerOrderTrackSchema.schema, partnerOrderTrackSchema.options);

    //employee model
    const Employee = db1.define('employee', employeeSchema.schema, employeeSchema.options);

    //employee work location model
    const EmployeeWorkLocation = db1.define('employee_work_location', employeeWorkLocationSchema.schema, employeeWorkLocationSchema.options);


    //employee role master model
    const EmployeeRoleMaster = db1.define('employee_role_master', employeeRoleMasterSchema.schema, employeeRoleMasterSchema.options);


    //service availability pincode model
    const ServiceAvailabilityPincode = db1.define('service_availability_pincode', serviceAvailabilityPincodeSchema.schema, serviceAvailabilityPincodeSchema.options);

    //laundryman-order mapping model
    const LaundrymanOrderMapping = db1.define('laundryman_order_mapping', laundrymanOrderMappingSchema.schema, laundrymanOrderMappingSchema.options);
    //delboy-order mapping model
    const DelboyOrderMapping = db1.define('`delboy_order_mapping', delboyOrderMappingSchema.schema, delboyOrderMappingSchema.options);


    // employee-login model
    const EmployeeLogin = db1.define('employee_login', employeeLoginSchema.schema, employeeLoginSchema.options);



    //user-login related models
    const CustomerMaster = db1.define('customer_master', customerMasterSchema.schema, customerMasterSchema.options);
    const Login = db1.define('login', loginSchema.schema, loginSchema.options);

    const PhonenoOtp = db1.define('phoneno_otp', phonenoOtpSchema.schema, phonenoOtpSchema.options);

    // product models
    const ProductMaster = db1.define('product_master', productMasterSchema.schema, productMasterSchema.options);
    const PriceMaster = db1.define('price_master', priceMasterSchema.schema, priceMasterSchema.options);
    const ProductImage = db1.define('product_image', productImageSchema.schema, productImageSchema.options);
    //cart models
    const CartHeader = db1.define('cart_header', cartHeaderSchema.schema, cartHeaderSchema.options);
    const CartDetails = db1.define('cart_details', cartDetailsSchema.schema, cartDetailsSchema.options);


    //order models
    const OrderHeader = db1.define('order_header', orderHeaderSchema.schema, orderHeaderSchema.options);
    const OrderDetails = db1.define('order_details', orderDetailsSchema.schema, orderDetailsSchema.options);
    const OrderHistory = db1.define('order_history', orderHistorySchema.schema, orderHistorySchema.options);
    const CancelOrderHistory = db1.define('cancel_order_history', cancelOrderHistorySchema.schema, cancelOrderHistorySchema.options);

    // address models
    const Address = db1.define('address', addressSchema.schema, addressSchema.options);

    //laundry_service_flow
    const LaundryServiceFlow = db1.define('laundry_service_flow', laundryServiceFlowSchema.schema, laundryServiceFlowSchema.options)

    ProductMaster.hasOne(PriceMaster, { as: 'price_details', foreignKey: 'prod_id' }); //puts foreignKey prod_id in price_master table
    ProductMaster.hasOne(ProductImage, { as: 'image_details', foreignKey: 'prod_id' }); //puts foreignKey prod_id in product_image table
    CartHeader.hasMany(CartDetails, { as: 'cart_details', foreignKey: 'cart_id' }); //puts foreignKey cart_id in cart_details table

    OrderHeader.hasMany(OrderDetails, { as: 'order_details', foreignKey: 'order_id' }); //puts foreignKey order_id in order_details table
    OrderHeader.hasMany(OrderHistory, { as: 'order_history', foreignKey: 'order_id' }); //puts foreignKey orde_id in order_history table
    OrderHistory.belongsTo(OrderHeader, { as: 'order_header', foreignKey: 'order_id' });

    Employee.hasOne(EmployeeWorkLocation, { as: 'work_location', foreignKey: 'emp_id' }); //puts foreignKey emp_id in employee_work_location table


    dbModels = {
        db1Conn: db1,
        CustomerMaster,
        Login,
        PhonenoOtp,
        ProductMaster,
        PriceMaster,
        ProductImage,
        CartHeader,
        CartDetails,
        OrderHeader,
        OrderDetails,
        OrderHistory,
        Address,
        LaundryServiceFlow,
        CancelOrderHistory,
        EmployeeLogin,
        LaundrymanOrderMapping,
        ServiceAvailabilityPincode,
        EmployeeRoleMaster,
        DelboyOrderMapping,
        Employee,
        EmployeeWorkLocation,
        PartnerOrderTrack,
        OrderNotifications,
        MisUser,
        MisUserLogin,
        OrderPaymentMapping,
        CategoryPrices,
        AppVersion,
        AppUpdateNotification,
        SliderImage
    };

    return dbModels;
}

/**
 * Exports
 */
exports.getModels = () => {
    // console.log(`\nIndexModel.getModels triggered`);
    if (dbModels) {
        // console.log(`\ndbModels exist. Returning...`);
        return Promise.resolve(dbModels);
    } else {
        return createDatabases();
    }
}

exports.SequelizeOp = SequelizeOp;