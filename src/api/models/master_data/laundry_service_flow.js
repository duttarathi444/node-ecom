const Sequelize = require("sequelize");

const laundryServiceFlowSchema = {
    schema: {
        // attributes
        laundry_service_flow_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        module_id: {
            type: Sequelize.INTEGER
        },
        status_id: {
            type: Sequelize.INTEGER
        },
        status_desc: {
            type: Sequelize.STRING
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = laundryServiceFlowSchema;