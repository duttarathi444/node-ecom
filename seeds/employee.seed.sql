insert into employee values(100,'Raj','kumar','raj@gmail.com',25,'Male',1,'Graduate','VOTERID','V10001','PAN','P1001','9937123456','Bank_Details-1','1','active');
insert into employee values(101,'Haris','kumar','hari@gmail.com',27,'Male',1,'Graduate','VOTERID','V10002','PAN','P1002','9937123466','Bank_Details-2','1','active');
insert into employee values(102,'Rabi','kumar','rabi@gmail.com',24,'Male',1,'Graduate','VOTERID','V10003','PAN','P1003','9937123477','Bank_Details-3','1','active');
insert into employee values(103,'Kiran','kumar','kiran@gmail.com',22,'Male',1,'Graduate','VOTERID','V10004','PAN','P1004','9937123000','Bank_Details-4','2','active');






insert into service_availability_pincode values(1,'440001');
insert into service_availability_pincode values(2,'440002');
insert into service_availability_pincode values(3,'440003');
insert into service_availability_pincode values(4,'440004');
insert into service_availability_pincode values(5,'440005');
insert into service_availability_pincode values(6,'440006');
insert into service_availability_pincode values(7,'440007');
insert into service_availability_pincode values(8,'440008');
insert into service_availability_pincode values(9,'440009');
insert into service_availability_pincode values(10,'440010');
insert into service_availability_pincode values(11,'440011');
insert into service_availability_pincode values(12,'440012');
insert into service_availability_pincode values(13,'440013');
insert into service_availability_pincode values(14,'440014');
insert into service_availability_pincode values(15,'440015');
insert into service_availability_pincode values(16,'440016');
insert into service_availability_pincode values(17,'440017');
insert into service_availability_pincode values(18,'440018');
insert into service_availability_pincode values(19,'440019');
insert into service_availability_pincode values(20,'440020');
insert into service_availability_pincode values(21,'440021');
insert into service_availability_pincode values(22,'440022');
insert into service_availability_pincode values(23,'440023');
insert into service_availability_pincode values(24,'440024');
insert into service_availability_pincode values(25,'440025');
insert into service_availability_pincode values(26,'440026');
insert into service_availability_pincode values(27,'440027');
insert into service_availability_pincode values(28,'440028');
insert into service_availability_pincode values(29,'440029');
insert into service_availability_pincode values(30,'440030');
insert into service_availability_pincode values(31,'440031');
insert into service_availability_pincode values(32,'440032');
insert into service_availability_pincode values(33,'440033');
insert into service_availability_pincode values(34,'440034');
insert into service_availability_pincode values(35,'440035');
insert into service_availability_pincode values(36,'440036');
insert into service_availability_pincode values(37,'440037');
insert into service_availability_pincode values(38,'441108');
insert into service_availability_pincode values(39,'441111');


=====================================================================
PRODUCTION EMPLOYEE DATA

CREATE TABLE `employee` (
  `emp_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_fname` varchar(45) DEFAULT NULL,
  `emp_lname` varchar(45) DEFAULT NULL,
  `emp_email_id` varchar(45) DEFAULT NULL,
  `emp_age` int(11) DEFAULT NULL,
  `emp_gender` varchar(45) DEFAULT NULL,
  `emp_address_id` int(11) DEFAULT NULL,
  `emp_qualification` varchar(45) DEFAULT NULL,
  `emp_primary_id_type` varchar(45) DEFAULT NULL,
  `emp_primary_id_value` varchar(45) DEFAULT NULL,
  `emp_secondary_id_type` varchar(45) DEFAULT NULL,
  `emp_secondary_id_value` varchar(45) DEFAULT NULL,
  `emp_contact_no` varchar(45) DEFAULT NULL,
  `emp_contact_no2` varchar(45) DEFAULT NULL,
  `emp_bank_account_no` varchar(45) DEFAULT NULL,
  `ifsc_code` varchar(45) DEFAULT NULL,
  `emp_role_id` int(11) DEFAULT NULL,
  `emp_status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) 

=========================================
insert into employee values(1,'Rushikesh Umendra','Pande','',0,'Male',1,'','Aadhar Card','2754 9669 5879','','','9021431445','9823509483','0635104000099059','IBKL0000635','1','active');
insert into employee values(2,'Khemraj Balaji','Nikose','',0,'Male',2,'','Aadhar Card','9326 9154 9436','','','9421120452','7304749181','35533134359','SBIN0016095','1','active');
insert into employee values(3,'Savita Shekhar','Nimbalkar','',0,'Female',3,'','Aadhar Card','4609 6782 4016','','','9518334214','9370096438','11970100010257','BARB0MAHNAG','1','active');
insert into employee values(4,'Roshani Pravin','Bhorkar','',0,'Female',4,'','Aadhar Card','3160 6330 9975','','','7620612618','9326019699','50218100002968','BARB0WARNAG','1','active');

insert into employee values(5,'Pravin Prakash','Bhorkar','',0,'Male',5,'','Aadhar Card','6675 0729 8235','','','7620612618','9326019699','8990100018020','BARB0SADNAG','1','active');
insert into employee values(6,'Mahendra Gopal','Dhuriya','',0,'Male',6,'','Aadhar Card','9032 7045 5633','','','9881779398','958443857','36002050001214','UBIN0536601','1','active');
insert into employee values(7,'Dharmendra Mandal','Kanojiya','',0,'Male',7,'','Aadhar Card','8729 5841 7054','','','7517668050','','0305101034175','CNRB0000305','1','active');
insert into employee values(8,'Gita Ratan','Kanojiya','',0,'Female',8,'','Aadhar Card','9464 5877 2743','','','8805782797','8055246493','10983424427','SBIN0001305','1','active');


insert into employee values(9,'Chandralal Kawalprasad','Shukla','',0,'Male',9,'','Aadhar Card','7685 0994 6726','','','8329813699','9309561926','32613300592','SBIN0011419','1','active');
insert into employee values(10,'Ritesh Digambar','Ninawe','',0,'Male',10,'','Aadhar Card','2648 4275 5562','','','7972015390','9172434597','20007738600','MAHB0000526','1','active');
insert into employee values(11,'Prema Manish','Khedkar','',0,'Female',11,'','Aadhar Card','9498 5023 2653','','','9767505841','9503850115','063110110009568','BKID0008700','1','active');
insert into employee values(12,'Madhuri Sudhir','Meshram','',0,'Female',12,'','Aadhar Card','3712 9154 1941','','','9763528962','9730304779','43678100006232','BARB0RANIDU','1','active');

insert into employee values(13,'Shrikant Baburaoji','Gonnade','',0,'Male',13,'','Aadhar Card','3497 1649 5068','','','8080226349','8329563172','30388046213','SBIN0000518','1','active');
insert into employee values(14,'Mahendra Badrinath','Lanjewar','',0,'Male',14,'','Aadhar Card','4896 9293 8494','','','9527607311','8888728672','20009744566','MAH0000312','1','active');
insert into employee values(15,'Mrs. Hemlata Sunil','Dodke','',0,'Female',15,'','Aadhar Card','2772 0358 4636','','','9172289587','9823579587','607002010008001','UBIN0560707','1','active');
insert into employee values(16,'Ratnakar Pandurag','Mahatkar','',0,'Male',16,'','Aadhar Card','9074 8512 0376 ','','','7066555349','7066555349','874810110001883','BKID0008748','1','active');

insert into employee values(17,'Sudhir Madhukar','Narwade','',0,'Male',17,'','Aadhar Card','5452 32176747','','','8999714352','9326691888','574602010012861','UBIN0557463','1','active');
insert into employee values(18,'Indira Manoj','Gawande','',0,'Female',18,'','Aadhar Card','6991 5690 7415','','','9529708314','8530064203','36607174495','','1','active');
insert into employee values(19,'Nirjala Rajendra','Gawai','',0,'Female',19,'','Aadhar Card','3216 6098 2289 ','','','7843041385','7038708699','371701000000065','IOBA0003717','1','active');
insert into employee values(20,'Reena Shyam','Yadao','',0,'Female',20,'','Aadhar Card','3216 6098 2289','','','7498248756','9284539987','371701000000065','IOBA0003717','1','active');
insert into employee values(21,'Vinod Nagorao','Patil','',0,'Male',21,'','Aadhar Card','4324 2135 8748','','','7620152160','','520101260796049','CORP0000209','1','active');
insert into employee values(22,'Rina Tushar','Bhaisare','',0,'Female',22,'','Aadhar Card','6014 0004 7576','','','9579740975','9657653081','','','1','active');





insert into employee values(23,'Akash Prakashrao','Shingade','',0,'Male',23,'','Aadhar Card','4938 4781 8822','','','9021628378','7058785830','5499101001274','CNRB0003125','1','active');
insert into employee values(24,'Sanchana vilas','Khadaskar','',0,'Female',24,'','Aadhar Card','8276 9740 6491','','','9011365458','','35602556635','SBIN0008239','1','active');
insert into employee values(25,'Rajesh Namdeorao','Sopankar','',0,'Male',25,'','Aadhar Card','2423 1997 8422','','','9096064526','8788137089','33705391250','SBIN0011419','1','active');




insert into employee values(43,'Sanjay Pritichand','Choudhari','',0,'Male',1,'','Aadhar Card','7338 2237 1713','','','9112088952','','874810100003945','BKID0008748','1','active');
insert into employee values(44,'Arati Arjun','Raut','',0,'Female',1,'','Aadhar Card','8981 6094 3405','','','9172280188','','38120807743','SBIN0017638','1','active');
insert into employee values(45,'Mangesh Ramesh','Wasnik','',0,'Male',1,'','Aadhar Card','9103 0304 9520','','','9960058647','8600148647','574602010010454','UBIN0557463','1','active');
insert into employee values(46,'Pankaj Ramesh','Hedaoo','',0,'Male',1,'','Aadhar Card','9809 7710 4171','','','8999647162','7447515915','4495700186','UJVN0004495','1','active');



============================================================================================================================================================================

insert into employee values(26,'Shekhar Ganpatrao','Nimbalkar','',0,'Male',26,'','Aadhar Card','2614 8305 0272','','','9518334214','9370096438','470870100008627','BARB0TILNAG','2','active');
insert into employee values(27,'Arvind Bastiram','Meshram','',0,'Male',27,'','Aadhar Card','7843 7867 9383','','','9326472628','','543102010009851','UBIN0554316','2','active');
insert into employee values(28,'Toshif Laxmanrao','Dhurve','',0,'Male',28,'','Aadhar Card','7819 7826 8796','','','9146587025','9373937102','','','2','active');
insert into employee values(29,'Amit Dharmraj','Mandlik','',0,'Male',29,'','Aadhar Card','6018 3391 8274','','','8237780660','7769889798','04660100019590','BARB0GANNAG','2','active');

insert into employee values(30,'Sudhir Sumandas','Meshram','',0,'Male',30,'','Aadhar Card','7723 9778 2560','','','9730304779','9763528962','30594989773','SBIN0014726','2','active');
insert into employee values(31,'Nitin Bhaskarrao','Jangam','',0,'Male',31,'','Aadhar Card','7425 3281 1188','','','9764422845','7719073403','60110902155','MAHB0000452','2','active');
insert into employee values(32,'Vishwajit Vijay','Sahurkar','',0,'Male',32,'','Aadhar Card','8012 1902 4956','','','9529809157','8552048575','50216811231','ALLA0212832','2','active');
insert into employee values(33,'Devendra Tribhuvannath','Sahu','',0,'Male',33,'','Aadhar Card','3490 5624 8826','','','9028684510','9168370817','60146329384','MAHB0000356','2','active');


insert into employee values(34,'Vilas Jagannath singh','Chauhan','',0,'Male',34,'','Aadhar Card','5703 1903 9318','','','9975722498','9975140399','','','2','active');
insert into employee values(35,'Manoj Sahebrao','Mohod','',0,'Male',35,'','Aadhar Card','4372 2655 4370','','','8007530005','8669813771','31650100011538','BARB0JARIPA','2','active');
insert into employee values(36,'Rahul Sadhuram','Machhale','',0,'Male',36,'','Aadhar Card','5585 0593 1988','','','7387484581','7038175581','34752749086','SBIN0016095','2','active');
insert into employee values(37,'Kamal Santosh','Yerpude','',0,'Male',37,'','Aadhar Card','8431 2814 0025','','','9156856244','9923131666','0813000400007885','PUNB0882400','2','active');

insert into employee values(38,'Kapil Natthuji','Bambal','',0,'Male',38,'','Aadhar Card','3057 4037 4378','','','8625888882','9049198786','0813000400007885','PUNB0882400','2','active');

insert into employee values(47,'Shantnu suresh','Goje','',0,'Male',47,'','Aadhar Card','5632 1994 0772','','','9765992208','9325417406','509302010010517','UBIN0550931','2','active');
insert into employee values(48,'Sayog Ganesh','Thakre','',0,'Male',48,'','Aadhar Card','2262 2757 0250','','','9307669045','','3688333093','CBIN0284426','2','active');

   

//TEST ACCOUNT
insert into employee values(39,'Raj','kumar','raj@gmail.com',25,'Male',1,'Graduate','VOTERID','V10001','PAN','P1001','9937123456','','Bank_Details-1','','1','active');
insert into employee values(40,'Haris','kumar','hari@gmail.com',27,'Male',1,'Graduate','VOTERID','V10002','PAN','P1002','9937123466','','Bank_Details-2','','1','active');
insert into employee values(41,'Rabi','kumar','rabi@gmail.com',24,'Male',1,'Graduate','VOTERID','V10003','PAN','P1003','9937123477','','Bank_Details-3','','2','active');
insert into employee values(42,'Kiran','kumar','kiran@gmail.com',22,'Male',1,'Graduate','VOTERID','V10004','PAN','P1004','9937123000','','Bank_Details-4','','2','active');



insert into employee values(49,'Nisha','Rupesh Hedau','',0,'Female',49,'','Aadhar Card','6192 69395383','','','7020173944','','','','1','active');
insert into employee values(50,'Shubham','Arjun Tiwari','',0,'Male',50,'','Aadhar Card','2025 5875 2524','','','9689485319','','20256960390','SBIN0017639','1','active');
insert into employee values(51,'Pundalik','Raghobaji Laxane','',0,'Male',51,'','Aadhar Card','8383 9503 0089','','','7264017914','8530388490','60130248141','MAHB0000951','1','active');
insert into employee values(52,'Manish','Madhukar Fender','',0,'Male',52,'','Aadhar Card','8066 3893 9033','','','9822715895','','11072752230','SBIN0001633','1','active');



================================================================
//PRODUCTION

insert into employee_login values(1,1,'rushikesh1','12345678','05/02/2020 01:17pm','','',1);
insert into employee_login values(2,2,'khemraj2','12345678','05/02/2020 01:20pm','','',1);
insert into employee_login values(3,3,'savita3','12345678','05/02/2020 01:35pm','','',1);
insert into employee_login values(4,4,'roshani4','12345678','05/02/2020 01:50pm','','',1);

insert into employee_login values(5,5,'pravin5','12345678','05/02/2020 01:17pm','','',1);
insert into employee_login values(6,6,'mahendra6','12345678','05/02/2020 01:20pm','','',1);
insert into employee_login values(7,7,'dharmendra7','12345678','05/02/2020 01:35pm','','',1);
insert into employee_login values(8,8,'gita8','12345678','05/02/2020 01:50pm','','',1);

insert into employee_login values(9,9,'chandralal9','12345678','05/02/2020 01:17pm','','',1);
insert into employee_login values(10,10,'ritesh10','12345678','05/02/2020 01:20pm','','',1);
insert into employee_login values(11,11,'prema11','12345678','05/02/2020 01:35pm','','',1);
insert into employee_login values(12,12,'madhuri12','12345678','05/02/2020 01:50pm','','',1);

insert into employee_login values(13,13,'shrikant13','12345678','05/02/2020 01:17pm','','',1);
insert into employee_login values(14,14,'mahendra14','12345678','05/02/2020 01:20pm','','',1);
insert into employee_login values(15,15,'hemlata15','12345678','05/02/2020 01:35pm','','',1);
insert into employee_login values(16,16,'ratnakar16','12345678','05/02/2020 01:50pm','','',1);

insert into employee_login values(17,17,'sudhir17','12345678','05/02/2020 01:17pm','','',1);
insert into employee_login values(18,18,'indira18','12345678','05/02/2020 01:20pm','','',1);
insert into employee_login values(19,19,'nirjala19','12345678','05/02/2020 01:35pm','','',1);
insert into employee_login values(20,20,'reena20','12345678','05/02/2020 01:50pm','','',1);
insert into employee_login values(21,21,'vinod21','12345678','05/02/2020 01:17pm','','',1);
insert into employee_login values(22,22,'rina22','12345678','05/02/2020 01:20pm','','',1);




insert into employee_login values(23,23,'reena20','12345678','05/02/2020 01:50pm','','',1);
insert into employee_login values(24,24,'vinod21','12345678','05/02/2020 01:17pm','','',1);
insert into employee_login values(25,25,'rina22','12345678','05/02/2020 01:20pm','','',1);
insert into employee_login values(26,26,'sudhir17','12345678','05/02/2020 01:17pm','','',2);
insert into employee_login values(27,27,'indira18','12345678','05/02/2020 01:20pm','','',2);
insert into employee_login values(28,28,'nirjala19','12345678','05/02/2020 01:35pm','','',2);
insert into employee_login values(29,29,'reena20','12345678','05/02/2020 01:50pm','','',2);
insert into employee_login values(30,30,'vinod21','12345678','05/02/2020 01:17pm','','',2);
insert into employee_login values(31,31,'rina22','12345678','05/02/2020 01:20pm','','',2);
insert into employee_login values(32,32,'reena20','12345678','05/02/2020 01:50pm','','',2);
insert into employee_login values(33,33,'vinod21','12345678','05/02/2020 01:17pm','','',2);
insert into employee_login values(34,34,'rina22','12345678','05/02/2020 01:20pm','','',2);
insert into employee_login values(35,35,'reena20','12345678','05/02/2020 01:50pm','','',2);
insert into employee_login values(36,36,'vinod21','12345678','05/02/2020 01:17pm','','',2);
insert into employee_login values(37,37,'rina22','12345678','05/02/2020 01:20pm','','',2);
insert into employee_login values(38,38,'rina22','12345678','05/02/2020 01:20pm','','',2);



insert into employee values(39,'Raj','kumar','raj@gmail.com',25,'Male',1,'Graduate','VOTERID','V10001','PAN','P1001','9937123456','','Bank_Details-1','','1','active');
insert into employee values(40,'Haris','kumar','hari@gmail.com',27,'Male',1,'Graduate','VOTERID','V10002','PAN','P1002','9937123466','','Bank_Details-2','','1','active');
insert into employee values(41,'Rabi','kumar','rabi@gmail.com',24,'Male',1,'Graduate','VOTERID','V10003','PAN','P1003','9937123477','','Bank_Details-3','','2','active');
insert into employee values(42,'Kiran','kumar','kiran@gmail.com',22,'Male',1,'Graduate','VOTERID','V10004','PAN','P1004','9937123000','','Bank_Details-4','','2','active');


insert into employee_login values(39,39,'IE1039','12345678','05/02/2020 01:50pm','','',1);
insert into employee_login values(40,40,'IE1040','12345678','05/02/2020 01:17pm','','',1);
insert into employee_login values(41,41,'IE1041','12345678','05/02/2020 01:20pm','','',2);
insert into employee_login values(42,42,'IE1042','12345678','05/02/2020 01:20pm','','',2);

insert into employee values(43,'Sanjay Pritichand','Choudhari','',0,'Male',1,'','Aadhar Card','7338 2237 1713','','','9112088952','','874810100003945','BKID0008748','1','active');
insert into employee values(44,'Arati Arjun','Raut','',0,'Female',1,'','Aadhar Card','8981 6094 3405','','','9172280188','','38120807743','SBIN0017638','1','active');
insert into employee values(45,'Mangesh Ramesh','Wasnik','',0,'Male',1,'','Aadhar Card','9103 0304 9520','','','9960058647','8600148647','574602010010454','UBIN0557463','1','active');
insert into employee values(46,'Pankaj Ramesh','Hedaoo','',0,'Male',1,'','Aadhar Card','9809 7710 4171','','','8999647162','7447515915','4495700186','UJVN0004495','1','active');

insert into employee_login values(43,43,'IE1043','875499','12/02/2020 12:50pm','','',1);
insert into employee_login values(44,44,'IE1044','985410','12/02/2020 12:57pm','','',1);
insert into employee_login values(45,45,'IE1045','109561','12/02/2020 12:57pm','','',1);
insert into employee_login values(46,46,'IE1046','111057','12/02/2020 12:57pm','','',1);


insert into employee_login values(47,47,'IE1047','128501','12/02/2020 12:57pm','','',2);
insert into employee_login values(48,48,'IE1048','135940','12/02/2020 12:57pm','','',2);




insert into employee_login values(49,49,'IE1049','145949','08/03/2020 1:10pm','','',1,'');
insert into employee_login values(50,50,'IE1050','165001','08/03/2020 1:10pm','','',1,'');
insert into employee_login values(51,51,'IE1051','175152','08/03/2020 1:10pm','','',1,'');
insert into employee_login values(52,52,'IE1052','187521','08/03/2020 1:10pm','','',1,'');


==============================================================
CREATE TABLE `laundry_partner_address` (
  `emp_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `address_details` varchar(999) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`laundry_partner_address_id`)
)

================================================================
insert into laundry_partner_address values(1,'plot no 12, Padmavati Nagar, Near Thakre house, Godhani, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(2,'28 Thakre layout, beside Ramnath city, Bhokara, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(3,'c/o Namdeo Enpreddiwar, Juni Mangalwari,Gujri chowk, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(4,'100 Jay Ambey nagar, Bhandewadi Pardi Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(5,'100 Jay Ambey nagar, Bhandewadi Pardi Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(6,'House no. 678, Opp royal Bakrey, Hanuman Vatika, Main Road, Sitabuldi, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(7,'House no 929, kasabpura, Nalsahebroad, Hansapuri, Mahatma fule Bazar, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(8,'Near Hanuman Mandir, Nalsaheb Chowk, Kasabpura,mahatma Fule Bazar, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(9,'Plot no 152, New Vasundhara Soc, Revati Nagar, Besa Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(10,'Jai bhole Nagar, Behind Kaal Bhairav Mandir, Binaki Layout, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(11,'plot no 45, flat no 302, Krishna Pearl Appt, Katol Road, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(12,'plot no 69, Lal Zenda Chowk, Yashodhara Nagar, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(13,'Plot no. 21, behind Chota Tajbagh, Survey layout, Sakkardara, Nagpur ','Nagpur','Maharashtra');
insert into laundry_partner_address values(14,'Plot no 9, pratap nagar, Near Buddha vihar, Sarvoday Nagar, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(15,'Plot no. 182, Akhil Vishwabharati Hou. Co-op Soc, Dabha Road, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(16,'5 Juni wasahat, Near Hanuman Mandir, New Narsala bus stop, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(17,'Plot no 06 road no 10, Jogi nagar, Rameshwari Road, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(18,'Plot no 137, maa Bhavani nagar, Kharabi road, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(19,'flat no 406, Royal complex, Nr indian petrol pump, mohd rafiq chowk,Yashodhara nagar, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(20,'plot no 35, Darshana soc, Nr hanuman mandir Ganesh Nagar, Dabha, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(21,'plot no 420, near Nari ring Road, Angulimal Nagar, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(22,'plot no 105, Nr Varsha Tembhurkar House, Lumbini Nagar, Christian colony, Nagpur','Nagpur','Maharashtra');



insert into laundry_partner_address values(23,'qtr no 250/4, near ESIS hospital, Somawari quarters, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(24,'plot no 23, LIC colony behind tajshree honda,khamala road, Deonagar nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(25,'flat no 504, swayambhu appt, near Nasare sabhagruha, Hudkeshwar Nagpur','Nagpur','Maharashtra');

insert into laundry_partner_address values(26,'c/o namdeo Enprediwar, Juni Mangalwari, gujari chowk, Bagadganj, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(27,'plot no 36, Near Aandhi Tufan kirana, Smita nagar, Nari road, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(28,'plot no 84, Dhawani Nagar, Manpure Galli, Punapur Road, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(29,'house no 430, jagannath Budhawari, bh Shani Temple, Bharat mata chowk, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(30,'plot no 69, lal zenda chowk, Yashodhara Nagar, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(31,'plot no 40, borgaon gorewada Road, Shingane layout, Ekta nagar, NR new Hanuman mandir, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(32,'near FCI godown, Rajiv nagar, Wardha Road, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(33,'plot no 1433, near Water tank, Minimata nagar, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(34,'plot no 05, BH laxmibajaj Ayurvedic college, Nr hanuman Temple, Vidya nagari, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(35,'plot no 375, Dhammayan nagar, Nr Rani Durgawati chowk, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(36,'plot no 36, near Shivgiri Temple, Kooradi Road, Om nagar, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(37,'c/o Amit kothe, plot no 84, nr shitala mata mandir, bhole nagar, hudkeshwar ps road, nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(38,'plot no 247, near Chauhan kirana, Umrer Road, Ekta nagar, Dighori, Nagpur','Nagpur','Maharashtra');


insert into laundry_partner_address values(43,'plot no 98, back of Take off city, Sant gajanan nagar, Gargoti narsala, nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(44,'plot no 173, nr Vinayakrao High school, Nr NIT garden, Shanti Nagar, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(45,'plot no 149, near new ring Road, Jogi nagar, po Bhagwan nagar, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(46,'plot no 94/95, near mahakalkar lawn, opp nag-shiv-hanuman mandir, balaji nagar, pardi, nagpur','Nagpur','Maharashtra');


insert into laundry_partner_address values(47,'plot no.75, parddi,bhavani nagar,near ganesh mandir,nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(48,'plot no 35, near gurudwara, khalasa ground, teka naka, nagpur','Nagpur','Maharashtra');




insert into laundry_partner_address values(49,'plot no 727, Near madhuban bank, hedau naiwas, Satranjipura nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(50,'plot no 8, near datta mandir, wardha road, chinchbhuvan, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(51,'near NIT garden, Beisde Kawale novelty, old babhukheda, Nagpur','Nagpur','Maharashtra');
insert into laundry_partner_address values(52,'plot no  71, jai  Durga soc layout, somalwada, Nagpur','Nagpur','Maharashtra');


===========================================================
CREATE TABLE `delman_pincode_map` (
  `delman_pincode_map_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) DEFAULT NULL,
  `pincode` varchar(15) DEFAULT NULL,
  `emp_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`delman_pincode_map_id`)
) 


=============================================

insert into delman_pincode_map values(1,1,'440030',1);
insert into delman_pincode_map values(2,1,'440013',1);

insert into delman_pincode_map values(3,2,'441111',1);

insert into delman_pincode_map values(4,3,'440008',1);
insert into delman_pincode_map values(5,3,'440030',1);
insert into delman_pincode_map values(6,3,'441111',1);
insert into delman_pincode_map values(7,3,'440014',1);

insert into delman_pincode_map values(8,4,'440035',1);
insert into delman_pincode_map values(9,4,'440036',1);
insert into delman_pincode_map values(10,4,'440008',1);

insert into delman_pincode_map values(11,5,'440035',1);
insert into delman_pincode_map values(12,5,'440036',1);
insert into delman_pincode_map values(13,5,'440008',1);


insert into delman_pincode_map values(14,7,'440002',1);
insert into delman_pincode_map values(15,7,'440032',1);
insert into delman_pincode_map values(16,7,'440018',1);

insert into delman_pincode_map values(17,8,'440002',1);
insert into delman_pincode_map values(18,8,'440032',1);
insert into delman_pincode_map values(19,8,'440018',1);

insert into delman_pincode_map values(20,9,'440037',1);


insert into delman_pincode_map values(21,10,'440017',1);
insert into delman_pincode_map values(22,10,'440026',1);

insert into delman_pincode_map values(23,12,'440026',1);
insert into delman_pincode_map values(24,12,'440017',1);

insert into delman_pincode_map values(25,14,'440005',1);
insert into delman_pincode_map values(26,14,'440022',1);
insert into delman_pincode_map values(27,14,'440003',1);
insert into delman_pincode_map values(28,14,'440015',1);
insert into delman_pincode_map values(29,14,'440025',1);



insert into delman_pincode_map values(30,15,'440007',1);
insert into delman_pincode_map values(31,15,'440006',1);
insert into delman_pincode_map values(32,15,'440023',1);
insert into delman_pincode_map values(33,15,'440013',1);

insert into delman_pincode_map values(34,16,'440034',1);
insert into delman_pincode_map values(35,16,'440024',1);
insert into delman_pincode_map values(36,16,'440009',1);

insert into delman_pincode_map values(37,17,'440034',1);
insert into delman_pincode_map values(38,17,'440024',1);
insert into delman_pincode_map values(39,17,'440009',1);
insert into delman_pincode_map values(40,17,'440027',1);

insert into delman_pincode_map values(41,18,'440023',1);
insert into delman_pincode_map values(42,18,'440035',1);
insert into delman_pincode_map values(43,18,'440024',1);


insert into delman_pincode_map values(44,20,'440007',1);
insert into delman_pincode_map values(45,20,'440006',1);
insert into delman_pincode_map values(46,20,'440013',1);


insert into delman_pincode_map values(47,21,'440026',1);
insert into delman_pincode_map values(48,21,'441111',1);


insert into delman_pincode_map values(49,23,'440024',1);
insert into delman_pincode_map values(50,23,'440027',1);
insert into delman_pincode_map values(51,23,'440034',1);

insert into delman_pincode_map values(52,24,'441108',1);
insert into delman_pincode_map values(53,24,'440005',1);
insert into delman_pincode_map values(54,24,'440022',1);
insert into delman_pincode_map values(55,24,'440003',1);
insert into delman_pincode_map values(56,24,'440015',1);


insert into delman_pincode_map values(57,25,'440024',1);
insert into delman_pincode_map values(58,25,'440027',1);
insert into delman_pincode_map values(59,25,'440009',1);


insert into delman_pincode_map values(60,26,'440024',1);
insert into delman_pincode_map values(61,26,'440027',1);
insert into delman_pincode_map values(62,26,'440009',1);

insert into delman_pincode_map values(63,27,'440035',1);//delete
insert into delman_pincode_map values(64,27,'440036',1);//delete
insert into delman_pincode_map values(65,27,'440008',1);//delete


insert into delman_pincode_map values(66,28,'440009',1);//delete
insert into delman_pincode_map values(67,28,'440024',1);//delete
insert into delman_pincode_map values(68,28,'440034',1);//delete


insert into delman_pincode_map values(69,29,'440008',1);//delete
insert into delman_pincode_map values(70,29,'440036',1);//delete
insert into delman_pincode_map values(71,29,'440035',1);//delete


insert into delman_pincode_map values(72,27,'440026',2);
insert into delman_pincode_map values(73,27,'440030',2);
insert into delman_pincode_map values(74,27,'441111',2);
insert into delman_pincode_map values(75,27,'440014',2);
insert into delman_pincode_map values(76,27,'440017',2);

insert into delman_pincode_map values(77,29,'440002',2);
insert into delman_pincode_map values(77,29,'440018',2);
insert into delman_pincode_map values(79,29,'440032',2);


insert into delman_pincode_map values(80,32,'440025',2);
insert into delman_pincode_map values(81,32,'440005',2);
insert into delman_pincode_map values(82,32,'440022',2);
insert into delman_pincode_map values(83,32,'440003',2);
insert into delman_pincode_map values(84,32,'440012',2);

insert into delman_pincode_map values(85,35,'440006',2);
insert into delman_pincode_map values(86,35,'440007',2);
insert into delman_pincode_map values(87,35,'440013',2);


insert into delman_pincode_map values(88,36,'440023',2);
insert into delman_pincode_map values(89,36,'440001',2);
insert into delman_pincode_map values(90,36,'440010',2);
insert into delman_pincode_map values(91,36,'440033',2);


insert into delman_pincode_map values(92,37,'440027',2);
insert into delman_pincode_map values(93,37,'440024',2);
insert into delman_pincode_map values(94,37,'440009',2);


insert into delman_pincode_map values(95,38,'441108',2);//already assigned (deleted and new assignment done to 38)
insert into delman_pincode_map values(96,38,'440015',2);//already assigned
insert into delman_pincode_map values(97,38,'440037',2);//already assigned

insert into delman_pincode_map values(105,32,'441108',2);//to be assigned (done)
insert into delman_pincode_map values(106,32,'440015',2);//to be assigned
insert into delman_pincode_map values(107,32,'440037',2);//to be assigned

 
insert into delman_pincode_map values(98,47,'440035',2);
insert into delman_pincode_map values(99,47,'440008',2);
insert into delman_pincode_map values(100,47,'440036',2);


insert into delman_pincode_map values(101,39,'753002',1);
insert into delman_pincode_map values(102,40,'753001',1);
insert into delman_pincode_map values(103,41,'753001',2);
insert into delman_pincode_map values(104,42,'753002',2);


insert into delman_pincode_map values(105,49,'440008',1);
insert into delman_pincode_map values(106,50,'440008',1);
insert into delman_pincode_map values(107,51,'440027',1);
insert into delman_pincode_map values(108,52,'440015',1);


=====================================================





insert into employee values(39,'Raj','kumar','raj@gmail.com',25,'Male',1,'Graduate','VOTERID','V10001','PAN','P1001','9937123456','','Bank_Details-1','','1','active');
insert into employee values(40,'Haris','kumar','hari@gmail.com',27,'Male',1,'Graduate','VOTERID','V10002','PAN','P1002','9937123466','','Bank_Details-2','','1','active');
insert into employee values(41,'Rabi','kumar','rabi@gmail.com',24,'Male',1,'Graduate','VOTERID','V10003','PAN','P1003','9937123477','','Bank_Details-3','','2','active');
insert into employee values(42,'Kiran','kumar','kiran@gmail.com',22,'Male',1,'Graduate','VOTERID','V10004','PAN','P1004','9937123000','','Bank_Details-4','','2','active');


insert into employee_login values(39,39,'IE1039','12345678','05/02/2020 01:50pm','','',1);
insert into employee_login values(40,40,'IE1040','12345678','05/02/2020 01:17pm','','',1);
insert into employee_login values(41,41,'IE1041','12345678','05/02/2020 01:20pm','','',2);
insert into employee_login values(42,42,'IE1042','12345678','05/02/2020 01:20pm','','',2);






====================================================================



insert into partner_order_process_capability values(1,1,1,60,0);
insert into partner_order_process_capability values(2,2,1,60,0);
insert into partner_order_process_capability values(3,4,1,60,0);
insert into partner_order_process_capability values(4,5,1,60,0);
insert into partner_order_process_capability values(5,7,1,60,0);

insert into partner_order_process_capability values(6,9,1,60,0);
insert into partner_order_process_capability values(7,10,1,60,0);
insert into partner_order_process_capability values(8,13,1,60,0);
insert into partner_order_process_capability values(9,14,1,60,0);
insert into partner_order_process_capability values(10,15,1,60,0);


insert into partner_order_process_capability values(11,17,1,60,0);
insert into partner_order_process_capability values(12,19,1,60,0);
insert into partner_order_process_capability values(13,20,1,60,0);
insert into partner_order_process_capability values(14,21,1,60,0);
insert into partner_order_process_capability values(15,23,1,60,0);
insert into partner_order_process_capability values(16,24,1,60,0);
insert into partner_order_process_capability values(17,25,1,60,0);





insert into edi_test values('ISA~00~          ~00~          ~12~4164457255     ~ZZ~METRO    ~191112~0750~U~00401~000000731~0~P~>=');
insert into edi_test values('GS~PO~4164457255~METRO~20191112~0750~731~X~004010=');
insert into edi_test values('ST~810~000000001=');

insert into edi_test values('BIG~20200116~2323231~20191024~878067=');

insert into edi_test values('IT1~1.00~1~EA~107.8~~VN~ASSEMBLY-T1-1~BP~ASSEMBLY-T1-1~~=');

insert into edi_test values('TDS~108.8=');

insert into edi_test values('SAC~C~D360~~~100~~~~~~~06=');


insert into edi_test values('CTT~1=');

insert into edi_test values('SE~7~00000002=');

insert into edi_test values('GE~1~731=');

insert into edi_test values('IEA~1~000000731=');

======================================================================
SELECT  laundry_db.employee.emp_id,  laundry_db.employee.emp_fname,  laundry_db.employee.emp_lname, laundry_db.delman_pincode_map.pincode,laundry_db.employee.emp_role_id FROM laundry_db.employee
INNER JOIN laundry_db.delman_pincode_map ON laundry_db.delman_pincode_map.emp_id = laundry_db.employee.emp_id;